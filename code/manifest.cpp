// #include <windows.h>
#include "jec_win.h"
#include "jec_gl.h"

#include "intrinsics.h"

#define STBTT_malloc(x,u)  ((void)(u),Allocate(x))
#define STBTT_free(x,u)    ((void)(u),Deallocate(x))
#define STBRP_SORT SelectionSort
#define STBRP_ASSERT Assert
#define STBTT_assert Assert

#define STBTT_cos(x)  Cos(x)
#define STBTT_acos(x) ArcCos(x)

#define STBTT_ifloor(x)   ((int) Floor(x))
#define STBTT_iceil(x)    ((int) Ceil(x))

#define STBTT_sqrt(x)      SquareRoot(x)
#define STBTT_pow(x,y)     Power(x,y)

#define STBTT_fmod(x,y)    Fmod(x,y)

#define STBTT_strlen(x)    StringLength(x)

#define STBTT_memcpy       CopyMemory
#define STBTT_memset       SetMemory

#define STBTT_fabs(x)      Abs(x)

#define STB_TRUETYPE_IMPLEMENTATION
#define STB_RECT_PACK_IMPLEMENTATION
#include "stb_rect_pack.h"
#include "stb_truetype.h"

#include "platform.cpp"
#include "texteditor.h"

#include "texteditor.cpp"
#include "win32_texteditor.cpp"
