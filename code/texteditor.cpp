
#if 0
bitmap
LoadBitmapFromFile(char *Filename, platform_code *Platform)
{
    bitmap Result = {};
    
    int Width, Height, Channels;
    // stbi_set_flip_vertically_on_load(true);
    
    debug_read_file_result File = Platform->DebugReadFile(Filename);
    u8 *Bitmap = stbi_load_from_memory((u8 *)File.Memory, (int)File.Size, &Width, &Height, &Channels, 0);
    Result.Width  = Width;
    Result.Height = Height;
    Result.Stride = Result.Width*Channels;
    
    // Note: + Stride for SIMD friendliness
    Result.Memory = Allocate(Result.Stride*Result.Height + Width*Channels);
    
    u32 *Pixel = (u32 *)Result.Memory;
    u32 *Source = (u32 *)Bitmap;
    for(s32 Y = 0;
        Y < Height;
        ++Y)
    {
        for(s32 X = 0;
            X < Width;
            ++X)
        {
            v4 Texel =
            {
                (r32)((*Source >> 16) & 0xFF),
                (r32)((*Source >>  8) & 0xFF),
                (r32)((*Source >>  0) & 0xFF),
                (r32)((*Source >> 24) & 0xFF)
            };
            
            Texel = SRGB255ToLinear1Fast(Texel);
            
            Texel.rgb = Texel.rgb*Texel.a;
            
            Texel = Linear1ToSRGB255Fast(Texel);
            
            *Pixel++ = (((u32)(Texel.a + 0.5f) << 24) |
                        ((u32)(Texel.r + 0.5f) << 16) |
                        ((u32)(Texel.g + 0.5f) <<  8) |
                        ((u32)(Texel.b + 0.5f) <<  0));
            ++Source;
        }
    }
    
    Platform->DebugFreeFile(&File);
    
    return Result;
}
#endif

void
LoadAssets(mandala *TransMandala, editor_state *State)
{
    GlobalFontMandala = InitMandala(Allocate(Mega(3)), Mega(3));

    StartTimedPoint(KeywordStuffs);
    {
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "alignas");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "alignof");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "and");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "and_eq");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "atomic_cancel");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "atomic_commit");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "atomic_noexcept");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "auto");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "bitand");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "bitor");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "bool");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "break");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "case");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "catch");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "char");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "char16_t");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "char32_t");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "class");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "compl");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "concept");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "const");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "constexpr");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "const_cast");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "continue");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "co_await");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "co_return");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "co_yield");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "decltype");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "default");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "delete");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "do");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "double");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "dynamic_cast");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "else");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "enum");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "explicit");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "export");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "extern");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "false");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "float");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "for");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "friend");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "goto");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "if");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "import");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "inline");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "int");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "long");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "module");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "mutable");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "namespace");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "new");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "noexcept");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "not");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "not_eq");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "nullptr");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "operator");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "or");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "or_eq");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "private");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "protected");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "public");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "register");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "reinterpret_cast");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "requires");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "return");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "short");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "signed");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "sizeof");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "static");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "static_assert");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "static_cast");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "struct");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "switch");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "synchronized");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "template");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "this");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "thread_local");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "throw");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "true");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "try");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "typedef");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "typeid");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "typename");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "union");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "unsigned");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "using");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "virtual");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "void");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "volatile");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "wchar_t");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "while");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "xor");
        InsertNode(&State->Dictionary[Dictionary_cpp_keyword], "xor_eq");
    }
    EndTimedPoint(KeywordStuffs);
}

void
CopySection(file_buffer *FileBuffer, umm FirstPoint, umm SecondPoint)
{
    StartTimedPoint(CopySection);
    if(FirstPoint != SecondPoint)
    {
        umm MinPoint = Min(FirstPoint, SecondPoint);
        umm MaxPoint = Max(FirstPoint, SecondPoint);
        umm Diff = (MaxPoint - MinPoint);
        
        // Note: +1 for null termination
        u8 *ClipboardText = (u8 *)Allocate(Diff + 1);
        SubString(ClipboardText, FileBuffer->Memory, MinPoint, MaxPoint);
        
        SetClipboardText(ClipboardText);
        Deallocate(ClipboardText);
    }
    EndTimedPoint(CopySection);
}

umm
PasteSection(file_buffer *FileBuffer, umm BytesIn)
{
    // Note/Todo: Keep an eye on how long it takes to get rid of carriage returns
    StartTimedPoint(PasteSection);
    u32x Result = 0;
    
    u8 *Text = (u8 *)GetClipboardText();
    if(Text)
    {
        umm TextSize = StringLength(Text);
        umm BufferSize = 0;
#if 1
        u8 *Buffer = (u8 *)Allocate(TextSize + 1);
        u8 *Dest = Buffer;
        for(u8 *At = Text;
            At < Text + TextSize;
            ++At)
        {
            if(*At != '\r')
            { // Note: Don't need those nasty carriage returns...
                ++BufferSize;
                *Dest++ = *At;
            }
        }
        *Dest++ = '\0';
        Result = BufferSize;
        AddString(FileBuffer, Buffer, BytesIn, true);
        Deallocate(Dest);
#else
        BufferSize = TextSize;
        Result = TextSize;
        AddString(FileBuffer, Text, BytesIn, true);
#endif
        
        Deallocate(Text);
    }
    
    EndTimedPoint(PasteSection);
    return Result;
}

void 
UpdateAndRender(editor_state *State, editor_backbuffer *Buffer, editor_memory *Memory, r32 DeltaTime)
{
    if(!Memory->IsInitialized)
    {
        State->Mandala.Base = (u8 *)Memory->PermanentMemory;
        State->Mandala.Size = Memory->PermanentMemorySize;
        
        State->TransMandala.Base = (u8 *)Memory->TransientMemory;
        State->TransMandala.Size = Memory->TransientMemorySize;
        
        State->RenderGroup = InitRenderGroup(&State->Assets, &State->TransMandala, Mega(2));
        
        State->FileHash.FileBufferCount = 256;
        State->FileHash.FileHashArray = (file_hash_slot *)Allocate(State->FileHash.FileBufferCount*sizeof(file_hash_slot));
        
        if(StringLength(State->CommandLine) > 0)
        {
            // Todo: More thorough parsing... or at least parsing in some form
            // Note: This won't work if we're passed multiple files
            u8 Directory[MAX_PATH];
            u8 Path[MAX_PATH];
            if(State->CommandLine[0] == '"')
            { // Note: Sometimes people will pass with quotations
                u32x Length = StringLength(State->CommandLine);
                StringCopyLength(State->CommandLine, State->CommandLine + 1, Length - 1);
                State->CommandLine[Length - 2] = '\0';
            }
            
            // Todo: Make sure IsPathRelative is working!!!! make it rigorous
            if(IsPathRelative(State->CommandLine))
            {
                CurrentWorkingDirectory(Directory);
                StringCopy(Path, Directory);
                StringCat(Path, State->CommandLine);
            }
            else
            {
                StringCopy(Path, State->CommandLine);
            }
            
            file_buffer *FileBuffer = ReadFileIntoFileHash(&State->CommentKeywordHash, &State->FileHash, Path, State->Dictionary, &State->Config);
            
            State->Instances = (file_instance *)Allocate(sizeof(file_instance));
            State->InstanceCount = 1;
            
            State->Instances[0] = MakeInstanceOfFileBuffer(FileBuffer, &State->Config);
            State->Instances[0].Window = RectR32(0.0f, 0.0f,
                                                 (r32)Buffer->Width, (r32)Buffer->Height);
            
            State->InstanceWindows = 1;
        }
        else
        {
            State->Instances = (file_instance *)Allocate(sizeof(file_instance)*2);
            
#if 0
            State->Instances[0] = MakeInstanceOfFileBuffer(MakeNullFileBuffer());
            State->Instances[0].Window = RectR32(0.0f, 0.0f,
                                                 (r32)Buffer->Width/2.0f, (r32)Buffer->Height);
            
            State->Instances[1] = MakeInstanceOfFileBuffer(MakeNullFileBuffer());
            State->Instances[1].Window = RectR32((r32)Buffer->Width/2.0f, 0.0f,
                                                 (r32)Buffer->Width, (r32)Buffer->Height);
            
            State->InstanceCount = 2;
            
            State->InstanceWindows = 2;
#else
            
            State->Instances[0] = MakeNullFileInstance(&State->Config);
            PlaceFileBufferIntoHash(&State->FileHash, State->Instances[0].FileBuffer);
            
            State->Instances[0].Window = RectR32(0.0f, 0.0f,
                                                 (r32)Buffer->Width, (r32)Buffer->Height);
            
            State->InstanceCount = 1;
            
            State->InstanceWindows = 1;
#endif
        }
        
        // Todo: Window Instances?
        State->ActiveInstanceIndex = 0;
        State->Instances[State->ActiveInstanceIndex].Active = true;
        
        LoadAssets(&State->TransMandala, State);
        
        InitOpenGL();
        CompileShaders(&State->OpenGLInfo);
        
        Memory->IsInitialized = true;
    }
    ResetRenderGroup(State->RenderGroup);
    render_group *RenderGroup = State->RenderGroup;
    
    if(State->Config.ConfigReloaded)
    {
        State->Config.ConfigReloaded = false;
        
        // Note: Yes we are remaking the font everytime that the config gets reloaded/reread
        GlobalFontMandala.Used = 0;
        LoadFont(&State->Assets.CodeFont, &State->Config);
        
        // Note: This is to expand the size of the undo based on the Config file
        for(umm BufferIndex = 0;
            BufferIndex < State->FileHash.FileBufferCount;
            ++BufferIndex)
        { // Todo/testing: Not fully tested...
            file_hash_slot *FileHashSlot = (State->FileHash.FileHashArray + BufferIndex);
            
            if(FileHashSlot->File)
            {
                for(file_hash_slot *At = FileHashSlot;
                    At;
                    At = At->Next)
                {
                    file_buffer *FileBuffer = At->File;
                    
                    void *TransferMemory = FileBuffer->UndoStack.Stack;
                    FileBuffer->UndoStack.Stack = (undo_node *)Allocate(sizeof(undo_node)*State->Config.UndoMaxCount);
                    if(State->Config.UndoMaxCount < FileBuffer->UndoStack.UndoMaxCount)
                    {
                        if(FileBuffer->UndoStack.UndoCount)
                        {
                            CopyMemory(FileBuffer->UndoStack.Stack,
                                   (u8 *)TransferMemory + sizeof(undo_node)*(FileBuffer->UndoStack.UndoCount - State->Config.UndoMaxCount),
                                   FileBuffer->UndoStack.UndoMaxCount);
                            FileBuffer->UndoStack.Cursor = FileBuffer->UndoStack.UndoCount = Min(FileBuffer->UndoStack.UndoCount, State->Config.UndoMaxCount);
                        }
                    }
                    else
                    {
                        if(FileBuffer->UndoStack.UndoCount)
                        {
                            CopyMemory(FileBuffer->UndoStack.Stack, TransferMemory, sizeof(undo_node)*FileBuffer->UndoStack.UndoCount);
                        }
                    }
                    
                    FileBuffer->UndoStack.UndoMaxCount = State->Config.UndoMaxCount;
                    Deallocate(TransferMemory);
                }
            }
        }
    }
    
    bitmap BackBuffer_ = {};
    bitmap *BackBuffer = &BackBuffer_;
    BackBuffer->Memory = Buffer->Memory;
    BackBuffer->Width  = SafeTruncateToU16(Buffer->Width);
    BackBuffer->Height = SafeTruncateToU16(Buffer->Height);
    
    PushOffset(RenderGroup, V2(0.0f, 0.0f));

    { // Note: Background
        Clear(State->RenderGroup, State->Config.BackgroundColor);
    }
    
    // Intermediate drawing
    { // Note: FileBuffer rendering
        for(u32x InstanceIndex = 0;
            InstanceIndex < State->InstanceCount;
            ++InstanceIndex)
        {
            file_instance *FileInstance = State->Instances + InstanceIndex;
            
#if 0
            if(FileInstance->FileBuffer->IsBinaryFile)
            {
                PushBinaryFileInstance(RenderGroup, FileInstance, &State->CommentKeywordHash, &State->Assets.CodeFont, &State->Config, State->IsMaximized);
            }
            else
            {
                PushTextFileInstance(RenderGroup, FileInstance, &State->CommentKeywordHash, &State->Assets.CodeFont, &State->Config, State->IsMaximized);
            }
#else
            PushTextFileInstance(RenderGroup, FileInstance, &State->CommentKeywordHash, &State->Assets.CodeFont, &State->Config, DeltaTime, State->IsMaximized);

            // Note: Decrement of all paste sections by Delta Time
            for(smm Index = 0;
                Index < FileInstance->PasteLocationAssemblage.Count;
                ++Index)
            {
                paste_location_data *PasteSection = FileInstance->PasteLocationAssemblage.PasteLocationData + Index;
                
                PasteSection->Time -= DeltaTime;
                if(PasteSection->Time <= 0.0f)
                {
                    if(Index < FileInstance->PasteLocationAssemblage.Count - 1)
                    {
                        MoveMemory(FileInstance->PasteLocationAssemblage.PasteLocationData + Index, FileInstance->PasteLocationAssemblage.PasteLocationData + Index + 1, (FileInstance->PasteLocationAssemblage.Count - (Index + 1))*sizeof(paste_location_data));
                    }
                    --FileInstance->PasteLocationAssemblage.Count;
                    --Index;
                }
            }
#endif
        }
    }
    
    PushOffset(RenderGroup, V2(0.0f, 0.0f));
    
#if 0
    PushClipRect(RenderGroup, RectR32(0, 0, (r32)Buffer->Width, (r32)TitlebarHeight));
    if(!State->IsMaximized && TitlebarHeight > 0)
    {
        { // Note: Title bar
            v4 TitleBarColor = V4(22, 22, 22, 255);
            PushRect(RenderGroup, V2(0, 0), V2((r32)Buffer->Width, (r32)TitlebarHeight), TitleBarColor);
            
            v4 TitleFontColor = V4(145, 145, 145, 255);
            FormatString(State->TitleBuffer, "%s dt:%0.4f fps:%0.2f", State->LocalTime, dt, 1.0f/(dt));
            PushTextLine(State->RenderGroup, &State->Assets.TitleFont, State->TitleBuffer,
                         V2(5, TitlebarHeight/2.0f - State->Assets.TitleFont.Height/2.0f),
                         TitleFontColor);
        }
    }
#endif

    if(State->IsMaximized)
    { // Note: Window border then title bar divider
        v4 BorderColor = State->Config.BorderColor;
        
        PushRectOutline(State->RenderGroup,
                        V2(0, 0), V2((r32)Buffer->Width, (r32)Buffer->Height),
                        (r32)State->Config.BorderSize,
                        BorderColor);
#if 0
        PushRect(State->RenderGroup,
                 V2(0, (r32)TitlebarHeight), V2((r32)Buffer->Width, (r32)State->Config.BorderSize),
                 BorderColor);
#endif
    }
    
    RenderGroupToBuffer(State->RenderGroup, &State->Assets.CodeFont.FontAtlas, &State->OpenGLInfo, BackBuffer);
}

void
MoveToLastSlash(u8 *String)
{
    if(String && StringLength(String) > 0)
    {
        u8 *At = String + StringLength(String);
        for(;
            At && At > String;
            --At)
        {
            if(*At == '\\' ||
               *At == '/')
            {
                ++At;
                break;
            }
        }
        *At = '\0';
    }
}

void
MoveToSecondLastSlash(char *String)
{
    b32x FirstSlash = false;
    char *At = String + StringLength(String);
    for(;
        At && At >= String;
        --At)
    {
        if(*At == '\\' ||
           *At == '/')
        {
            if(FirstSlash)
            {
                ++At;
                break;
            }
            else
            {
                FirstSlash = true;
            }
        }
    }
    *At = '\0';
}

void
SplitWindowHorizontal(editor_state *State, file_instance *FileInstance)
{ // Todo: Debug these "alt-x" to start
    rect SecondWindow;
    SecondWindow.P1.x = FileInstance->Window.P1.x;
    SecondWindow.P1.y = (FileInstance->Window.P2.y - FileInstance->Window.P1.y) / 2;
    SecondWindow.P2.x = FileInstance->Window.P2.x;
    SecondWindow.P2.y = FileInstance->Window.P2.y;
    
    FileInstance->Window.P2.y = SecondWindow.P1.y;
    
    ++State->InstanceCount;
    State->Instances = (file_instance *)Reallocate(State->Instances,
                                                      sizeof(file_instance)*(State->InstanceCount - 1), sizeof(file_instance)*State->InstanceCount);
    ++State->InstanceWindows;
    
    file_instance CopyInstance ={0};
    
    b32x Found = false;
    for(u32x Index = 0;
        Index < State->InstanceCount;
        ++Index)
    {
        file_instance *IndexInstance = State->Instances + Index;
        
        if(Index > State->ActiveInstanceIndex)
        {
            if(CopyInstance.FileBuffer == 0)
            {
                CopyInstance = *IndexInstance;
                *IndexInstance = MakeNullFileInstance(&State->Config);
                IndexInstance->Window = SecondWindow;
            }
            else
            {
                // Copy      Index->Trans Copy->Index Trans->Copy  Cont.
                // 1stIndex  2ndIndex                              3rdIndex
                
                file_instance TransInstance = *IndexInstance;
                *IndexInstance = CopyInstance;
                CopyInstance = TransInstance;
            }
        }
    }
}

void
SplitWindowVertical(editor_state *State, file_instance *FileInstance)
{
    rect SecondWindow;
    SecondWindow.P1.x = FileInstance->Window.P1.x + (FileInstance->Window.P2.x - FileInstance->Window.P1.x) / 2;
    SecondWindow.P1.y = FileInstance->Window.P1.y;
    SecondWindow.P2.x = FileInstance->Window.P2.x;
    SecondWindow.P2.y = FileInstance->Window.P2.y;
    
    FileInstance->Window.P2.x = SecondWindow.P1.x;
    
    ++State->InstanceCount;
    State->Instances = (file_instance *)Reallocate(State->Instances,
                                                      sizeof(file_instance)*(State->InstanceCount - 1), sizeof(file_instance)*State->InstanceCount);
    ++State->InstanceWindows;
    
    file_instance CopyInstance = {};
    
    b32x Found = false;
    for(u32x Index = 0;
        Index < State->InstanceCount;
        ++Index)
    {
        file_instance *IndexInstance = State->Instances + Index;
        
        if(Index > State->ActiveInstanceIndex)
        {
            if(CopyInstance.FileBuffer == 0)
            {
                CopyInstance = *IndexInstance;
                *IndexInstance = MakeNullFileInstance(&State->Config);
                IndexInstance->Window = SecondWindow;
            }
            else
            {
                // Copy      Index->Trans Copy->Index Trans->Copy  Cont.
                // 1stIndex  2ndIndex                              3rdIndex
                
                file_instance TransInstance = *IndexInstance;
                *IndexInstance = CopyInstance;
                CopyInstance = TransInstance;
            }
        }
    }
}

inline void
ReverseIncrementalSearch(file_instance *FileInstance)
{
    if(FileInstance->FileBuffer->Size)
    {
        FileInstance->SearchAssemblage.Count = 0;
        FileInstance->SearchAssemblage.Cursor = 0;

        umm CommandLength = StringLength(FileInstance->CommandBuffer);
        u8 *EndOfFile = FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size;

        for(u8 *At = FileInstance->FileBuffer->Memory;
            At < EndOfFile;
            )
        {
            if(*At == *FileInstance->CommandBuffer && At + CommandLength < EndOfFile &&
               CompareStringLength(At, FileInstance->CommandBuffer, CommandLength))
            {
                if(FileInstance->SearchAssemblage.Cursor == 0 && FileInstance->Cursor < At - FileInstance->FileBuffer->Memory)
                {
                    FileInstance->SearchAssemblage.Cursor = FileInstance->SearchAssemblage.Count;
                }

                umm *BytesIn = (umm *)ArrayInsert(FileInstance->SearchAssemblage.WordLocation,
                                                  FileInstance->SearchAssemblage.Count, FileInstance->SearchAssemblage.MaxCount);

                *BytesIn = At - FileInstance->FileBuffer->Memory;

                At += CommandLength;
            }
            else
            {
                ++At;
            }
        }

        if(FileInstance->SearchAssemblage.Count)
        {
            // Todo: Why was this here?
            // FileInstance->SearchAssemblage.Cursor = FileInstance->SearchAssemblage.Count;

            FileInstance->Cursor = *(FileInstance->SearchAssemblage.WordLocation + FileInstance->SearchAssemblage.Cursor - 1);
        }

        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                     "Reverse-I-Search %lld/%lld ",
                     FileInstance->SearchAssemblage.Cursor, FileInstance->SearchAssemblage.Count);
    }
}

inline void
IncrementalSearch(file_instance *FileInstance)
{
    if(FileInstance->FileBuffer->Size)
    {
        FileInstance->SearchAssemblage.Count = 0;
        FileInstance->SearchAssemblage.Cursor = 0;

        umm CommandLength = StringLength(FileInstance->CommandBuffer);
        u8 *EndOfFile = FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size;

        for(u8 *At = FileInstance->FileBuffer->Memory;
            At < EndOfFile;
            )
        {
            if(*At == *FileInstance->CommandBuffer && At + CommandLength < EndOfFile &&
                CompareStringLength(At, FileInstance->CommandBuffer, CommandLength))
            {
                if(FileInstance->SearchAssemblage.Cursor == 0 && FileInstance->Cursor <= At - FileInstance->FileBuffer->Memory)
                {
                    FileInstance->SearchAssemblage.Cursor = FileInstance->SearchAssemblage.Count + 1;
                }

                umm *BytesIn = (umm *)ArrayInsert(FileInstance->SearchAssemblage.WordLocation,
                                                  FileInstance->SearchAssemblage.Count, FileInstance->SearchAssemblage.MaxCount);

                *BytesIn = At - FileInstance->FileBuffer->Memory;

                At += CommandLength;
            }
            else
            {
                ++At;
            }
        }

        if(FileInstance->SearchAssemblage.Count)
        {
            // Todo: Why was this here?
            // FileInstance->SearchAssemblage.Cursor = FileInstance->SearchAssemblage.Count;

            FileInstance->Cursor = *(FileInstance->SearchAssemblage.WordLocation + FileInstance->SearchAssemblage.Cursor - 1);
        }

        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                     "I-Search %lld/%lld ",
                     FileInstance->SearchAssemblage.Cursor, FileInstance->SearchAssemblage.Count);
    }
}

inline void
IncrementalQuerySearch(file_instance *FileInstance)
{
    if(FileInstance->FileBuffer->Size)
    {
        FileInstance->SearchAssemblage.Count = 0;
        FileInstance->SearchAssemblage.Cursor = 0;

        umm ReplaceeLength = StringLength(FileInstance->QueryReplacee);
        u8 *EndOfFile = FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size;

        for(u8 *At = FileInstance->FileBuffer->Memory;
            At < EndOfFile;
            )
        {
            if(*At == *FileInstance->QueryReplacee && At + ReplaceeLength < EndOfFile &&
               CompareStringLength(At, FileInstance->QueryReplacee, ReplaceeLength))
            {
                if(FileInstance->SearchAssemblage.Cursor == 0 && FileInstance->Cursor <= At - FileInstance->FileBuffer->Memory)
                {
                    FileInstance->SearchAssemblage.Cursor = FileInstance->SearchAssemblage.Count + 1;
                }

                umm *BytesIn = (umm *)ArrayInsert(FileInstance->SearchAssemblage.WordLocation,
                                                  FileInstance->SearchAssemblage.Count, FileInstance->SearchAssemblage.MaxCount);

                *BytesIn = At - FileInstance->FileBuffer->Memory;

                At += ReplaceeLength;
            }
            else
            {
                ++At;
            }
        }

        if(FileInstance->SearchAssemblage.Count)
        {
            // Todo: Why was this here?
            // FileInstance->SearchAssemblage.Cursor = FileInstance->SearchAssemblage.Count;

            FileInstance->Cursor = *(FileInstance->SearchAssemblage.WordLocation + FileInstance->SearchAssemblage.Cursor - 1);
            FileInstance->CursorBias = GetCursorBias(FileInstance);
        }

        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                     "Replace: %s with: %s %lld/%lld ",
                     FileInstance->QueryReplacee, FileInstance->QueryReplacer,
                     FileInstance->SearchAssemblage.Cursor, FileInstance->SearchAssemblage.Count);
    }
}

#define Key_Escape    0x1b
#define Key_Space     0x20
#define Key_Left      0x25
#define Key_Up        0x26
#define Key_Right     0x27
#define Key_Down      0x28
#define Key_Insert    0x2D
#define Key_End       0x23
#define Key_Home      0x24
#define Key_Delete    0x2E
#define Key_Backspace 0x08
#define Key_Tab       0x09
#define Key_Enter     0x0D
#define Key_F1        0x70
#define Key_F2        0x71
#define Key_F3        0x72
#define Key_F4        0x73
#define Key_F5        0x74
#define Key_F6        0x75
#define Key_F7        0x76
#define Key_F8        0x77

void 
KeyDown(editor_state *State, u8 Key, u8 VK, b32 ShiftIsDown, b32 ControlIsDown, b32 AltIsDown, b32 WindowsIsDown)
{ // Todo: System for mapping keys?
    if(VK == Key_F4)
    {
        State->Running = false;
        return;
    }
    
    if(VK == Key_Tab && AltIsDown)
    {
        return;
    }
    
    config *Config = &State->Config;

    // Todo: Get focused file buffer
    file_instance *FileInstance = GetActiveFileInstance(State->Instances, State->InstanceCount);
    if(!FileInstance)
    {
        return;
    }

    // Note: Reset the undo stack's cursor
    if(FileInstance && (Key != 'z' || !ControlIsDown))
    {
        FileInstance->FileBuffer->UndoStack.Cursor = FileInstance->FileBuffer->UndoStack.UndoCount - 1;
    }
    

    if(VK == Key_F3)
    {
        if(FileInstance->CommandType == CommandType_QueryReplace)
        {
            if(FileInstance->SearchAssemblage.Count)
            {
                if(++FileInstance->SearchAssemblage.Cursor > FileInstance->SearchAssemblage.Count)
                {
                    FileInstance->SearchAssemblage.Cursor = 1;
                }

                FileInstance->Cursor = *(FileInstance->SearchAssemblage.WordLocation + FileInstance->SearchAssemblage.Cursor - 1);

                FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                             "Replace: %s with: %s %lld/%lld ",
                             FileInstance->QueryReplacee, FileInstance->QueryReplacer,
                             FileInstance->SearchAssemblage.Cursor, FileInstance->SearchAssemblage.Count);
            }
        }
    }
    else if(VK == Key_Right)
    {
        if(!FileInstance->CommandFocus)
        {
            if(ControlIsDown)
            {
                if(FileInstance->FileBuffer->Language == Language_None)
                {
                    AdvanceWord(FileInstance);
                }
                else
                {
                    AdvanceLexicalWord(FileInstance);
                }
            }
            else
            {
#if 0
                if((umm)FileInstance->Cursor + 1 < FileInstance->FileBuffer->Size &&
                   *(FileInstance->FileBuffer->Memory + FileInstance->Cursor + 1) == '\r' && (umm)FileInstance->Cursor + 2 < FileInstance->FileBuffer->Size &&
                   *(FileInstance->FileBuffer->Memory + FileInstance->Cursor + 2) == '\n')
                {
                    ++FileInstance->Cursor;
                }
                else if(*(FileInstance->FileBuffer->Memory + FileInstance->Cursor)     == '\r' && (umm)FileInstance->Cursor + 1 < FileInstance->FileBuffer->Size &&
                        *(FileInstance->FileBuffer->Memory + FileInstance->Cursor + 1) == '\n')
                {
                    ++FileInstance->Cursor;
                }
#else
                if(FileInstance->FileBuffer->Size && (umm)FileInstance->Cursor + 1 < FileInstance->FileBuffer->Size &&
                   *(FileInstance->FileBuffer->Memory + FileInstance->Cursor)     == '\r' &&
                   *(FileInstance->FileBuffer->Memory + FileInstance->Cursor + 1) == '\n')
                {
                    ++FileInstance->Cursor;
                }
#endif
                FileInstance->Cursor = (u32x)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size - 1, -1) + 1;
            }
            
            FileInstance->CursorBias = GetCursorBias(FileInstance);
        }
    }
    else if(VK == Key_Left)
    {
        if(!FileInstance->CommandFocus)
        {
            if(ControlIsDown)
            {
                if(FileInstance->FileBuffer->Language == Language_None)
                {
                    PrecursorWord(FileInstance);
                }
                else
                {
                    PrecursorLexicalWord(FileInstance);
                }
            }
            else
            {
                if(FileInstance->FileBuffer->Size >= 2 && FileInstance->Cursor - 1 > 0 &&
                   *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\n' && FileInstance->Cursor - 2 >= 0 &&
                   *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 2) == '\r')
                {
                    --FileInstance->Cursor;
                }
                else if(FileInstance->FileBuffer->Size >= 2 &&
                        *(FileInstance->FileBuffer->Memory + FileInstance->Cursor)     == '\n' && FileInstance->Cursor > 0 &&
                        *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\r')
                {
                    --FileInstance->Cursor;
                }
                FileInstance->Cursor = (u32x)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size + 1, 1) - 1;
            }
            
            FileInstance->CursorBias = GetCursorBias(FileInstance);
        }
    }
    else if(VK == Key_Up)
    {
        if(ControlIsDown)
        {
            MoveToPrevParagraph(FileInstance);
        }
        else
        {
            if(!FileInstance->CommandFocus || FileInstance->CommandType == CommandType_Open)
            {
                MoveCursorUpLine(FileInstance, Config->LineWrapping);
                
                if(FileInstance->CommandType == CommandType_Open)
                {
                    MoveToLastSlash(FileInstance->CommandBuffer);
                    
                    u8 Item[MAX_PATH];
                    
                    GetCursorLineCharacters(FileInstance, Item);
                    
                    u8 *ItemName = Item + 2;
                    
                    StringCat(FileInstance->CommandBuffer, ItemName);
                }
            }
        }
    }
    else if(VK == Key_Down)
    {
        if(ControlIsDown)
        {
            MoveToNextParagraph(FileInstance);
        }
        else
        {
            if(!FileInstance->CommandFocus || FileInstance->CommandType == CommandType_Open)
            {
                MoveCursorDownLine(FileInstance, Config->LineWrapping);
                
                if(FileInstance->CommandType == CommandType_Open)
                {
                    MoveToLastSlash(FileInstance->CommandBuffer);
                    
                    u8 Item[MAX_PATH];
                    
                    GetCursorLineCharacters(FileInstance, Item);
                    
                    u8 *ItemName = Item + 2;
                    
                    StringCat(FileInstance->CommandBuffer, ItemName);
                }
            }
        }
    }
    else if(VK == Key_Insert)
    {
        FileInstance->Insert = !FileInstance->Insert;
    }
    else if(VK == Key_Home)
    {
        if(ControlIsDown)
        {
            FileInstance->Cursor = 0;
        }
        else
        {
            FileInstance->Cursor = BeginningOfCursorLine(FileInstance);
        }
        
        FileInstance->CursorBias = GetCursorBias(FileInstance);
    }
    else if(VK == Key_End)
    {
        if(ControlIsDown)
        {
            FileInstance->Cursor = FileInstance->FileBuffer->Size;
        }
        else
        {
            FileInstance->Cursor = EndOfCursorLine(FileInstance);
            if(FileInstance->FileBuffer->Memory[FileInstance->Cursor] == '\n' &&
               FileInstance->FileBuffer->Memory[FileInstance->Cursor - 1] == '\r')
            {
                --FileInstance->Cursor;
            }
        }
        
        FileInstance->CursorBias = GetCursorBias(FileInstance);
    }
    else if(VK == Key_Delete)
    {
        if(FileInstance->CommandFocus)
        {
        }
        else
        {
            if(ControlIsDown)
            {
                token *Token = GetTokenAtPoint(FileInstance->FileBuffer, FileInstance->Cursor, 0);
                if(Token)
                {
                    if(Token->Type == Token_Whitespace && TokenContainsNewline(FileInstance->FileBuffer, Token))
                    {
                        u8 *At;
                        for(At = FileInstance->FileBuffer->Memory + FileInstance->Cursor + 1;
                            At < FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size && *At;
                            ++At)
                        {
                            if(*At == '\n' || !IsWhitespace(*At))
                            {
                                break;
                            }
                        }
                        smm SecondPoint = At - FileInstance->FileBuffer->Memory;
                        DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor, SecondPoint, true);
                    }
                    else if(Token->Type == Token_Literal || Token->Type == Token_Comment)
                    {
                        umm FirstPoint = FileInstance->Cursor;
                        AdvanceWord(FileInstance);
                        DeleteSection(FileInstance->FileBuffer, FirstPoint, FileInstance->Cursor, true);
                        FileInstance->Cursor = FirstPoint;
                    }
                    else
                    {
                        DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor, Token->BytesIn + Token->BytesLong, true);
                    }
                }
            }
            else
            {
                if(RoundS32xToU32x(FileInstance->Cursor) < FileInstance->FileBuffer->Size)
                {
                    if(*(FileInstance->FileBuffer->Memory + FileInstance->Cursor) == '\r' && 
                       *(FileInstance->FileBuffer->Memory + FileInstance->Cursor + 1) == '\n')
                    {
                        DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor, FileInstance->Cursor + 2, true);
                    }
                    else if(*(FileInstance->FileBuffer->Memory + FileInstance->Cursor) == '\n' && 
                            *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\r')
                    {
                        Assert(!"This shouldn't really happen, going to throw an error for now, since we should be on the \r");
                        DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor - 1, FileInstance->Cursor + 1, true);
                    }
                    else
                    {
                        DeleteCharacter(FileInstance->FileBuffer, FileInstance->Cursor, true);
                    }

                    if(FileInstance->MarkerIsDown)
                    { // Todo: broadcast this to other FileInstances with the same file buffer?
                        if(FileInstance->Cursor < FileInstance->Marker)
                        {
                            FileInstance->Marker = (u32x)ClampS64(FileInstance->Marker, FileInstance->FileBuffer->Size + 1, 1) - 1;
                        }
                    }

                }
            }

            ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
        }
    }
    else if(VK == Key_Backspace)
    {
        if(FileInstance->CommandFocus)
        {
            smm StringSize = (smm)StringLength(FileInstance->CommandBuffer);
            if(StringSize)
            {
                if(ControlIsDown)
                {
                    enum
                    {
                        Word,
                        Seperator, // For cases like: "..\..\..\" where we want to distinguish between the '.' and '\'
                        Whitespace,
                        Grammatical
                    };
                    int TokenType = 0;
                    char Character = *(FileInstance->CommandBuffer + StringSize - 1);
                    b32 OpenFileHack = (FileInstance->CommandType == CommandType_Open) ? true : false;

                    if(IsAlphabetical(Character) || IsNumeric(Character))
                    {
                        TokenType = Word;
                    }
                    else if(IsWhitespace(Character))
                    {
                        TokenType = Whitespace;
                    }
                    else if(Character == '/' || Character == '\\')
                    {
                        TokenType = Seperator;
                    }
                    else
                    {
                        TokenType = Grammatical;
                    }

                    smm Scan = 1;
                    int CheckedTokenType = 0;
                    while(Scan < StringSize)
                    {
                        ++Scan;
                        Character = *(FileInstance->CommandBuffer + StringSize - Scan);

                        if(IsAlphabetical(Character) || IsNumeric(Character))
                        {
                            CheckedTokenType = Word;
                        }
                        else if(IsWhitespace(Character))
                        {
                            CheckedTokenType = Whitespace;
                        }
                        else if(Character == '/' || Character == '\\')
                        {
                            if(OpenFileHack)
                            {
                                --Scan;
                                break;
                            }
                            CheckedTokenType = Seperator;
                        }
                        else
                        {
                            CheckedTokenType = Grammatical;
                        }

                        if(CheckedTokenType != TokenType)
                        {
                            if(!OpenFileHack)
                            {
                                --Scan;
                                break;
                            }
                        }
                    }

                    SubString(FileInstance->CommandBuffer, FileInstance->CommandBuffer, 0, StringSize - Scan);
                }
                else
                {
                    SubString(FileInstance->CommandBuffer, FileInstance->CommandBuffer, 0, StringSize - 1);
                }
            }
            
            if(FileInstance->CommandType == CommandType_Open)
            {
                FileInstance->FileBuffer->Memory = GetDirectoryListing(&State->FileHash, FileInstance->CommandBuffer);
                FileInstance->FileBuffer->Size = StringLength(FileInstance->FileBuffer->Memory);
                FileInstance->Cursor = 0;
            }
            else if(FileInstance->CommandType == CommandType_ForwardSearch)
            {
                IncrementalSearch(FileInstance);
            }
            else if(FileInstance->CommandType == CommandType_ReverseSearch)
            {
                ReverseIncrementalSearch(FileInstance);
            }
        }
        else
        {
            if(FileInstance->Cursor > 0)
            {
                if(ControlIsDown)
                {
                    if(FileInstance->FileBuffer->Language == Language_None || !FileInstance->FileBuffer->LexerAssemblage.Count)
                    {
                        umm SecondPoint = FileInstance->Cursor;
                        PrecursorWord(FileInstance);
                        DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor, SecondPoint, true);
                    }
                    else
                    {
                        token *Token = GetTokenAtPoint(FileInstance->FileBuffer, FileInstance->Cursor - 1, 0);
                        if(Token->Type == Token_Whitespace && TokenContainsNewline(FileInstance->FileBuffer, Token))
                        {
                            umm CursorLine = GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor);
                            u8 *Line = GetLinePointer(FileInstance->FileBuffer, CursorLine, 0);
                            smm BytesIn = ((Line - 1) - FileInstance->FileBuffer->Memory);
                            smm Difference = FileInstance->Cursor - BytesIn;
                            DeleteSection(FileInstance->FileBuffer, BytesIn, FileInstance->Cursor, true);
                            FileInstance->Cursor -= Difference;
                        }
                        else if(Token->Type == Token_Whitespace)
                        {
                            token *PrevToken = Token - 1;
                            if(PrevToken > FileInstance->FileBuffer->LexerAssemblage.Tokens)
                            {
                                smm Difference = FileInstance->Cursor - PrevToken->BytesIn;
                                DeleteSection(FileInstance->FileBuffer, PrevToken->BytesIn, FileInstance->Cursor, true);
                                FileInstance->Cursor -= Difference;
                            }
                            else
                            {
                                smm Difference = FileInstance->Cursor;
                                DeleteSection(FileInstance->FileBuffer, 0, FileInstance->Cursor, true);
                                FileInstance->Cursor -= Difference;
                            }
                        }
                        else if(Token->Type == Token_Comment || Token->Type == Token_Literal)
                        {
                            umm SecondPoint = FileInstance->Cursor;
                            PrecursorWord(FileInstance);
                            u8 *At = FileInstance->FileBuffer->Memory + FileInstance->Cursor;
                            if(*At == '"' && *(FileInstance->FileBuffer->Memory + SecondPoint - 1) != '"')
                            {
                                ++FileInstance->Cursor;
                            }
                            DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor, SecondPoint, true);
                        }
                        else
                        {
                            smm Difference = FileInstance->Cursor - Token->BytesIn;
                            DeleteSection(FileInstance->FileBuffer, Token->BytesIn, FileInstance->Cursor, true);
                            FileInstance->Cursor -= Difference;
                        }
                    }

                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
                else
                {
                    if(FileInstance->MarkerIsDown)
                    { // Todo: broadcast this to other FileInstances with the same file buffer?
                        if(FileInstance->Cursor <= FileInstance->Marker)
                        {
                            FileInstance->Marker = (smm)ClampS64(FileInstance->Marker, FileInstance->FileBuffer->Size + 1, 1) - 1;
                        }
                    }

                    if(*(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\n' &&
                       FileInstance->Cursor > 1 && *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 2) == '\r')
                    {
                        DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor - 2, FileInstance->Cursor, true);
                        FileInstance->Cursor = (smm)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size + 1, 1) - 2;
                    }
                    else
                    {
                        DeleteCharacter(FileInstance->FileBuffer, FileInstance->Cursor - 1, true);
                        FileInstance->Cursor = (smm)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size + 1, 1) - 1;
                    }

                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
            }
        }
    }
    else if(VK == Key_Enter)
    {
        if(FileInstance->CommandFocus)
        {
            if(FileInstance->CommandType == CommandType_Open)
            {
                u8 Item[MAX_PATH];
                b32x IsDirectory = false;
                b32x IsFile = false;
                b32x IsLoadedFile = false;
                b32x WillCreateFile = false;
                
                u8 *ItemName = Item + 2;
                
                GetCursorLineCharacters(FileInstance, Item);
                if(*(Item) == '-' && *(Item + 1) == '>')
                {
                    IsDirectory = true;
                }
                else if(*(Item) == '#')
                {
                    IsLoadedFile = true;
                }
                else
                {
                    u8 Filename[MAX_PATH] = "";
                    FromLastSlash(Filename, FileInstance->CommandBuffer);
                    if(!CompareString(Filename, ItemName))
                    {
                        WillCreateFile = true;
                    }
                    else
                    {
                        IsFile = true;
                    }
                }
                
                if(WillCreateFile)
                {
                    file_buffer *FileBuffer = ReadFileIntoFileHash(&State->CommentKeywordHash, &State->FileHash, FileInstance->CommandBuffer, State->Dictionary, &State->Config);
                    
                    rect CarryOverWindow = FileInstance->Window;
                    DeconstructAssemblages(FileInstance->FileBuffer);

                    *FileInstance = MakeInstanceOfFileBuffer(FileBuffer, &State->Config);

                    FileInstance->Window = CarryOverWindow;
                    FileInstance->Active = true;
                    if(FileInstance->Buffer)
                    {
                        Deallocate(FileInstance->Buffer);
                    }
                }
                else if(IsFile)
                {
                    MoveToLastSlash(FileInstance->CommandBuffer);
                    StringCat(FileInstance->CommandBuffer, ItemName);

                    rect CarryOverWindow = FileInstance->Window;
                    DeconstructAssemblages(FileInstance->FileBuffer);

                    file_buffer *FileBuffer = ReadFileIntoFileHash(&State->CommentKeywordHash, &State->FileHash, FileInstance->CommandBuffer, State->Dictionary, &State->Config);

                    *FileInstance = MakeInstanceOfFileBuffer(FileBuffer, &State->Config);
                    FileInstance->Active = true;
                    FileInstance->Window = CarryOverWindow;

                    if(FileInstance->Buffer)
                    {
                        Deallocate(FileInstance->Buffer);
                    }
                }
                else if(IsLoadedFile)
                {
                    MoveToLastSlash(FileInstance->CommandBuffer);
                    StringCat(FileInstance->CommandBuffer, ItemName);
                    
                    file_buffer *FileBuffer = GetFileBufferFromHash(&State->FileHash, FileInstance->CommandBuffer);
                    
                    rect CarryOverWindow = FileInstance->Window;
                    *FileInstance = MakeInstanceOfFileBuffer(FileBuffer, &State->Config);
                    FileInstance->Window = CarryOverWindow;
                    FileInstance->Active = true;

                    if(FileInstance->Buffer)
                    {
                        Deallocate(FileInstance->Buffer);
                    }
                }
                else // Note: IsDirectory
                {
                    MoveToLastSlash(FileInstance->CommandBuffer);
                    // Todo: If ItemName == ".." Delete to the last back slash
                    ItemName = Item + 2;
                    
                    u32x ItemNameLength = StringLength(ItemName);
                    *(ItemName + ItemNameLength) = '\\';
                    *(ItemName + ItemNameLength + 1) = '\0';
                    
                    FileInstance->Cursor = 0;
                    StringCat(FileInstance->CommandBuffer, ItemName);
                    FileInstance->FileBuffer->Memory = GetDirectoryListing(&State->FileHash, FileInstance->CommandBuffer);
                    FileInstance->FileBuffer->Size = StringLength(FileInstance->FileBuffer->Memory);

                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
            }
            else if(FileInstance->CommandType == CommandType_ForwardSearch)
            {
                if(FileInstance->FileBuffer->Size)
                {
                    if(FileInstance->SearchAssemblage.Count)
                    {
                        if(++FileInstance->SearchAssemblage.Cursor > FileInstance->SearchAssemblage.Count)
                        {
                            FileInstance->SearchAssemblage.Cursor = 1;
                        }

                        FileInstance->Cursor = *(FileInstance->SearchAssemblage.WordLocation + FileInstance->SearchAssemblage.Cursor - 1);

                        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                                     "I-Search %lld/%lld ",
                                     FileInstance->SearchAssemblage.Cursor, FileInstance->SearchAssemblage.Count);
                    }
                }
            }
            else if(FileInstance->CommandType == CommandType_ReverseSearch)
            {
                if(FileInstance->SearchAssemblage.Count)
                {
                    if(!--FileInstance->SearchAssemblage.Cursor)
                    {
                        FileInstance->SearchAssemblage.Cursor = FileInstance->SearchAssemblage.Count;
                    }

                    FileInstance->Cursor = *(FileInstance->SearchAssemblage.WordLocation + FileInstance->SearchAssemblage.Cursor - 1);

                    FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                                 "Reverse-I-Search %lld/%lld ",
                                 FileInstance->SearchAssemblage.Cursor, FileInstance->SearchAssemblage.Count);
                }
            }
            else if(FileInstance->CommandType == CommandType_QueryReplace)
            {
                if(StringLength(FileInstance->QueryReplacer))
                {
                    if(FileInstance->SearchAssemblage.Count)
                    {
                        if(++FileInstance->SearchAssemblage.Cursor > FileInstance->SearchAssemblage.Count)
                        {
                            FileInstance->SearchAssemblage.Cursor = 1;
                        }

                        FileInstance->Cursor = *(FileInstance->SearchAssemblage.WordLocation + FileInstance->SearchAssemblage.Cursor - 1);

                        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                                     "Replace: %s with: %s %lld/%lld ",
                                     FileInstance->QueryReplacee, FileInstance->QueryReplacer,
                                     FileInstance->SearchAssemblage.Cursor, FileInstance->SearchAssemblage.Count);
                    }
                }
                else if(StringLength(FileInstance->CommandBuffer))
                {
                    if(StringLength(FileInstance->QueryReplacee))
                    {
                        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate, "%s%s ",
                                     FileInstance->CommandPredicate, FileInstance->CommandBuffer);
                        StringCopy(FileInstance->QueryReplacer, FileInstance->CommandBuffer);
                        StringCopy(FileInstance->CommandBuffer, "");

                        IncrementalQuerySearch(FileInstance);
                    }
                    else
                    {
                        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate, "Replace: %s with: ",
                                     FileInstance->CommandBuffer);
                        StringCopy(FileInstance->QueryReplacee, FileInstance->CommandBuffer);
                        StringCopy(FileInstance->CommandBuffer, "");
                    }
                }
            }
            else if(FileInstance->CommandType == CommandType_SelectionQueryReplace)
            {
                if(StringLength(FileInstance->CommandBuffer))
                {
                    if(StringLength(FileInstance->QueryReplacee))
                    {
                        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate, "%s%s",
                                     FileInstance->CommandPredicate, FileInstance->CommandBuffer);
                        StringCopy(FileInstance->QueryReplacer, FileInstance->CommandBuffer);
                        StringCopy(FileInstance->CommandBuffer, "");

                        umm MaxPoint = Max(FileInstance->Cursor, FileInstance->Marker);
                        umm QueryReplacerLength = StringLength(FileInstance->QueryReplacer);
                        umm QueryReplaceeLength = StringLength(FileInstance->QueryReplacee);
                        umm OccuranceCount = 0;

                        for(u8 *At = FileInstance->FileBuffer->Memory + Min(FileInstance->Cursor, FileInstance->Marker);
                            (At < FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size) && (At <= FileInstance->FileBuffer->Memory + MaxPoint - QueryReplaceeLength);
                            )
                        {
                            if(CompareStringLength(At, FileInstance->QueryReplacee, QueryReplaceeLength))
                            {
                                umm AtPosition = At - FileInstance->FileBuffer->Memory;
                                DeleteSection(FileInstance->FileBuffer, AtPosition, AtPosition + QueryReplaceeLength, true);
                                AddString(FileInstance->FileBuffer, FileInstance->QueryReplacer, AtPosition, true);
                                At = FileInstance->FileBuffer->Memory + AtPosition + QueryReplacerLength;
                                ++OccuranceCount;
                            }
                            else
                            {
                                ++At;
                            }
                        }

                        FileInstance->CommandFocus = false;
                        FileInstance->CommandType = CommandType_None;
                        StringCopy(FileInstance->CommandBuffer, "");
                        StringCopy(FileInstance->CommandPredicate, "");
                        StringCopy(FileInstance->QueryReplacee, "");
                        StringCopy(FileInstance->QueryReplacer, "");

                        FileInstance->Cursor += (QueryReplacerLength-QueryReplaceeLength)*OccuranceCount;

                        ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                    }
                    else
                    {
                        FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate, "Selection Replace: %s with: ",
                                     FileInstance->CommandBuffer);
                        StringCopy(FileInstance->QueryReplacee, FileInstance->CommandBuffer);
                        StringCopy(FileInstance->CommandBuffer, "");
                    }
                }
            }
            else if(FileInstance->CommandType == CommandType_GoToLine)
            {
                umm Line = AsciiToInteger(FileInstance->CommandBuffer);

                if(Line > 0 && Line <= FileInstance->FileBuffer->LineAssemblage.Count)
                {
                    umm BytesIn = (FileInstance->FileBuffer->LineAssemblage.LineCache + Line - 1)->BytesIn;
                    FileInstance->Cursor = BytesIn;
                }

                StringCopy(FileInstance->CommandPredicate, "");
                StringCopy(FileInstance->CommandBuffer, "");
                FileInstance->CommandFocus = false;
            }
            else if(FileInstance->CommandType == CommandType_None)
            { // Todo: Parse?
                if(CompareString(FileInstance->CommandBuffer, "save") ||
                   CompareString(FileInstance->CommandBuffer, "Save"))
                {
                    SaveBufferToFile(FileInstance);
                    StringCopy(FileInstance->CommandPredicate, "");
                    StringCopy(FileInstance->CommandBuffer, "");
                    FileInstance->CommandFocus = false;
                }
                else if(CompareString(FileInstance->CommandBuffer, "exit") ||
                        CompareString(FileInstance->CommandBuffer, "Exit"))
                {
                    State->Running = false;
                }
                else
                {
                    StringCopy(FileInstance->CommandPredicate, "Command Unknown ");
                    StringCopy(FileInstance->CommandBuffer, "");
                }
            }
        }
        else
        {
            if(ControlIsDown)
            {
                
            }
            else if(FileInstance->Insert)
            {
                if(FileInstance->Cursor > 0 &&
                   RoundS32xToU32x(FileInstance->Cursor) < FileInstance->FileBuffer->Size)
                {
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor) = '\n';
                }
            }
            else
            {
                if(FileInstance->FileBuffer->LineEnding == LineEnding_Dos)
                {
                    AddCharacter(FileInstance->FileBuffer, '\n', FileInstance->Cursor, true);
                    AddCharacter(FileInstance->FileBuffer, '\r', FileInstance->Cursor, true);
                    FileInstance->Cursor = (smm)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size - 2, -2) + 2;
                }
                else
                {
                    AddCharacter(FileInstance->FileBuffer, '\n', FileInstance->Cursor, true);
                    FileInstance->Cursor = (smm)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size - 1, -1) + 1;
                }
                
                b32 IndentOnEnter = true;
                u32x IndentAmount = 0;
                u8 *AddToString = 0;
                if(IndentOnEnter)
                {
                    IndentAmount = GetIndentationAmountAtPoint(FileInstance->FileBuffer, FileInstance->Cursor);
                    AddToString = (u8 *)Allocate(IndentAmount + 1);
                    StringNCharCopy(AddToString, ' ', IndentAmount);
                    *(AddToString + IndentAmount) = '\0';
                    
                    AddString(FileInstance->FileBuffer, AddToString, FileInstance->Cursor, true);
                }
                
                if(FileInstance->MarkerIsDown)
                { // Todo: broadcast this to other FileInstances with the same file buffer?
                    if(FileInstance->Cursor <= FileInstance->Marker)
                    {
                        FileInstance->Marker = (smm)ClampS64(FileInstance->Marker, FileInstance->FileBuffer->Size - (s32x)IndentAmount - 1, -(s32x)IndentAmount - 1) + IndentAmount + 1;
                    }
                }
                
                if(IndentOnEnter)
                {
                    FileInstance->Cursor = (smm)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size - (s32)IndentAmount, -(s32x)IndentAmount) + IndentAmount;
                    
                    if(AddToString)
                    {
                        Deallocate(AddToString);
                    }
                }
                
                FileInstance->CursorBias = GetCursorBias(FileInstance);
                ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
            }
        }
    }
    else if(VK == Key_Tab)
    {
        if(FileInstance->CommandFocus)
        {
            
        }
        else
        {
            b32 MatchesFound = false;
            if(!ControlIsDown)
            {
                u8 Prefix[1024] = "";

                token *Token = GetTokenAtPoint(FileInstance->FileBuffer, FileInstance->Cursor, 0);
                if(Token && (Token->Type != Token_Identifier || Token->Type != Token_Keyword))
                {
                    if(Token - 1 >= FileInstance->FileBuffer->LexerAssemblage.Tokens &&
                       ((Token - 1)->Type == Token_Identifier || (Token - 1)->Type == Token_Keyword))
                    {
                        Token = Token - 1;
                    }
                }

                if(Token && (Token->Type == Token_Identifier || Token->Type == Token_Keyword))
                {
                    if(!State->WordListIsValid)
                    {
                        GetTokenString(FileInstance->FileBuffer, Token, Prefix);

                        if(State->WordList.Count)
                        {
                            Deallocate(State->WordList.Words);
                            Deallocate(State->WordList.WordLocations);
                            State->WordList.Words = 0;
                            State->WordList.WordLocations = 0;
                            State->WordList.Count = 0;
                            State->WordList.Size = 0;
                            State->WordList.Cursor = 0;
                        }

                        trie_node *TempDictionary = 0;
                        switch(FileInstance->FileBuffer->Language)
                        {
                            case Language_Jai:
                            {
                                TempDictionary = &State->Dictionary[Dictionary_jai];
                            } break;
                            case Language_Odin:
                            {
                                TempDictionary = &State->Dictionary[Dictionary_odin];
                            } break;
                            case Language_Cpp:
                            {
                                TempDictionary = &State->Dictionary[Dictionary_cpp];
                            } break;
                            default:
                            {
                                TempDictionary = &State->Dictionary[Dictionary_normal];
                            } break;
                        }

                        ListWordsFromPrefix(TempDictionary, Prefix, &State->WordList);
                        State->WordListIsValid = true;
                    }

                    if(State->WordList.Count > 1)
                    {
                        umm Index = ++State->WordList.Cursor%State->WordList.Count;
                        State->WordList.Cursor = Index;
                        u8 *Word = State->WordList.Words + State->WordList.WordLocations[Index];

                        DeleteSection(FileInstance->FileBuffer, Token->BytesIn, Token->BytesIn + Token->BytesLong, true);
                        AddString(FileInstance->FileBuffer, Word, Token->BytesIn, true);

                        FileInstance->Cursor = Token->BytesIn + Token->BytesLong + (StringLength(Word) - Token->BytesLong);
                        FileInstance->CursorBias = GetCursorBias(FileInstance);

                        ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                        MatchesFound = true;
                    }
                }
            }

            if(ControlIsDown || !MatchesFound)
            {
                if(FileInstance->MarkerIsDown)
                {
                    u32x FirstLineNumber = GetLineAtBytesIn(FileInstance->FileBuffer, Min(FileInstance->Cursor, FileInstance->Marker));
                    ReindentLine(FileInstance->FileBuffer, FirstLineNumber);
                    
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);

                    umm CursorLine = GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor);

                    u32x MaxBytesIn = GetLineAtBytesIn(FileInstance->FileBuffer, Max(FileInstance->Cursor, FileInstance->Marker));
                    for(u32x AtLine = FirstLineNumber + 1;
                        AtLine <= MaxBytesIn;
                        ++AtLine)
                    {
                        ReindentLine(FileInstance->FileBuffer, AtLine);
                        ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                    }
                    
                    umm CursorLineBytesIn = (FileInstance->FileBuffer->LineAssemblage.LineCache + CursorLine - 1)->BytesIn;
                    umm LineLength = GetLineLength(FileInstance->FileBuffer, CursorLine);
                    FileInstance->Cursor = (FileInstance->CursorBias > (smm)LineLength) ? CursorLineBytesIn + LineLength - 1 : CursorLineBytesIn + FileInstance->CursorBias - 1;

                    FileInstance->MarkerIsDown = false;
                }
                else
                {
                    u32x CursorLine = GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor);
                    ReindentLine(FileInstance->FileBuffer, CursorLine);

                    umm CursorLineBytesIn = (FileInstance->FileBuffer->LineAssemblage.LineCache + CursorLine - 1)->BytesIn;
                    umm LineLength = GetLineLength(FileInstance->FileBuffer, CursorLine);
                    FileInstance->Cursor = (FileInstance->CursorBias > (smm)LineLength) ? CursorLineBytesIn + LineLength - 1 : CursorLineBytesIn + FileInstance->CursorBias - 1;

                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
            }
        }
    }
    else if(VK == Key_Escape)
    {
        if(FileInstance->CommandType == CommandType_Open)
        {
            file_instance *ToDeallocate = FileInstance->Buffer;
            Deallocate(FileInstance->FileBuffer->LineAssemblage.LineCache); FileInstance->FileBuffer->LineAssemblage.Count = 0;
            *FileInstance = *FileInstance->Buffer;
            Deallocate(ToDeallocate);

            ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
            FileInstance->CommandFocus = false;
        }
        else if(FileInstance->CommandType != CommandType_None)
        {
            FileInstance->CommandFocus = false;
            FileInstance->CommandType = CommandType_None;
            StringCopy(FileInstance->CommandBuffer, "");
            StringCopy(FileInstance->CommandPredicate, "");
            StringCopy(FileInstance->QueryReplacee, "");
            StringCopy(FileInstance->QueryReplacer, "");
            FileInstance->CursorBias = GetCursorBias(FileInstance);
        }
        else
        {
            FileInstance->CommandFocus = false;
            StringCopy(FileInstance->CommandBuffer, "");
            StringCopy(FileInstance->CommandPredicate, "");
            
            FileInstance->CursorBias = GetCursorBias(FileInstance);
            // Todo: Come back to this section, where I don't want the marker to go away with any commands
            // Though if someone does ctrl-`, the CommandType is None, so we have some thinking to do
            // about how we want this to behave.
            FileInstance->MarkerIsDown = false;
        }
    }
    else if(VK == Key_Space)
    {
        if(FileInstance->CommandFocus)
        {
            if(FileInstance->CommandType == CommandType_QueryReplace && StringLength(FileInstance->QueryReplacer))
            {
                DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor, FileInstance->Cursor + StringLength(FileInstance->QueryReplacee), true);
                AddString(FileInstance->FileBuffer, FileInstance->QueryReplacer, FileInstance->Cursor, true);

                ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);

                IncrementalQuerySearch(FileInstance);
            }
            else
            {
                FormatString(sizeof(FileInstance->CommandBuffer), FileInstance->CommandBuffer, "%s ",
                             FileInstance->CommandBuffer);
            }
        }
        else
        {
            if(ControlIsDown)
            {
                if(!FileInstance->CommandFocus)
                {
                    FileInstance->MarkerIsDown = true;
                    FileInstance->Marker = FileInstance->Cursor;
                }
            }
            else
            {
                if(FileInstance->Cursor <= FileInstance->Marker)
                {
                    FileInstance->Marker = (umm)ClampS64(FileInstance->Marker, FileInstance->FileBuffer->Size - 1, -1) + 1;
                }
                AddCharacter(FileInstance->FileBuffer, ' ', FileInstance->Cursor, true);
                FileInstance->Cursor = (umm)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size - 1, -1) + 1;
                FileInstance->CursorBias = GetCursorBias(FileInstance);
                ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
            }
        }
    }
    
    if(VK != Key_Tab)
    {
        State->WordListIsValid = false;
    }

    if(Key)
    {
        if(ControlIsDown)
        {
            if(Key == '`' || Key == '/')
            {
                StringCopy(FileInstance->CommandPredicate, "");
                StringCopy(FileInstance->CommandBuffer, "");
                FileInstance->CommandFocus = !FileInstance->CommandFocus;
            }
            else if(Key == 'o')
            {
                file_instance TempFileInstance = *FileInstance;

                rect CarryOverWindow = FileInstance->Window;
                *FileInstance = MakeNullFileInstance(&State->Config);
                StringCopy(FileInstance->FileBuffer->Filename, "*Search Buffer*");

                FileInstance->CommandType = CommandType_Open;
                StringCopy(FileInstance->CommandPredicate, "Open: ");

                // Platform->CurrentWorkingDirectory(FileInstance->CommandBuffer);
                
                FileInstance->Active = true;
                FileInstance->CommandFocus = true;
                FileInstance->FileBuffer->Memory = GetDirectoryListing(&State->FileHash, FileInstance->CommandBuffer);
                FileInstance->FileBuffer->Size = StringLength(FileInstance->FileBuffer->Memory);
                FileInstance->Window = CarryOverWindow;

                FileInstance->Buffer = (file_instance *)Allocate(sizeof(file_instance));
                ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                *FileInstance->Buffer = TempFileInstance;
            }
            else if(Key == 'w')
            {
                State->Instances[State->ActiveInstanceIndex].Active = false;
                State->ActiveInstanceIndex = (++State->ActiveInstanceIndex) % State->InstanceWindows;
                State->Instances[State->ActiveInstanceIndex].Active = true;
            }
            else if(Key == 'd')
            {
                if(FileInstance->MarkerIsDown)
                {
                    if(FileInstance->Marker != FileInstance->Cursor)
                    { // Note: No need to do extra work, right?
                        DeleteSection(FileInstance->FileBuffer, FileInstance->Marker, FileInstance->Cursor, true);
                    }
                    FileInstance->Cursor = Min(FileInstance->Marker, FileInstance->Cursor);
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                    FileInstance->MarkerIsDown = false;
                    FileInstance->Marker = 0;
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
                else
                {
                    umm CursorLine = GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor);
                    DeleteLine(FileInstance->FileBuffer, CursorLine, true);
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);

                    if(CursorLine - 1 >= 0 && CursorLine - 1 < FileInstance->FileBuffer->LineAssemblage.Count)
                    {
                        umm BytesIn = (FileInstance->FileBuffer->LineAssemblage.LineCache + CursorLine - 1)->BytesIn;
                        umm LineLength = GetLineLength(FileInstance->FileBuffer, CursorLine);
                        if(BytesIn == 0 && LineLength == 0)
                        {
                            FileInstance->Cursor = 0;
                        }
                        else
                        {
                            FileInstance->Cursor = (FileInstance->CursorBias > (smm)LineLength) ? BytesIn + LineLength - 1 : BytesIn + FileInstance->CursorBias - 1;
                        }
                    }
                    else
                    {
                        FileInstance->Cursor = ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size, 0);
                    }

                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
            }
            else if(Key == 's')
            {
                if(FileInstance->FileBuffer->Filename[0] != '*')
                { // Research: Can you make any files with '*' in them in linux?
                    SaveBufferToFile(FileInstance);
                    StringCopy(FileInstance->CommandPredicate, "");
                    StringCopy(FileInstance->CommandBuffer, "");
                    FileInstance->CommandFocus = false;

                    if(FileInstance->FileBuffer->HasChanges)
                    {
                        FileInstance->SaveIndicatorTime = FileInstance->SaveIndicatorDuration;
                        FileInstance->FileBuffer->HasChanges = false;
                    }
                }
                else
                { // Todo: Ask if they want to save the buffer as a file
                    
                }
            }
            else if(Key == 'f')
            { // Note: Incremental Forward Search
                if(FileInstance->CommandType == CommandType_ForwardSearch)
                {
                    FileInstance->CommandFocus = false;
                    FileInstance->CommandType = CommandType_None;
                    StringCopy(FileInstance->CommandBuffer, "");
                    StringCopy(FileInstance->CommandPredicate, "");
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
                else
                {
                    FileInstance->CommandFocus = true;
                    FileInstance->CommandType = CommandType_ForwardSearch;
                    StringCopy(FileInstance->CommandBuffer, "");
                    FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                                 "I-Search 0/0 ");
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
            }
            else if(Key == 'r')
            { // Note: Incremental Reverse Search
                if(FileInstance->CommandType == CommandType_ReverseSearch)
                {
                    FileInstance->CommandFocus = false;
                    FileInstance->CommandType = CommandType_None;
                    StringCopy(FileInstance->CommandBuffer, "");
                    StringCopy(FileInstance->CommandPredicate, "");
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
                else
                {
                    FileInstance->CommandFocus = true;
                    FileInstance->CommandType = CommandType_ReverseSearch;
                    StringCopy(FileInstance->CommandBuffer, "");
                    FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                                 "Reverse-I-Search 0/0 ");
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
            }
            else if(Key == 'z')
            { // Todo: Undo
                if(FileInstance->FileBuffer->UndoStack.Cursor >= 0)
                {
                    FileInstance->Cursor = (FileInstance->FileBuffer->UndoStack.Stack + FileInstance->FileBuffer->UndoStack.Cursor)->BytesIn;
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                    FileInstance->MarkerIsDown = false;
                    Undo(FileInstance->FileBuffer);
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
            }
            else if(Key == 'y')
            { // Todo: Redo ... ?
                
            }
            else if(Key == 'm')
            {
                if(FileInstance->MarkerIsDown)
                {
                    FileInstance->Cursor = FileInstance->Marker;
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
            }
            else if(Key == 'q')
            { // Todo: Query replace
                if(FileInstance->CommandType == CommandType_QueryReplace ||
                   FileInstance->CommandType == CommandType_SelectionQueryReplace)
                {
                    FileInstance->CommandFocus = false;
                    FileInstance->CommandType = CommandType_None;
                    StringCopy(FileInstance->CommandBuffer, "");
                    StringCopy(FileInstance->CommandPredicate, "");
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
                else
                {
                    if(FileInstance->MarkerIsDown)
                    {
                        FileInstance->CommandType = CommandType_SelectionQueryReplace;
                        StringCopy(FileInstance->CommandPredicate, "Selection Replace: ");
                    }
                    else
                    {
                        FileInstance->CommandType = CommandType_QueryReplace;
                        StringCopy(FileInstance->CommandPredicate, "Replace: ");
                    }
                    FileInstance->CommandFocus = true;
                    StringCopy(FileInstance->CommandBuffer, "");
                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
            }
            else if(Key == 'c')
            {
                if(FileInstance->MarkerIsDown)
                {
                    CopySection(FileInstance->FileBuffer, FileInstance->Cursor, FileInstance->Marker);

                    umm BytesCopied = Abs((smm)FileInstance->Cursor - (smm)FileInstance->Marker);
                    FormatString(sizeof(FileInstance->CommandBuffer), FileInstance->CommandBuffer, "%llu Bytes copied", BytesCopied);
                    FileInstance->MarkerIsDown = false;
                }
                else
                {
                    umm CursorLine = GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor);
                    umm BytesIn = (FileInstance->FileBuffer->LineAssemblage.LineCache + CursorLine - 1)->BytesIn;
                    umm LineLength = GetLineLength(FileInstance->FileBuffer, CursorLine);
                    CopySection(FileInstance->FileBuffer, BytesIn, BytesIn + LineLength);
                }
            }
            else if(Key == 'x')
            {
                if(FileInstance->MarkerIsDown)
                {
                    CopySection(FileInstance->FileBuffer, FileInstance->Cursor, FileInstance->Marker);
                    DeleteSection(FileInstance->FileBuffer, FileInstance->Cursor, FileInstance->Marker, true);

                    umm BytesCopied = Abs((smm)FileInstance->Cursor - (smm)FileInstance->Marker);
                    FileInstance->Cursor = Min(FileInstance->Cursor, FileInstance->Marker);
                    FormatString(sizeof(FileInstance->CommandBuffer), FileInstance->CommandBuffer, "%llu Bytes cut", BytesCopied);
                    FileInstance->MarkerIsDown = false;
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
                else
                {
                    umm CursorLine = GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor);
                    umm BytesIn = (FileInstance->FileBuffer->LineAssemblage.LineCache + CursorLine - 1)->BytesIn;
                    umm LineLength = GetLineLength(FileInstance->FileBuffer, CursorLine);
                    CopySection(FileInstance->FileBuffer, BytesIn, BytesIn + LineLength);
                    DeleteLine(FileInstance->FileBuffer, CursorLine, true);
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);

                    if(CursorLine - 1 >= 0 && CursorLine - 1 < FileInstance->FileBuffer->LineAssemblage.Count)
                    {
                        BytesIn = (FileInstance->FileBuffer->LineAssemblage.LineCache + CursorLine - 1)->BytesIn;
                        LineLength = GetLineLength(FileInstance->FileBuffer, CursorLine);
						
                        if(BytesIn == 0 && LineLength == 0)
                        {
                            FileInstance->Cursor = 0;
                        }
                        else
                        {
                            FileInstance->Cursor = (FileInstance->CursorBias > (smm)LineLength) ? BytesIn + LineLength - 1 : BytesIn + FileInstance->CursorBias - 1;
                        }
                    }
                    else
                    {
                        FileInstance->Cursor = ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size, 0);
                    }

                    FileInstance->CursorBias = GetCursorBias(FileInstance);
                }
            }
            else if(Key == 'v')
            { // Todo: DeleteSection marked by region first?
                b32 ContainsLine = false;
                char *Text = GetClipboardText();
                if(Text)
                {
                    umm TextLength = StringLength(Text);
                    if(Text[TextLength - 1] == '\n')
                    {
                        ContainsLine = true;
                    }
                }

                umm PasteCharCount = 0;
                if(ContainsLine)
                {
                    umm BytesIn = (umm)BeginningOfCursorLine(FileInstance);

                    PasteCharCount = PasteSection(FileInstance->FileBuffer, BytesIn);
                    FormatString(sizeof(FileInstance->CommandBuffer), FileInstance->CommandBuffer, "%llu Bytes pasted", PasteCharCount);
                    FileInstance->Cursor += PasteCharCount;
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }
                else
                {
                    PasteCharCount = PasteSection(FileInstance->FileBuffer, FileInstance->Cursor);
                    FormatString(sizeof(FileInstance->CommandBuffer), FileInstance->CommandBuffer, "%llu Bytes pasted", PasteCharCount);
                    FileInstance->Cursor += PasteCharCount;
                    ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
                }

                paste_location_data *Buffer = 0;

                Buffer = (paste_location_data *)ArrayInsert(FileInstance->PasteLocationAssemblage.PasteLocationData,
                                                            FileInstance->PasteLocationAssemblage.Count,
                                                            FileInstance->PasteLocationAssemblage.MaxCount);
                Buffer->BytesIn = FileInstance->Cursor - PasteCharCount;
                Buffer->BytesLong = FileInstance->Cursor - Buffer->BytesIn;
                Buffer->Time = Config->PasteColorChangeDuration;
            }
            else if(Key == 'g')
            {
                FileInstance->CommandFocus = true;
                FileInstance->CommandType = CommandType_GoToLine;
                StringCopy(FileInstance->CommandBuffer, "");
                FormatString(sizeof(FileInstance->CommandPredicate), FileInstance->CommandPredicate,
                             "Go To Line: ");
            }
        }
        else if(AltIsDown)
        {
            if(Key == 'x')
            {
                SplitWindowHorizontal(State, FileInstance);
            }
            else if(Key == 'v')
            {
                SplitWindowVertical(State, FileInstance);
            }
        }
        else if(FileInstance->CommandFocus)
        {
            if(!StringLength(FileInstance->QueryReplacer))
            {
                FormatString(sizeof(FileInstance->CommandBuffer), FileInstance->CommandBuffer, "%s%c", 
                             FileInstance->CommandBuffer, Key);
            }
            
            if(FileInstance->CommandType == CommandType_Open)
            {
                FileInstance->FileBuffer->Memory = GetDirectoryListing(&State->FileHash, FileInstance->CommandBuffer);
                FileInstance->FileBuffer->Size = StringLength(FileInstance->FileBuffer->Memory);
                FileInstance->Cursor = 0;
            }
            else if(FileInstance->CommandType == CommandType_ForwardSearch)
            {
                IncrementalSearch(FileInstance);
            }
            else if(FileInstance->CommandType == CommandType_ReverseSearch)
            {
                ReverseIncrementalSearch(FileInstance);
            }
        }
        else if(FileInstance->Insert)
        {
            if(FileInstance->Cursor >= 0 &&
               RoundS32xToU32x(FileInstance->Cursor) < FileInstance->FileBuffer->Size)
            {
                *(FileInstance->FileBuffer->Memory + FileInstance->Cursor) = Key;
            }
        }
        else
        {
            if(Key != ' ') // Note: we're already handling Key_Space
            {
                if(FileInstance->Cursor <= FileInstance->Marker)
                {
                    FileInstance->Marker = (u32x)ClampS64(FileInstance->Marker, FileInstance->FileBuffer->Size - 1, -1) + 1;
                }
                AddCharacter(FileInstance->FileBuffer, Key, FileInstance->Cursor, true);
                FileInstance->Cursor = (u32x)ClampS64(FileInstance->Cursor, FileInstance->FileBuffer->Size - 1, -1) + 1;
                FileInstance->CursorBias = GetCursorBias(FileInstance);
                ConstructAssemblages(FileInstance->FileBuffer, &State->CommentKeywordHash, &State->FileHash, State->Dictionary);
            }
        }
    }
}

void 
Resized(editor_state *State, v2 Before, v2 After)
{
    if(State->Instances)
    {
        v2 DimRatio = After/Before;

        for(u32x Index = 0;
            Index < State->InstanceCount;
            ++Index)
        {
            file_instance *Instance = State->Instances + Index;

            if(After.x == 0.0f || After.y == 0.0f)
            { // Note: Create an Instance copy to it's buffer
                Instance->Buffer = (file_instance *)Allocate(sizeof(file_instance));
                *Instance->Buffer = *Instance;

                Instance->Window.P1 = Hadamard(Instance->Window.P1, DimRatio);
                Instance->Window.P2 = Hadamard(Instance->Window.P2, DimRatio);
            }
            else if(Before.x == 0.0f || Before.y == 0.0f)
            { // Note: Restore an Instance copy from it's buffer
                file_instance *ToDeallocate = Instance->Buffer;
                *Instance = *Instance->Buffer;
                Deallocate(ToDeallocate);
            }
            else
            {
                Instance->Window.P1 = Hadamard(Instance->Window.P1, DimRatio);
                Instance->Window.P2 = Hadamard(Instance->Window.P2, DimRatio);
            }
        }
    }
}
