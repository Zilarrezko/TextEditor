typedef struct
{
    BITMAPINFO BitmapInfo;
    void *Memory;
    int Width;
    int Height;
    int Stride;
} win32_backbuffer;

typedef enum resizing_field
{
    ResizingField_active_resizing = 1 << 0,
    ResizingField_hot_resizing =    1 << 1,
    ResizingField_is_resizing_n =   1 << 2,
    ResizingField_is_resizing_s =   1 << 3,
    ResizingField_is_resizing_e =   1 << 4,
    ResizingField_is_resizing_w =   1 << 5,
    ResizingField_is_resizing_nw =  1 << 6,
    ResizingField_is_resizing_ne =  1 << 7,
    ResizingField_is_resizing_sw =  1 << 8,
    ResizingField_is_resizing_se =  1 << 9
} resizing_field;
typedef struct
{
    editor_state *State;

    b32x IsDraggingWindow;
    b32x ResizingField;
} window_proc_passin;
