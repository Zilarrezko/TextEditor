
#define Log(Format, ...) \
{ \
char _Buffer[512]; \
FormatString(sizeof(_Buffer), _Buffer, Format, __VA_ARGS__); \
OutputDebugStringA(_Buffer); \
}

global r32 GlobalPerfFreq;

#define StartTimedPoint(ID) \
LARGE_INTEGER ID ## StartCounter; \
LARGE_INTEGER ID ## EndCounter; \
u64 ID ## StartCycles = __rdtsc(); \
QueryPerformanceCounter(&ID ## StartCounter);

#define EndTimedPoint(ID) \
{ \
u64 ID ## EndCycles = __rdtsc(); \
QueryPerformanceCounter(&ID ## EndCounter); \
char Buffer ## ID[256]; \
FormatString(sizeof(Buffer ## ID), Buffer ## ID, "DEBUG  %-32s %8.2fms %13ucy \n", #ID, 1000*((r32)(ID ## EndCounter.QuadPart - ID ## StartCounter.QuadPart) / GlobalPerfFreq), ID ## EndCycles - ID ## StartCycles); \
OutputDebugStringA(Buffer ## ID); \
}

#define StartCountingPoint(ID) \
LARGE_INTEGER ID ## StartCycleCounter; \
LARGE_INTEGER ID ## EndCycleCounter; \
QueryPerformanceCounter(&ID ## StartCycleCounter); \
u64 ID ## Cycles = 0;

#define CycleCountingPoint(ID) \
++ID ## Cycles;

#define EndCountingPoint(ID) \
{\
QueryPerformanceCounter(&ID ## EndCycleCounter); \
u64 ID ## StartCycleCycles = __rdtsc(); \
r32 TimeInMilliseconds = 1000*((r32)(ID ## EndCycleCounter.QuadPart - ID ## StartCycleCounter.QuadPart) / GlobalPerfFreq); \
char Buffer ## ID[256]; \
FormatString(sizeof(Buffer ## ID), Buffer ## ID, "DEBUG  %-32s %8.2fms %13uh %7.2fms/h %7.2fh/ms\n", \
             #ID, TimeInMilliseconds, ID ## Cycles, TimeInMilliseconds/(r32)ID##Cycles, (r32)ID##Cycles/TimeInMilliseconds); \
OutputDebugStringA(Buffer ## ID); \
}

typedef struct
{
    void *Memory;
    size_t Size;
} debug_read_file_result;

global u8 FontPath[MAX_PATH];

void
DebugFreeFile(debug_read_file_result *File)
{
    if(File->Memory)
    {
        VirtualFree(File->Memory, 0, MEM_RELEASE);
        File->Size = 0;
        File->Memory = 0;
    }
#if DEBUG
    else
    {
        InvalidCodePath;
    }
#endif
}

#pragma warning(push)
#pragma warning(disable:4706)
debug_read_file_result
DebugReadFile(u8 *Filename)
{
    StartTimedPoint(ReadFile);
    // Todo: Make sure we're handling > 4GB files correctly now
    debug_read_file_result Result = {};

    // Todo: Use CreateFileW
    HANDLE FileHandle = CreateFileA((char *)Filename, GENERIC_READ, FILE_SHARE_READ, 0,
                                    OPEN_EXISTING, 0, 0);

    if(FileHandle == INVALID_HANDLE_VALUE)
    {
        int Error = GetLastError();
        
        char Buffer[512] = "";
        FormatString(sizeof(Buffer), Buffer, "[ERROR] %d at line: %d from filename %s", Error, __LINE__, Filename);
        OutputDebugStringA(Buffer);

        return Result;
    }

    LARGE_INTEGER FileSize;
    GetFileSizeEx(FileHandle, &FileSize);

    u32x TotalBytesRead = 0;

    Result.Memory = VirtualAlloc(0, (size_t)FileSize.QuadPart, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);

    // Todo: Make sure this works on Filesizes > 4GB
    DWORD BytesRead;
    b32 NoProblem = false;
    while(NoProblem = ReadFile(FileHandle, (void *)((u8 *)Result.Memory + BytesRead), (DWORD)ClampU32X(FileSize.QuadPart - TotalBytesRead, 0, U32_MAX), &BytesRead, 0))
    {
        TotalBytesRead += BytesRead;
        if(TotalBytesRead == (u32x)FileSize.QuadPart)
        {
            break;
        }
        else if(TotalBytesRead > (u32x)FileSize.QuadPart)
        {
            InvalidCodePath;
        }
        else
        {

        }

        if(!NoProblem)
        {
            int Error = GetLastError();

#if DEBUG
            char Buffer[512] = "";
            FormatString(512, Buffer, "Encountered error: %d at line: %d from filename %s", Error, __LINE__, Filename);
            MessageBoxA(0, Buffer, "Text Editor Error", MB_OK);
#endif

            InvalidCodePath;
            return Result;
        }
    }

    Result.Size = (umm)FileSize.QuadPart;

    CloseHandle(FileHandle);
    EndTimedPoint(ReadFile);
    return Result;
}
#pragma warning(pop)

void 
DebugWriteFile(u8 *Filename, void *Memory, size_t ContentSize)
{
    // Todo: Make sure we're handling > 4GB files correctly now
    // Todo: Replace with CreateFileW when we can
    HANDLE FileHandle = CreateFileA((char *)Filename, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, 0,
                                    CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

    if(!FileHandle)
    {
        int Error = GetLastError();

#if DEBUG
        char Buffer[512] = "";
        FormatString(512, Buffer, "Encountered error: %d at line: %d from filename %s", Error, __LINE__, Filename);
        MessageBoxA(0, Buffer, "Text Editor Error", MB_OK);
#endif

        InvalidCodePath;
        return;
    }

    DWORD BytesWritten = 0;
    if(!WriteFile(FileHandle, Memory, (DWORD)ContentSize, &BytesWritten, 0))
    {
        int Error = GetLastError();

#if DEBUG
        char Buffer[512] = "";
        FormatString(512, Buffer, "Encountered error: %d at line: %d from filename %s", Error, __LINE__, Filename);
        MessageBoxA(0, Buffer, "Text Editor Error", MB_OK);
#endif

        InvalidCodePath;
        return;
    }

    if(BytesWritten != ContentSize)
    {
        InvalidCodePath;
    }

    CloseHandle(FileHandle);
}

void 
SetClipboardText(u8 *String)
{ // Todo: Unicode
    if(OpenClipboard(0))
    {
        EmptyClipboard();

        HGLOBAL MemoryObject = GlobalAlloc(GMEM_MOVEABLE, StringLength(String) + 1);

        u8 *ClipboardString = (u8 *)GlobalLock(MemoryObject);
        StringCopy(ClipboardString, String);
        GlobalUnlock(MemoryObject);

        SetClipboardData(CF_TEXT, // CF_UNICODETEXT
                         MemoryObject);

        CloseClipboard();
    }
}

char *
GetClipboardText(void)
{ // Todo: Unicode Also this... look up tutorial...
    char *Result = 0;

    if(OpenClipboard(0))
    {
        HANDLE TextHandle = GetClipboardData(CF_TEXT);
        if(!TextHandle)
        {
            int Error = GetLastError();
            Assert("Wowee!");
        }

        char *Text = (char *)GlobalLock(TextHandle);

        if(Text)
        {
            u32x TextLength = StringLength(Text);
            Result = (char *)Allocate(TextLength + 1);

            char *Dest = Result;

            for(char *At = Text;
                *At;
                ++At)
            {
                *Dest++ = *At;
            }
            *Dest = '\0';
        }

        GlobalUnlock(TextHandle);
        CloseClipboard();
    }

    return Result;
}

void 
CurrentWorkingDirectory(u8 *Result)
{
    // Todo: Change to GetCurrentDirectoryW
    GetCurrentDirectoryA(MAX_PATH, (char *)Result);
    StringCat(Result, "\\");
}

b32 
IsPathRelative(char *Path)
{
    b32 Result = true;

    if(StringLength(Path) > 3)
    {
        if(*(Path + 1) == ':' &&
           *(Path + 2) == '\\')
        {
            Result = false;
        }

        Result = false;
    }

    return Result;
}
