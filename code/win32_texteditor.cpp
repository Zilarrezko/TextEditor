#include "win32_texteditor.h"

// #include "intrin.h"
// #include <Shlobj.h>

global win32_backbuffer BackBuffer;
global GLuint TextureHandle;
global HDC WindowDeviceContext;
global HDC FontDeviceContext;

global editor_memory GlobalEditorMemory;

global WINDOWPLACEMENT WindowPlacement = {sizeof(WINDOWPLACEMENT)};

void
ToggleFullscreen(HWND Window, b32 *IsMaximized)
{
    DWORD Style = GetWindowLong(Window, GWL_STYLE);
    if(Style & WS_OVERLAPPEDWINDOW) // !*IsMaximized
    {
        MONITORINFO MonitorInfo = {sizeof(MonitorInfo)};
        if(GetWindowPlacement(Window, &WindowPlacement) &&
           GetMonitorInfo(MonitorFromWindow(Window,
                          MONITOR_DEFAULTTOPRIMARY), &MonitorInfo))
        {
            SetWindowLong(Window, GWL_STYLE, Style & ~WS_OVERLAPPEDWINDOW);
            SetWindowPos(Window, HWND_TOP,
                         MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
                         MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left,
                         MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top,
                         SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
            *IsMaximized = true;
        }
    }
    else
    {
        int SetWindowLongReturn = SetWindowLong(Window, GWL_STYLE, Style | WS_OVERLAPPEDWINDOW);
        int Error = GetLastError();
        SetWindowPlacement(Window, &WindowPlacement);
        SetWindowPos(Window, 0, 0, 0, 0, 0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                     SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
        *IsMaximized = false;
    }
}

void
GetConfigLexeme(debug_read_file_result *ReadResult, char **At, char *Dest)
{
    for(;
        *At < (char *)ReadResult->Memory + ReadResult->Size;
        ++(*At))
    {
        if(IsWhitespace(**At))
        {
            break;
        }
        else
        {
            if(**At != '#')
            {
                *Dest++ = **At;
            }
            else
            {
                break;
            }
        }
    }
    *Dest = '\0';
}

void
ParseConfig(debug_read_file_result *ReadResult, comment_keyword_hash *CommentKeywordHash, config *Config)
{
#define MAX_CONFIG_ARG_COUNT 8
    char Buffer[MAX_CONFIG_ARG_COUNT][1024] = {};
    u32x BufferIndex = 0;

    char *EndofFile = (char *)ReadResult->Memory + ReadResult->Size;

    for(char *At = (char *)ReadResult->Memory;
        At <= EndofFile;
        ++At)
    {
        if(!IsWhitespace(*At))
        {
            if(*At == '#')
            {
                for(;
                    At < EndofFile;
                    )
                {
                    if(*At != '\n')
                    {
                        ++At;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                GetConfigLexeme(ReadResult, &At, Buffer[BufferIndex]);
                ++BufferIndex;
            }
        }

        if(*At == '\n' || At == EndofFile)
        {
            if(CompareString(Buffer[0], "BackgroundColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->BackgroundColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontIdentifierColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontIdentifierColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontLiteralColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontLiteralColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontNumberColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontNumberColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontCommentColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontCommentColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontKeywordColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontKeywordColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontOperatorColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontOperatorColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontPreprocessorColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontPreprocessorColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontUnknownColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontUnknownColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "CursorColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->CursorColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "MarkerColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->MarkerColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "CursorLineFillColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->CursorLineFillColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "CursorLineOutlineColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->CursorLineOutlineColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "GutterColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->GutterColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "CommandFontColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->CommandFontColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "BorderColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->BorderColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "InfoBarBorderColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->InfoBarBorderColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "InfoBarColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->InfoBarColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "InfoFontColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->InfoFontColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "CommandPredicateFontColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->CommandPredicateFontColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "FontGutterColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->FontGutterColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "CommandBarColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->CommandBarColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "GutterPaddingSize"))
            {
                if(StringLength(Buffer[1]))
                {
                    Config->GutterPaddingSize = (r32)AsciiToReal32(Buffer[1]);
                }
            }
            else if(CompareString(Buffer[0], "BorderSize"))
            {
                if(StringLength(Buffer[1]))
                {
                    Config->BorderSize = (r32)AsciiToReal32(Buffer[1]);
                }
            }
            else if(CompareString(Buffer[0], "CursorType"))
            {
                if(StringLength(Buffer[1]))
                {
                    cursor_type CursorType = CursorType_Block;

                    if(CompareString(Buffer[1], "line"))
                    {
                        CursorType = CursorType_Line;
                    }
                    else if(CompareString(Buffer[1], "underscore"))
                    {
                        CursorType = CursorType_Underscore;
                    }

                    Config->CursorType = CursorType;
                }
            }
            else if(CompareString(Buffer[0], "FontFile"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]))
                {
                    StringCopy(Config->FontFilename, Buffer[1]);
                    Config->FontSize = (u16)AsciiToInteger(Buffer[2]);
                }
            }
            else if(CompareString(Buffer[0], "UndoMaxCount"))
            {
                if(StringLength(Buffer[1]))
                {
                    Config->UndoMaxCount = (u32x)AsciiToInteger(Buffer[1]);
                }
            }
            else if(CompareString(Buffer[0], "LineWrapping"))
            {
                if(StringLength(Buffer[1]))
                {
                    if(CompareString(Buffer[1], "true") || CompareString(Buffer[1], "True") || CompareString(Buffer[1], "TRUE") || CompareString(Buffer[1], "on"))
                    {
                        Config->LineWrapping = true;
                    }
                    else if(CompareString(Buffer[1], "false") || CompareString(Buffer[1], "False") || CompareString(Buffer[1], "FALSE") || CompareString(Buffer[1], "off"))
                    {
                        Config->LineWrapping = false;
                    }
                }
            }
            else if(CompareString(Buffer[0], "MarkerSelection"))
            {
                if(StringLength(Buffer[1]))
                {
                    if(CompareString(Buffer[1], "true") || CompareString(Buffer[1], "True") || CompareString(Buffer[1], "TRUE") || CompareString(Buffer[1], "on"))
                    {
                        Config->MarkerSelection = true;
                    }
                    else if(CompareString(Buffer[1], "false") || CompareString(Buffer[1], "False") || CompareString(Buffer[1], "FALSE") || CompareString(Buffer[1], "off"))
                    {
                        Config->MarkerSelection = false;
                    }
                }
            }
            else if(CompareString(Buffer[0], "AutoSave"))
            {
                if(StringLength(Buffer[1]))
                {
                    if(CompareString(Buffer[1], "true") || CompareString(Buffer[1], "True") || CompareString(Buffer[1], "TRUE") || CompareString(Buffer[1], "on"))
                    {
                        Config->AutoSave = true;
                    }
                    else if(CompareString(Buffer[1], "false") || CompareString(Buffer[1], "False") || CompareString(Buffer[1], "FALSE") || CompareString(Buffer[1], "off"))
                    {
                        Config->AutoSave = false;
                    }
                }
            }
            else if(CompareString(Buffer[0], "LineNumbers"))
            {
                if(StringLength(Buffer[1]))
                {
                    if(CompareString(Buffer[1], "true") || CompareString(Buffer[1], "True") || CompareString(Buffer[1], "TRUE") || CompareString(Buffer[1], "on"))
                    {
                        Config->LineNumbers = true;
                        Config->LineNumberType = LineNumber_Normal;
                    }
                    else if(StringLength(Buffer[1]) && CompareString(Buffer[1], "CaptainKraft"))
                    {
                        Config->LineNumbers = true;
                        Config->LineNumberType = LineNumber_CaptainKraft;
                    }
                    else if(StringLength(Buffer[1]) && CompareString(Buffer[1], "relative"))
                    {
                        Config->LineNumbers = true;
                        Config->LineNumberType = LineNumber_Relative;
                    }
                    else
                    {
                        Config->LineNumbers = false;
                    }
                }
            }
            else if(CompareString(Buffer[0], "CommentKeyword"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]) && StringLength(Buffer[4]) && StringLength(Buffer[5]))
                {
                    b32 IsCaseSensitive;
                    v4 Color = V4((r32)AsciiToReal32(Buffer[3]), (r32)AsciiToReal32(Buffer[4]), (r32)AsciiToReal32(Buffer[5]), 255.0f);

                    u8 Keyword[512] = "";
                    if(CompareString(Buffer[2], "false") || CompareString(Buffer[2], "False") || CompareString(Buffer[2], "FALSE") || CompareString(Buffer[2], "off"))
                    {
                        ToLowerString(Keyword, Buffer[1]);
                        IsCaseSensitive = false;
                    }
                    else
                    {
                        StringCopy(Keyword, Buffer[1]);
                        IsCaseSensitive = true;
                    }

                    PlaceCommentKeywordIntoHash(CommentKeywordHash, Keyword, IsCaseSensitive, Color);
                }
            }
            else if(CompareString(Buffer[0], "PasteColorChangeDuration"))
            {
                if(StringLength(Buffer[1]))
                {
                    Config->PasteColorChangeDuration = AsciiToReal32(Buffer[1]);
                }
            }
            else if(CompareString(Buffer[0], "PasteColorChangeColor"))
            {
                if(StringLength(Buffer[1]) && StringLength(Buffer[2]) && StringLength(Buffer[3]))
                {
                    Config->PasteColorChangeColor = V4((r32)AsciiToReal32(Buffer[1]), (r32)AsciiToReal32(Buffer[2]), (r32)AsciiToReal32(Buffer[3]), 255.0f);
                }
            }
            else if(CompareString(Buffer[0], "RenderSpecialCharacters"))
            {
                if(StringLength(Buffer[1]))
                {
                    ToLowerString(Buffer[1], Buffer[1]);
                    if(CompareString(Buffer[1], "true") || CompareString(Buffer[1], "on"))
                    {
                        Config->RenderSpecialCharacters = true;
                    }
                    else
                    {
                        Config->RenderSpecialCharacters = false;
                    }
                }
            }

            for(u32x Index = 0; Index < MAX_CONFIG_ARG_COUNT; ++Index)
            {
                StringCopy(Buffer[Index], "");
            } BufferIndex = 0;
        }
    }
#undef MAX_CONFIG_ARG_COUNT
}

void
ReadConfig(config *Config, comment_keyword_hash *CommentKeywordHash, u8 *PathToConfig)
{
    debug_read_file_result ReadResult = {};
    if(PathToConfig)
    {
        ReadResult = DebugReadFile(PathToConfig);

        StringCopy(Config->Filename, PathToConfig);
    }
    else
    {
        ReadResult = DebugReadFile((u8 *)"config");
        StringCopy(Config->Filename, "config");
    }

    if(ReadResult.Memory)
    {
        // Todo: Change to CreateFileW
        HANDLE FileHandle = CreateFileA((char *)Config->Filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);

        if(FileHandle)
        {
            FILETIME LastWriteTime;

            if(!GetFileTime(FileHandle,
                            0,               // Time Created
                            0,               // Time Last Accessed
                            &LastWriteTime)) // Time Last Written
            {
                int Error = GetLastError();
                InvalidCodePath;
            }
            CloseHandle(FileHandle);

            Config->LastWriteTime = LastWriteTime;
            StringCopy(Config->FontFilename, "");

            ParseConfig(&ReadResult, CommentKeywordHash, Config);
            Config->ConfigReloaded = true;
        }

        DebugFreeFile(&ReadResult);
    }
    else
    { // Todo/Think: Make and populate one, or make defaults?
        StringCopy(Config->Filename, "");
    }
}

void
CheckConfig(config *Config, comment_keyword_hash *CommentKeywordHash)
{
    // Todo: Changee to CreateFileW
    HANDLE FileHandle = CreateFileA((char *)Config->Filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);

    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        FILETIME WriteTime;

        if(!GetFileTime(FileHandle,
                        0,               // Time Created
                        0,               // Time Last Accessed
                        &WriteTime)) // Time Last Written
        {
            int Error = GetLastError();
            InvalidCodePath;
        }
        CloseHandle(FileHandle);

        if(CompareFileTime(&Config->LastWriteTime, &WriteTime) == -1)
        { // The current Write time is later than the one recorded
            Config->LastWriteTime = WriteTime;

            debug_read_file_result ReadResult = DebugReadFile(Config->Filename);
            ParseConfig(&ReadResult, CommentKeywordHash, Config);
            Config->ConfigReloaded = true;

            DebugFreeFile(&ReadResult);
        }
    }
}

void
ResizeBackBuffer(win32_backbuffer *Buffer, int Width, int Height)
{
    if(Buffer->Memory)
    {
        VirtualFree(Buffer->Memory, 0, MEM_RELEASE);
    }

    int Bytespp = BITMAP_BYTESPP;

    Buffer->Width = Width;
    Buffer->Height = Height;
    Buffer->Stride = Width*Bytespp;

    Buffer->BitmapInfo.bmiHeader.biSize        = sizeof(Buffer->BitmapInfo.bmiHeader);
    Buffer->BitmapInfo.bmiHeader.biWidth       = Width;
    Buffer->BitmapInfo.bmiHeader.biHeight      = -Height;
    Buffer->BitmapInfo.bmiHeader.biPlanes      = 1;
    Buffer->BitmapInfo.bmiHeader.biBitCount    = 32;
    Buffer->BitmapInfo.bmiHeader.biCompression = BI_RGB;

    // Piggy: Two whole rows worth because SSE :x
    int MemorySize = Width*Height*Bytespp + Width*Bytespp*2;
    Buffer->Memory = VirtualAlloc(0, MemorySize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
}

void
Win32GetGLFunctions(void)
{
    glGenBuffers = (gl_gen_buffers *)wglGetProcAddress("glGenBuffers");
    glBindBuffer = (gl_bind_buffer *)wglGetProcAddress("glBindBuffer");
    glBufferData = (gl_buffer_data *)wglGetProcAddress("glBufferData");
    glEnableVertexAttribArray = (gl_enable_vertex_attrib_array *)wglGetProcAddress("glEnableVertexAttribArray");
    glVertexAttribPointer = (gl_vertex_attrib_pointer *)wglGetProcAddress("glVertexAttribPointer");
    glCreateProgram = (gl_create_program *)wglGetProcAddress("glCreateProgram");
    glCreateShader = (gl_create_shader *)wglGetProcAddress("glCreateShader");
    glShaderSource = (gl_shader_source *)wglGetProcAddress("glShaderSource");
    glCompileShader = (gl_compile_shader *)wglGetProcAddress("glCompileShader");
    glGetShaderiv = (gl_get_shader_iv *)wglGetProcAddress("glGetShaderiv");
    glGetShaderInfoLog = (gl_get_shader_info_log *)wglGetProcAddress("glGetShaderInfoLog");
    glAttachShader = (gl_attach_shader *)wglGetProcAddress("glAttachShader");
    glLinkProgram = (gl_link_program *)wglGetProcAddress("glLinkProgram");
    glUseProgram = (gl_use_program *)wglGetProcAddress("glUseProgram");
    glGetUniformLocation = (gl_get_uniform_location *)wglGetProcAddress("glGetUniformLocation");
    glUniform2f = (gl_uniform_2f *)wglGetProcAddress("glUniform2f");
    glUniform1i = (gl_uniform_1i *)wglGetProcAddress("glUniform1i");
    glBufferSubData = (gl_buffer_sub_data *)wglGetProcAddress("glBufferSubData");

    if(!glGenBuffers || !glBindBuffer || !glBufferData ||
       !glEnableVertexAttribArray || !glVertexAttribPointer ||
       !glCreateProgram || !glCreateShader || !glShaderSource ||
       !glCompileShader || !glGetShaderiv || !glGetShaderInfoLog ||
       !glAttachShader || !glLinkProgram || !glUseProgram || !glGetUniformLocation ||
       !glUniform2f || !glUniform1i || !glBufferSubData)
    {
        int Error = GetLastError();
        InvalidCodePath;
    }
}

void
Win32InitOpenGL(HDC DeviceContext)
{
    PIXELFORMATDESCRIPTOR DesiredFormat = {};
    DesiredFormat.nSize = sizeof(DesiredFormat);
    DesiredFormat.nVersion = 1;
    DesiredFormat.iPixelType = PFD_TYPE_RGBA;
    DesiredFormat.dwFlags = PFD_SUPPORT_OPENGL|PFD_DRAW_TO_WINDOW|PFD_DOUBLEBUFFER;
    DesiredFormat.cColorBits = 32;
    DesiredFormat.cAlphaBits = 8;
    DesiredFormat.iLayerType = PFD_MAIN_PLANE;

    int SuggestedIndex = ChoosePixelFormat(DeviceContext, &DesiredFormat);

    PIXELFORMATDESCRIPTOR SuggestedFormat;
    DescribePixelFormat(DeviceContext, SuggestedIndex, sizeof(SuggestedFormat), &SuggestedFormat);
    SetPixelFormat(DeviceContext, SuggestedIndex, &SuggestedFormat);

    HGLRC OpenGLRC = wglCreateContext(DeviceContext);
    if(wglMakeCurrent(DeviceContext, OpenGLRC))
    {
        glGenTextures(1, &TextureHandle);
        Win32GetGLFunctions();

        char Buffer[256];
        FormatString(sizeof(Buffer), Buffer, "OpenGL Version: %s\n", glGetString(GL_VERSION));
        OutputDebugStringA(Buffer);
    }
    else
    {
        // Todo: :( , switch to software rendering?
        InvalidCodePath;
    }
}

void
BlitBackBuffer(win32_backbuffer *Buffer, HDC DeviceContext, int WindowWidth, int WindowHeight)
{
#if 0
    StretchDIBits(DeviceContext,
        0, 0, WindowWidth, WindowHeight,
        0, 0, Buffer->Width, Buffer->Height,
        Buffer->Memory, &Buffer->BitmapInfo,
        DIB_RGB_COLORS, SRCCOPY);
#else 

    SwapBuffers(DeviceContext);
#endif
}

LRESULT CALLBACK
WindowProc(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
    LRESULT Result = 0;

    // Todo: Where to put these?
    // They're being used right now between the Mouse move and mouse click events
    static s32 PreviousClickX = 0;
    static s32 PreviousClickY = 0;

    window_proc_passin *Passin = 0;
    editor_state *EditorState = 0;
    if(Message == WM_CREATE)
    {
        CREATESTRUCT *CreateStruct = (CREATESTRUCT *)LParam;
        Passin = (window_proc_passin *)CreateStruct->lpCreateParams;
        SetWindowLongPtr(Window, GWLP_USERDATA, (LONG_PTR)Passin);
    }
    else
    {
        Passin = (window_proc_passin *)GetWindowLongPtr(Window, GWLP_USERDATA);
    }

    if(Passin)
    { // Todo: Passin may be 0 for any messages that come before WM_CREATE message
        EditorState = Passin->State;
    }

    switch(Message)
    {
        case WM_CREATE:
        {
        } break;

        case WM_ACTIVATEAPP:
        {
        } break;

        case WM_CLOSE:
        case WM_DESTROY:
        case WM_QUIT:
        {
            EditorState->Running = false;
            PostQuitMessage(0);
        } break;

        case WM_SIZE:
        {
            v2 DimBefore = V2((r32)BackBuffer.Width, (r32)BackBuffer.Height);

            RECT WindowRect;
            // GetWindowRect(Window, &WindowRect);
            GetClientRect(Window, &WindowRect);
            int WindowWidth = WindowRect.right - WindowRect.left;
            int WindowHeight = WindowRect.bottom - WindowRect.top;
            v2 DimAfter = V2((r32)WindowWidth, (r32)WindowHeight);

            if(WindowHeight > 0 && WindowWidth > 0)
            {
                ResizeBackBuffer(&BackBuffer, WindowWidth, WindowHeight);
                Resized(Passin->State, DimBefore, DimAfter);

                if(GlobalEditorMemory.IsInitialized)
                {
                    editor_backbuffer EditorBackBuffer ={};
                    EditorBackBuffer.Memory =      BackBuffer.Memory;
                    EditorBackBuffer.Width =  (u16)BackBuffer.Width; // Todo: Come back and write a proper conversion function
                    EditorBackBuffer.Height = (u16)BackBuffer.Height;

                    UpdateAndRender(Passin->State, &EditorBackBuffer, &GlobalEditorMemory, 0.0f);
                    BlitBackBuffer(&BackBuffer, WindowDeviceContext, BackBuffer.Width, BackBuffer.Height);
                    ValidateRect(Window, 0);
                }
            }

        }break;

        case WM_PAINT:
        {
            BlitBackBuffer(&BackBuffer, WindowDeviceContext, BackBuffer.Width, BackBuffer.Height);
            ValidateRect(Window, 0);
        } break;

        case WM_DROPFILES:
        {
            char Path[MAX_PATH] = "";
            POINT DropPoint;
            HDROP DropHandle = (HDROP)(WParam);
            UINT Length = DragQueryFile(DropHandle, 0, 0, 0) + 1;
            DragQueryFile(DropHandle, 0, Path, Length);
            DragQueryPoint(DropHandle, &DropPoint);

            // Todo: Finish this

            DragFinish(DropHandle);
        } break;

        case WM_CHAR:
        case WM_SYSCHAR:
        {
            u32 CharacterCode = (u32)WParam;

            u32 ScanCode = (LParam & 0x00FF0000) >> 16;
            u32 VKCode = MapVirtualKey(ScanCode, MAPVK_VSC_TO_VK);

            u8 KeyStates[256];
            GetKeyboardState(KeyStates);

            b32 ShiftIsDown =   KeyStates[VK_LSHIFT]   &(0x80) | KeyStates[VK_RSHIFT]   &(0x80);
            b32 ControlIsDown = KeyStates[VK_LCONTROL] &(0x80) | KeyStates[VK_RCONTROL] &(0x80);
            b32 AltIsDown =     KeyStates[VK_LMENU]    &(0x80) | KeyStates[VK_RMENU]    &(0x80);
            b32 WindowsIsDown = KeyStates[VK_LWIN]     &(0x80) | KeyStates[VK_RWIN]     &(0x80);

            if(!ControlIsDown && !(VKCode >= 0x01 && VKCode <= 0x2F || VKCode >= 0x5B && VKCode <= 0xFE))
            {
                KeyDown(Passin->State, (char)CharacterCode, 0, ShiftIsDown, ControlIsDown, AltIsDown, WindowsIsDown);
            }
        } break;

        case WM_SYSKEYDOWN:
        case WM_KEYDOWN:
        {
            u8 VKCode = (u8)WParam;

            u8 KeyStates[256];
            GetKeyboardState(KeyStates);

            b32 ShiftIsDown =   KeyStates[VK_LSHIFT]   &(0x80) | KeyStates[VK_RSHIFT]   &(0x80);
            b32 ControlIsDown = KeyStates[VK_LCONTROL] &(0x80) | KeyStates[VK_RCONTROL] &(0x80);
            b32 AltIsDown =     KeyStates[VK_LMENU]    &(0x80) | KeyStates[VK_RMENU]    &(0x80);
            b32 WindowsIsDown = KeyStates[VK_LWIN]     &(0x80) | KeyStates[VK_RWIN]     &(0x80);

            if(ControlIsDown || VKCode >= 0x01 && VKCode <= 0x2F || VKCode >= 0x5B && VKCode <= 0xFE)
            {
                if((AltIsDown && VKCode == VK_RETURN) ||
                    VKCode == VK_F11)
                {
                    ToggleFullscreen(Window, &Passin->State->IsMaximized);
                    break;
                }

                // Todo: I have a feeling I don't need to do this...
                //       And this most likely won't work on other keyboard layouts
                char Key = 0;
                if(WParam == VK_OEM_1) { if(ShiftIsDown) Key = ':'; else Key = ';'; }
                else if(WParam == VK_OEM_PLUS) { if(ShiftIsDown) Key = '+'; else Key = '='; }
                else if(WParam == VK_OEM_COMMA) { if(ShiftIsDown) Key = '<'; else Key = ','; }
                else if(WParam == VK_OEM_MINUS) { if(ShiftIsDown) Key = '_'; else Key = '-'; }
                else if(WParam == VK_OEM_PERIOD) { if(ShiftIsDown) Key = '>'; else Key = '.'; }
                else if(WParam == VK_OEM_2) { if(ShiftIsDown) Key = '?'; else Key = '/'; }
                else if(WParam == VK_OEM_3) { if(ShiftIsDown) Key = '~'; else Key = '`'; }
                else if(WParam == VK_OEM_4) { if(ShiftIsDown) Key = '{'; else Key = '['; }
                else if(WParam == VK_OEM_5) { if(ShiftIsDown) Key = '|'; else Key = '\\'; }
                else if(WParam == VK_OEM_6) { if(ShiftIsDown) Key = '}'; else Key = ']'; }
                else if(WParam == VK_OEM_7) { if(ShiftIsDown) Key = '"'; else Key = '\''; }
                else if(WParam == VK_OEM_8) { if(ShiftIsDown) Key = 'W'; else Key = 'W'; }
                else if(WParam == '0') { if(ShiftIsDown) Key = ')'; else Key = '0'; }
                else if(WParam == '1') { if(ShiftIsDown) Key = '!'; else Key = '1'; }
                else if(WParam == '2') { if(ShiftIsDown) Key = '@'; else Key = '2'; }
                else if(WParam == '3') { if(ShiftIsDown) Key = '#'; else Key = '3'; }
                else if(WParam == '4') { if(ShiftIsDown) Key = '$'; else Key = '4'; }
                else if(WParam == '5') { if(ShiftIsDown) Key = '%'; else Key = '5'; }
                else if(WParam == '6') { if(ShiftIsDown) Key = '^'; else Key = '6'; }
                else if(WParam == '7') { if(ShiftIsDown) Key = '&'; else Key = '7'; }
                else if(WParam == '8') { if(ShiftIsDown) Key = '*'; else Key = '8'; }
                else if(WParam == '9') { if(ShiftIsDown) Key = '('; else Key = '9'; }
                else if(WParam == 'A') { if(ShiftIsDown) Key = 'A'; else Key = 'a'; }
                else if(WParam == 'B') { if(ShiftIsDown) Key = 'B'; else Key = 'b'; }
                else if(WParam == 'C') { if(ShiftIsDown) Key = 'C'; else Key = 'c'; }
                else if(WParam == 'D') { if(ShiftIsDown) Key = 'D'; else Key = 'd'; }
                else if(WParam == 'E') { if(ShiftIsDown) Key = 'E'; else Key = 'e'; }
                else if(WParam == 'F') { if(ShiftIsDown) Key = 'F'; else Key = 'f'; }
                else if(WParam == 'G') { if(ShiftIsDown) Key = 'G'; else Key = 'g'; }
                else if(WParam == 'H') { if(ShiftIsDown) Key = 'H'; else Key = 'h'; }
                else if(WParam == 'I') { if(ShiftIsDown) Key = 'I'; else Key = 'i'; }
                else if(WParam == 'J') { if(ShiftIsDown) Key = 'J'; else Key = 'j'; }
                else if(WParam == 'K') { if(ShiftIsDown) Key = 'K'; else Key = 'k'; }
                else if(WParam == 'L') { if(ShiftIsDown) Key = 'L'; else Key = 'l'; }
                else if(WParam == 'M') { if(ShiftIsDown) Key = 'M'; else Key = 'm'; }
                else if(WParam == 'N') { if(ShiftIsDown) Key = 'N'; else Key = 'n'; }
                else if(WParam == 'O') { if(ShiftIsDown) Key = 'O'; else Key = 'o'; }
                else if(WParam == 'P') { if(ShiftIsDown) Key = 'P'; else Key = 'p'; }
                else if(WParam == 'Q') { if(ShiftIsDown) Key = 'Q'; else Key = 'q'; }
                else if(WParam == 'R') { if(ShiftIsDown) Key = 'R'; else Key = 'r'; }
                else if(WParam == 'S') { if(ShiftIsDown) Key = 'S'; else Key = 's'; }
                else if(WParam == 'T') { if(ShiftIsDown) Key = 'T'; else Key = 't'; }
                else if(WParam == 'U') { if(ShiftIsDown) Key = 'U'; else Key = 'u'; }
                else if(WParam == 'V') { if(ShiftIsDown) Key = 'V'; else Key = 'v'; }
                else if(WParam == 'W') { if(ShiftIsDown) Key = 'W'; else Key = 'w'; }
                else if(WParam == 'X') { if(ShiftIsDown) Key = 'X'; else Key = 'x'; }
                else if(WParam == 'Y') { if(ShiftIsDown) Key = 'Y'; else Key = 'y'; }
                else if(WParam == 'Z') { if(ShiftIsDown) Key = 'Z'; else Key = 'z'; }

                KeyDown(Passin->State, Key, VKCode, ShiftIsDown, ControlIsDown, AltIsDown, WindowsIsDown);
            }
        } break;

        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
        {
            s32 ResizeBorder = 5;

            RECT WindowRect;
            // GetWindowRect(Window, &WindowRect);
            GetClientRect(Window, &WindowRect);
            int WindowWidth = WindowRect.right - WindowRect.left;
            int WindowHeight = WindowRect.bottom - WindowRect.top;

            s32 WindowLeft =   WindowRect.left;
            s32 WindowRight =  WindowRect.right;
            s32 WindowTop =    WindowRect.top;
            s32 WindowBottom = WindowRect.bottom;

            s32 XPosition = (int)(short)LOWORD((DWORD)LParam) + WindowRect.left;
            s32 YPosition = (int)(short)HIWORD((DWORD)LParam) + WindowRect.top;

            mouse_state MouseState;
            switch(Message)
            {
                case WM_LBUTTONDOWN: { MouseState = MouseState_left_pressed; } break;
                case WM_LBUTTONUP: { MouseState = MouseState_left_released; } break;
                case WM_RBUTTONDOWN: { MouseState = MouseState_right_pressed; } break;
                case WM_RBUTTONUP: { MouseState = MouseState_right_released; } break;
                InvalidDefaultCase;
            }

            PreviousClickX = XPosition;
            PreviousClickY = YPosition;
        } break;

        case WM_LBUTTONDBLCLK:
        {
            s32 ResizeBorder = 5;

            RECT WindowRect;
            // GetWindowRect(Window, &WindowRect);
            GetClientRect(Window, &WindowRect);
            int WindowWidth = WindowRect.right - WindowRect.left;
            int WindowHeight = WindowRect.bottom - WindowRect.top;

            s32 WindowLeft =   WindowRect.left;
            s32 WindowRight =  WindowRect.right;
            s32 WindowTop =    WindowRect.top;
            s32 WindowBottom = WindowRect.bottom;

            s32 XPosition = (int)(short)LOWORD((DWORD)LParam) + WindowRect.left;
            s32 YPosition = (int)(short)HIWORD((DWORD)LParam) + WindowRect.top;

            if(PointInRectangle(Recti(WindowLeft + ResizeBorder, WindowTop + ResizeBorder, WindowRight - ResizeBorder, WindowBottom - ResizeBorder),
                V2((r32)XPosition, (r32)YPosition)))
            {
                ToggleFullscreen(Window, &Passin->State->IsMaximized);
            }
        } break;

#if 0
        case WM_MOUSEMOVE:
        {
            WINDOWINFO WindowInfo;
            GetWindowInfo(Window, &WindowInfo);
            int WindowWidth = WindowInfo.rcWindow.right - WindowInfo.rcWindow.left;
            int WindowHeight = WindowInfo.rcWindow.bottom - WindowInfo.rcWindow.top;

            s32 WindowLeft = WindowInfo.rcWindow.left;
            s32 WindowRight = WindowInfo.rcWindow.right;
            s32 WindowTop = WindowInfo.rcWindow.top;
            s32 WindowBottom = WindowInfo.rcWindow.bottom;

            s32 XPosition = (int)(short)LOWORD(LParam) + WindowInfo.rcWindow.left;
            s32 YPosition = (int)(short)HIWORD(LParam) + WindowInfo.rcWindow.top;

            s32 ResizeBorder = 5;

            recti WindowRecti = {WindowLeft, WindowTop,
                                 WindowRight, WindowBottom};
            recti NonInteractionWindowRecti = {WindowLeft + ResizeBorder, WindowTop + ResizeBorder,
                                               WindowRight - ResizeBorder, WindowBottom - ResizeBorder};


            if(BitfieldTest(Passin->ResizingField, ResizingField_active_resizing) &&
               BitfieldTest(Passin->ResizingField, ResizingField_hot_resizing))
            {
                if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_nw))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft - (Passin->State->MouseX - XPosition), WindowTop - (Passin->State->MouseY - YPosition),
                        WindowWidth + (Passin->State->MouseX - XPosition), WindowHeight + (Passin->State->MouseY - YPosition),
                        0);
                }
                else if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_ne))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft, WindowTop - (Passin->State->MouseY - YPosition),
                        WindowWidth - (Passin->State->MouseX - XPosition), WindowHeight + (Passin->State->MouseY - YPosition),
                        0);
                }
                else if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_se))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft, WindowTop,
                        WindowWidth - (Passin->State->MouseX - XPosition), WindowHeight - (Passin->State->MouseY - YPosition),
                        0);
                }
                else if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_sw))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft - (Passin->State->MouseX - XPosition), WindowTop,
                        WindowWidth + (Passin->State->MouseX - XPosition), WindowHeight - (Passin->State->MouseY - YPosition),
                        0);
                }
                else if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_s))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft, WindowTop,
                        WindowWidth, WindowHeight - (Passin->State->MouseY - YPosition),
                        0);
                }
                else if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_w))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft - (Passin->State->MouseX - XPosition), WindowTop,
                        WindowWidth + (Passin->State->MouseX - XPosition), WindowHeight,
                        0);
                }
                else if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_e))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft, WindowTop,
                        WindowWidth - (Passin->State->MouseX - XPosition), WindowHeight,
                        0);
                }
                else if(BitfieldTest(Passin->ResizingField, ResizingField_is_resizing_n))
                {
                    SetWindowPos(Window, 0,
                        WindowLeft, WindowTop - (Passin->State->MouseY - YPosition),
                        WindowWidth, WindowHeight + (Passin->State->MouseY - YPosition),
                        0);
                }
            }
            else if(PointInRectangle(WindowRecti, V2((r32)XPosition, (r32)YPosition)))
            {
                if(!PointInRectangle(NonInteractionWindowRecti, V2((r32)XPosition, (r32)YPosition)))
                {
                    HCURSOR WindowsCursor = LoadCursor(0, IDC_ARROW);
                    if(PointInRectangle(Recti(WindowLeft, WindowTop, WindowLeft + ResizeBorder, WindowTop + ResizeBorder), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Top Left
                        WindowsCursor = LoadCursor(0, IDC_SIZENWSE);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_nw);
                    }
                    else if(PointInRectangle(Recti(WindowRight - ResizeBorder, WindowBottom - ResizeBorder, WindowRight, WindowBottom), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Bottom Right
                        WindowsCursor = LoadCursor(0, IDC_SIZENWSE);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_se);
                    }
                    else if(PointInRectangle(Recti(WindowRight - ResizeBorder, WindowTop, WindowRight, WindowTop + ResizeBorder), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Top Right
                        WindowsCursor = LoadCursor(0, IDC_SIZENESW);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_ne);
                    }
                    else if(PointInRectangle(Recti(WindowLeft, WindowBottom - ResizeBorder, WindowLeft + ResizeBorder, WindowBottom), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Bottom Left
                        WindowsCursor = LoadCursor(0, IDC_SIZENESW);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_sw);
                    }
                    else if(PointInRectangle(Recti(WindowLeft, WindowTop, WindowRight, WindowTop + ResizeBorder), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Top
                        WindowsCursor = LoadCursor(0, IDC_SIZENS);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_n);
                    }
                    else if(PointInRectangle(Recti(WindowLeft, WindowBottom - ResizeBorder, WindowRight, WindowBottom), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Bottom
                        WindowsCursor = LoadCursor(0, IDC_SIZENS);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_s);
                    }
                    else if(PointInRectangle(Recti(WindowLeft, WindowTop, WindowLeft + ResizeBorder, WindowBottom), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Left
                        WindowsCursor = LoadCursor(0, IDC_SIZEWE);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_w);
                    }
                    else if(PointInRectangle(Recti(WindowRight - ResizeBorder, WindowTop, WindowRight, WindowBottom), V2((r32)XPosition, (r32)YPosition)))
                    { // Note: Right
                        WindowsCursor = LoadCursor(0, IDC_SIZEWE);
                        BitfieldSet(Passin->ResizingField, ResizingField_active_resizing);
                        BitfieldSet(Passin->ResizingField, ResizingField_is_resizing_e);
                    }

                    SetCursor(WindowsCursor);
                }
                else if(PointInRectangle(Recti(WindowLeft + ResizeBorder, WindowTop + ResizeBorder, WindowRight - ResizeBorder, WindowBottom - ResizeBorder),
                    V2((r32)XPosition, (r32)YPosition)))
                {
                    SetCursor(LoadCursor(0, IDC_ARROW));
                    Passin->ResizingField = 0;
                }
                else
                {
                    HCURSOR WindowsCursor = LoadCursor(0, IDC_ARROW);
                    SetCursor(WindowsCursor);

                    Passin->ResizingField = 0;
                }
            }

            if(Passin->IsDraggingWindow)
            {
                if(Passin->State->IsMaximized)
                {
                    int PrevWindowWidth =  WindowWidth;
                    int PrevWindowHeight = WindowHeight;

                    ToggleFullscreen(Window, &Passin->State->IsMaximized);

                    GetWindowInfo(Window, &WindowInfo);
                    WindowWidth =  WindowInfo.rcWindow.right - WindowInfo.rcWindow.left;
                    WindowHeight = WindowInfo.rcWindow.bottom - WindowInfo.rcWindow.top;

                    SetWindowPos(Window, 0,
                        (s32)((r32)WindowWidth* ((r32)(XPosition)/(r32)PrevWindowWidth) + 0.5f),
                        (s32)((r32)WindowHeight*((r32)(YPosition)/(r32)PrevWindowHeight) + 0.5f),
                        WindowWidth, WindowHeight, 0);
                }
                else
                {
                    SetWindowPos(Window, 0,
                                 WindowInfo.rcWindow.left + (XPosition - Passin->State->MouseX),
                                 WindowInfo.rcWindow.top  + (YPosition - Passin->State->MouseY),
                                 WindowWidth, WindowHeight, 0);
                }
            }


            Passin->State->MouseX = XPosition;
            Passin->State->MouseY = YPosition;
        } break;
#endif

        case WM_SETFOCUS:
        {
            // Passin->State->Pause = false;
            SetCursor(LoadCursor(0, IDC_ARROW));
            // SetCapture(Window);
        } break;
        case WM_KILLFOCUS:
        {
            // Passin->State->Pause = true;
        } break;

        default:
        {
            Result = DefWindowProcA(Window, Message, WParam, LParam);
        }
    }

    return Result;
}

typedef struct
{
    char *StringToPrint;
} work_queue_entry;

global volatile u32 EntriesCompleted;
global volatile u32 NextEntryToDo;
global volatile u32 EntryCount;
work_queue_entry Entries[1024];

#define CompletePastWritesBeforeFutureWrites _WriteBarrier(); _mm_sfence()

void
PushStringTest(HANDLE SemaphoreHandle, char *String)
{
    Assert(EntryCount < ArrayCount(Entries));
    work_queue_entry *Entry = Entries + EntryCount;
    Entry->StringToPrint = String;

    _WriteBarrier(); _mm_sfence();

    // Note: This is incremented at the end for multithreaded purposes
    //       In case it is incremented when it's accessed and a thread tries to access a string
    //       before it's there
    ++EntryCount;

    // Note: Wake dem threads up, damn they be sleepin
    ReleaseSemaphore(SemaphoreHandle, 1, 0);
}

typedef struct
{
    HANDLE SemaphoreHandle;
    int ThreadIndex;
} win32_thread_info;

b32
DoWorkerWork(int ThreadIndex)
{
    b32 DidWork = false;
    if(NextEntryToDo < EntryCount)
    {
        int EntryIndex = InterlockedIncrement((volatile LONG *)&NextEntryToDo) - 1;
        _ReadBarrier();
        work_queue_entry *Entry = Entries + EntryIndex;

        char Buffer[512];
        FormatString(sizeof(Buffer), Buffer, "Thread %u: %s\n", ThreadIndex, Entry->StringToPrint);
        OutputDebugStringA(Buffer);

        InterlockedIncrement((volatile LONG *)&EntriesCompleted);
        DidWork = true;
    }

    return DidWork;
}

DWORD WINAPI
ThreadProc(void *Parameter)
{
    win32_thread_info *ThreadInfo = (win32_thread_info *)Parameter;

    for(;;)
    {
        if(!DoWorkerWork(ThreadInfo->ThreadIndex))
        {
            WaitForSingleObjectEx(ThreadInfo->SemaphoreHandle, INFINITE, false);
        }
    }
}

int CALLBACK
WinMain(HINSTANCE Instance, HINSTANCE PrevInstance, LPSTR CommandLine, int ShowCommand)
{

#if 0
    win32_thread_info ThreadInfo[7];

    u32 InitialCount = 0;
    u32 ThreadCount = ArrayCount(ThreadInfo);
    HANDLE SemaphoreHandle = CreateSemaphoreEx(0, InitialCount, ThreadCount, 0, 0, SEMAPHORE_ALL_ACCESS);

    for(int ThreadIndex = 0;
        ThreadIndex < ArrayCount(ThreadInfo);
        ++ThreadIndex)
    {
        win32_thread_info *Info = ThreadInfo + ThreadIndex;
        Info->SemaphoreHandle = SemaphoreHandle;
        Info->ThreadIndex = ThreadIndex;

        DWORD ThreadID;
        HANDLE ThreadHandle = CreateThread(0, 0, ThreadProc, Info, 0, &ThreadID);
        CloseHandle(ThreadHandle);
    }

    PushString(SemaphoreHandle, "String 0");
    PushString(SemaphoreHandle, "String 1");
    PushString(SemaphoreHandle, "String 2");
    PushString(SemaphoreHandle, "String 3");
    PushString(SemaphoreHandle, "String 4");
    PushString(SemaphoreHandle, "String 5");
    PushString(SemaphoreHandle, "String 6");
    PushString(SemaphoreHandle, "String 7");
    PushString(SemaphoreHandle, "String 8");
    PushString(SemaphoreHandle, "String 9");

    PushString(SemaphoreHandle, "String 10");
    PushString(SemaphoreHandle, "String 11");
    PushString(SemaphoreHandle, "String 12");
    PushString(SemaphoreHandle, "String 13");
    PushString(SemaphoreHandle, "String 14");
    PushString(SemaphoreHandle, "String 15");
    PushString(SemaphoreHandle, "String 16");
    PushString(SemaphoreHandle, "String 17");
    PushString(SemaphoreHandle, "String 18");
    PushString(SemaphoreHandle, "String 19");

    while(EntryCount != NextEntryToDo) { DoWorkerWork(7); }
#endif

    char FontPathTemp[MAX_PATH];
    GetWindowsDirectoryA((LPSTR)FontPathTemp, MAX_PATH);

    StringCopy(FontPath, FontPathTemp);
    StringCat(FontPath, "\\Fonts\\");

    LARGE_INTEGER TempPerfFreq;
    QueryPerformanceFrequency(&TempPerfFreq);
    GlobalPerfFreq = (r32)TempPerfFreq.QuadPart;

    WNDCLASSA WindowClass = {};
    WindowClass.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    WindowClass.lpfnWndProc   = WindowProc;
    WindowClass.hInstance     = Instance;
    WindowClass.hIcon         = 0;
    WindowClass.hCursor       = LoadCursor(0, IDC_ARROW);
    // WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    WindowClass.lpszClassName = "TextEditorWindowClass";

    if(!RegisterClassA(&WindowClass))
    {
        int Error = GetLastError();
        InvalidCodePath;
        return -1;
    }

    int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);

    editor_state EditorState = {};
    EditorState.CommandLine = (char *)CommandLine;
    EditorState.Config.ConfigReloaded = true;

    GlobalEditorMemory.PermanentMemorySize = Kilo(512);
    GlobalEditorMemory.TransientMemorySize = Mega(10);

    GlobalEditorMemory.MainMemoryBlockSize = GlobalEditorMemory.PermanentMemorySize + GlobalEditorMemory.TransientMemorySize;
    GlobalEditorMemory.MainMemoryBlock = VirtualAlloc(0, GlobalEditorMemory.MainMemoryBlockSize, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);

    GlobalEditorMemory.PermanentMemory = GlobalEditorMemory.MainMemoryBlock;
    GlobalEditorMemory.TransientMemory = ((u8 *)GlobalEditorMemory.MainMemoryBlock + GlobalEditorMemory.PermanentMemorySize);

    EditorState.CommentKeywordHash.CommentKeywordCount = 32;
    EditorState.CommentKeywordHash.CommentHashArray = (comment_hash_slot *)Allocate(sizeof(comment_hash_slot)*EditorState.CommentKeywordHash.CommentKeywordCount);

    ReadConfig(&EditorState.Config, &EditorState.CommentKeywordHash, 0);

    window_proc_passin Passin = {};
    Passin.State = &EditorState;

    int WantedWindowWidth = ScreenWidth*2/3; // 960;
    int WantedWindowHeight = ScreenHeight*2/3; // 540;

    HWND Window = CreateWindowExA(0,
                                  WindowClass.lpszClassName,
                                  "TexEdtr",
                                  0,
                                  (ScreenWidth - WantedWindowWidth)/2, (ScreenHeight - WantedWindowHeight)/2, 
                                  WantedWindowWidth, WantedWindowHeight,
                                  0,
                                  0,
                                  Instance,
                                  &Passin);

    if(!Window)
    {
        int Error = GetLastError();
        InvalidCodePath;
        return -2;
    }

    RECT WindowRect;
    // GetWindowRect(Window, &WindowRect);
    GetClientRect(Window, &WindowRect);
    int WindowWidth = WindowRect.right - WindowRect.left;
    int WindowHeight = WindowRect.bottom - WindowRect.top;

    WindowDeviceContext = GetDC(Window);

    Win32InitOpenGL(WindowDeviceContext);
    ResizeBackBuffer(&BackBuffer, WindowWidth, WindowHeight);

#if 1
    SetWindowLong(Window, GWL_STYLE, WS_OVERLAPPEDWINDOW);
    ShowWindow(Window, SW_SHOW);
#endif

    DragAcceptFiles(Window, true);

    r32 dt = 0.0f; // 0.03333333f;
#if 1
    r32 TargetFramerate = 0.016666666f; // 0.006944444f;
#else
    r32 TargetFramerate = 0.0f;
#endif

    LARGE_INTEGER LastCounter;
    LARGE_INTEGER EndCounter;
    QueryPerformanceCounter(&LastCounter);
    MSG Message;

    EditorState.Running = true;
    while(EditorState.Running)
    {
        while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&Message);
            DispatchMessageA(&Message);
        }
        if(!EditorState.Pause)
        {
#if 0
            SYSTEMTIME Time;
            GetLocalTime(&Time);
            // Note:
            // Time.wYear;
            // Time.wMonth;
            // Time.wDayOfWeek;
            // Time.wDay;
            // Time.wHour;
            // Time.wMinute;
            // Time.wSecond;
            // Time.wMilliseconds;

            FormatString(EditorState.LocalTime, "%02u:%02u", Time.wHour, Time.wMinute);
#endif


            if(*EditorState.Config.Filename)
            {
                CheckConfig(&EditorState.Config, &EditorState.CommentKeywordHash);
            }

            editor_backbuffer EditorBackBuffer = {};
            EditorBackBuffer.Memory =      BackBuffer.Memory;
            EditorBackBuffer.Width =  (u16)BackBuffer.Width; // Todo: Come back with a proper conversion function
            EditorBackBuffer.Height = (u16)BackBuffer.Height;

            UpdateAndRender(&EditorState, &EditorBackBuffer, &GlobalEditorMemory, dt);
            
            BlitBackBuffer(&BackBuffer, WindowDeviceContext, BackBuffer.Width, BackBuffer.Height);

            QueryPerformanceCounter(&EndCounter);
            dt = (r32)(EndCounter.QuadPart - LastCounter.QuadPart) / GlobalPerfFreq;

            if(dt < TargetFramerate)
            {
                DWORD SleepMS = (DWORD)(TargetFramerate - dt);

                while(dt < TargetFramerate)
                {
                    Sleep(SleepMS);
                    QueryPerformanceCounter(&EndCounter);
                    dt = (r32)(EndCounter.QuadPart - LastCounter.QuadPart) / GlobalPerfFreq;
                    SleepMS = (DWORD)(TargetFramerate - dt);
                }
            }

#if 0
            char buffer[256];
            FormatString(buffer, "dt(%0.4f) fps(%0.1f)\n", dt, (1.0f/dt));
            OutputDebugStringA(buffer);
#endif

            LastCounter = EndCounter;
        }
        else
        {
            Sleep(120);
        }
    }

    return 0;
}

void 
WinMainCRTStartup()
{
    int Result = WinMain(GetModuleHandle(0), 0, 0, 0);
    ExitProcess(Result);
}