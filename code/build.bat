@ECHO off

REM You might not be able to build in 32bit as we removed CRT and didn't add 64bit -> 32bit floating point/integer code


ctime -begin build_stats.ctm

pushd ..\build

REM -arch:IA32 for 32-bit builds
set CompilerFlags=-nologo -Od -MTd -fp:fast -fp:except- -EHa- -Gm- -GR- -Zi -Zo -WX -W4 -wd4201 -wd4100 -wd4189 -wd4505 -wd4127 -FC -DDEBUG -GS- -Gs9999999
set LinkerFlags=-nodefaultlib -subsystem:windows -incremental:no -opt:ref Gdi32.lib Opengl32.lib User32.lib Shell32.lib Kernel32.lib -STACK:0x100000,0x100000

cl %CompilerFlags% -Fetext_editor.exe ..\code\manifest.cpp /link %LinkerFlags%
set LastError=%ERRORLEVEL%

popd

ctime -end build_stats.ctm %LastError%
