
typedef enum token_type
{
    Token_Identifier,
    Token_Literal,
    Token_Number,
    Token_Keyword,
    Token_Operator,
    Token_Preprocessor,
    Token_Whitespace,
    Token_Comment,
    Token_CommentKeyword,
    Token_Unknown
} token_type;
struct token // Note: Prototyped in file.h
{
    // Note: These two are token_type, just reducing struct size
    u8 Type;
    u8 SubType;

    umm BytesIn;
    u16 BytesLong; // Note: These probably won't be too large, huh?
};

typedef struct comment_hash_slot comment_hash_slot;

struct comment_hash_slot
{
    v4 Color;
    u8 *Keyword;
    b32 IsCaseSensitive;

    comment_hash_slot *Next;
};

typedef struct comment_keyword_hash
{
    umm CommentKeywordCount;
    comment_hash_slot *CommentHashArray;
} comment_keyword_hash;

local inline b32
IsOperator(char Char)
{
    b32 Result = false;

    switch(Char)
    {
        case '(':
        case ')':
        case '*':
        case ',':
        case '.':
        case '&':
        case '^':
        case '!':
        case '[':
        case ']':
        case '?':
        case ';':
        case ':':
        case '|':
        case '{':
        case '}':
        case '=':
        case '<':
        case '>':
        case '-':
        case '+':
        case '%':
        {
            Result = true;
        }
    }

    return Result;
}

local inline b32
IsNumeric(char Char)
{
    b32 Result = false;

    if(Char >= '0' && Char <= '9')
    {
        Result = true;
    }

    return Result;
}

local inline b32
IsAlphabetical(char Char)
{
    b32 Result = false;

    if(Char >= 'a' && Char <= 'z' ||
       Char >= 'A' && Char <= 'Z')
    {
        Result = true;
    }

    return Result;
}

local inline b32
IsKeyword(language Language, trie_node *Dictionary, u8 *String)
{
    b32 Result = false;

    switch(Language)
    {
        case Language_Jai:
        {
            Result = IsInTree(&Dictionary[Dictionary_jai_keyword], String);
        } break;
        case Language_Odin:
        {
            Result = IsInTree(&Dictionary[Dictionary_odin_keyword], String);
        } break;
        case Language_Cpp:
        {
            Result = IsInTree(&Dictionary[Dictionary_cpp_keyword], String);
        } break;
    }

    return Result;
}

local inline b32
IsCommentKeyword(comment_keyword_hash *CommentKeywordHash, u8 *String)
{
    b32 Result = false;
    
    u8 LowerString[1024];
    ToLowerString(LowerString, String);

    for(b32 Found = 0;
        Found < 2;
        ++Found)
    {
        umm ArrayIndex = ProvideValidHashIndexInSize(Found == 0 ? String : LowerString, CommentKeywordHash->CommentKeywordCount);

        for(comment_hash_slot *At = CommentKeywordHash->CommentHashArray + ArrayIndex;
            At;
            At = At->Next)
        {
            if(At->IsCaseSensitive)
            {
                if(CompareString(At->Keyword, String))
                {
                    Found = 2;
                    Result = true;
                    break;
                }
            }
            else
            {
                if(CompareString(At->Keyword, LowerString))
                {
                    Found = 2;
                    Result = true;
                    break;
                }
            }
        }
    }

    return Result;
}

local inline void
GetCppSubToken(token *Token, comment_keyword_hash *CommentKeywordHash, u8 *&At, u8 *FileStart, u8 *FileEnd, b32 IsMultiline)
{
    u8 Buffer[1024];

    switch(*At)
    {
        case '(':
        case ')':
        case '*':
        case ',':
        case '.':
        case '&':
        case '^':
        case '!':
        case '[':
        case ']':
        case '?':
        case ';':
        case ':':
        case '|':
        case '{':
        case '}':
        case '=':
        case '<':
        case '>':
        case '-':
        case '+':
        case '%':
        case '/':
        {
            ++At;
            if(At[-1] == '*' && *At == '/')
            {
                ++At;
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Token->SubType = Token_Operator;
                break;
            }
            while(At <= FileEnd && IsOperator(*At))
            {
                ++At;
            }
            Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
            Assert(Token->BytesLong > 0);
            Token->SubType = Token_Operator;
        } break;

        case '\\':
        {
            ++At;
            Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
            Assert(Token->BytesLong > 0);
            Token->SubType = Token_Preprocessor;
        } break;
        case '#':
        {
            ++At;
            while(At <= FileEnd && IsAlphabetical(*At))
            {
                ++At;
            }
            Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
            Assert(Token->BytesLong > 0);
            Token->SubType = Token_Preprocessor;
        } break;

        default:
        {
            if(IsWhitespace(*At))
            {
                ++At;
                while(At < FileEnd && IsWhitespace(*At))
                {
                    if(!IsMultiline && *At != '\n' && *At != '\r')
                    {
                        // Note: This might make it play weird with other aspects of the program
                        // splitting the whitespace like this...
                        break;
                    }
                    ++At;
                }
                Token->SubType = Token_Whitespace;
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
            }
            else if(IsAlphabetical(*At) || *At == '_')
            {
                ++At;
                while(At < FileEnd && (IsAlphabetical(*At) || IsNumeric(*At) || *At == '_'))
                {
                    ++At;
                }

                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);

                // Note: Can't have an identifier that's over 1023 characters long, for now
                umm ComputedTokenLength = Min(sizeof(Buffer) - 1, Token->BytesLong);
                StringCopyLength(Buffer, FileStart + Token->BytesIn, ComputedTokenLength);
                Buffer[ComputedTokenLength] = 0;

                if(Token->Type == Token_Comment && IsCommentKeyword(CommentKeywordHash, Buffer))
                {
                    Token->SubType = Token_CommentKeyword;
                }
                else
                {
                    Token->SubType = Token_Identifier;
                }
            }
            else if(IsNumeric(*At))
            {
                ++At;
                while(At < FileEnd && IsNumeric(*At) || *At == '.' || *At == 'x' || *At == 'X' || (*At >= 'a' && *At <= 'f') || (*At >= 'A' && *At <= 'F'))
                {
                    ++At;
                }

                Token->SubType = Token_Number;
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
            }
            else
            { // Todo: What to do here?
                ++At;
                Token->SubType = Token_Unknown;
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
            }
        } break;
    }
}

void
LexateCpp(file_buffer *FileBuffer, comment_keyword_hash *CommentKeywordHash, trie_node *Dictionary)
{
    StartTimedPoint(Lexer);
    u8 *At = FileBuffer->Memory;
    u8 *FileStart = FileBuffer->Memory;
    u8 *FileEnd = FileBuffer->Memory + FileBuffer->Size;

    lexer_assemblage *LexerAssemblage = &FileBuffer->LexerAssemblage;
    
    if(LexerAssemblage->Tokens)
    {
        Deallocate(LexerAssemblage->Tokens);
        LexerAssemblage->Count = 0;
    }

    LexerAssemblage->Tokens = (token *)Allocate(sizeof(token));
    LexerAssemblage->MaxCount = 1;

    u8 Buffer[1024];
    b32 SkipToken = false;
    token *Token = 0;

    StartCountingPoint(Tokens);
    while(At < FileEnd)
    {        
        switch(*At)
        {
            case '(':
            case ')':
            case '*':
            case ',':
            case '.':
            case '&':
            case '^':
            case '!':
            case '[':
            case ']':
            case '?':
            case ';':
            case ':':
            case '|':
            case '{':
            case '}':
            case '=':
            case '<':
            case '>':
            case '-':
            case '+':
            case '%':
            {
                if(SkipToken)
                {
                    SkipToken = false;
                }
                else
                {
                    Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                }

                Token->BytesIn = At - FileStart;
                ++At;
                while(At <= FileEnd && IsOperator(*At))
                {
                    ++At;
                }
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
                Token->Type = Token_Operator;
            } break;

            case '\\':
            {
                if(SkipToken)
                {
                    SkipToken = false;
                }
                else
                {
                    Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                }

                Token->BytesIn = At - FileStart;
                ++At;
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
                Token->Type = Token_Preprocessor;
            } break;
            case '#':
            {
                if(SkipToken)
                {
                    SkipToken = false;
                }
                else
                {
                    Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                }

                Token->BytesIn = At - FileStart;
                ++At;
                while(At <= FileEnd && IsAlphabetical(*At))
                {
                    ++At;
                }
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
                Token->Type = Token_Preprocessor;
            } break;

            case '/':
            {
                b32 FirstTimeThrough = true;
                umm FirstPoint = At - FileStart;
                ++At;
                if(At < FileEnd)
                {
                    if(*At == '/')
                    {
                        ++At;
                        if(!(At < FileEnd && *At != '\n' && *At != '\r'))
                        {
                            Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                            Token->BytesIn = At - FileStart;
                            Token->Type = Token_Comment;
                        }
                        while(At < FileEnd && *At != '\n' && *At != '\r')
                        {
                            Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                            if(FirstTimeThrough)
                            {
                                FirstTimeThrough = false;
                                Token->BytesIn = FirstPoint;
                            }
                            else
                            {
                                Token->BytesIn = At - FileStart;
                            }
                            Token->Type = Token_Comment;
                            GetCppSubToken(Token, CommentKeywordHash, At, FileStart, FileEnd, false);
                            // Note: GetCppSubToken handles Token->BytesLong
                        }
						if(FirstTimeThrough)
						{
							Token->BytesIn = FirstPoint;
						}
                        Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                    }
                    else if(*At == '*')
                    {
                        ++At;
                        if(!(At < FileEnd && !(At[0] == '*' && At[1] == '/')))
                        {
                            Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                            Token->BytesIn = At - FileStart;
                            Token->Type = Token_Comment;
                        }
                        while(At < FileEnd && !(At[0] == '*' && At[1] == '/'))
                        {
                            Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                            if(FirstTimeThrough)
                            {
                                FirstTimeThrough = false;
                                Token->BytesIn = FirstPoint;
                            }
                            else
                            {
                                Token->BytesIn = At - FileStart;
                            }
                            Token->Type = Token_Comment;
                            GetCppSubToken(Token, CommentKeywordHash, At, FileStart, FileEnd, true);
                            // Note: GetCppSubToken handles Token->BytesLong
                        }
                        // Note: Probably handled now through GetCppSubToken
                        if(At[0] == '*')
                        {
                            At += 2;
                        }
                        Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                    }
                    else
                    {
                        Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                        Token->BytesIn = FirstPoint;
                        Token->Type = Token_Operator;
                        Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                    }
                }
                else
                {
                    Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                    Token->BytesIn = FirstPoint;
                    Token->Type = Token_Operator;
                    Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                }
                Assert(Token->BytesLong > 0);
            } break;

            case '\'':
            {
                if(SkipToken)
                {
                    SkipToken = false;
                }
                else
                {
                    Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                }

                Token->BytesIn = At - FileStart;
                ++At;
                while(At < FileEnd && At[0] != '\'')
                {
                    if(At[0] == '\\' && 
                       At + 1 < FileEnd)
                    {
                        ++At;
                    }
                    ++At;
                }
                ++At;
                Token->Type = Token_Literal;
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
            } break;

            case '"':
            {
                if(SkipToken)
                {
                    SkipToken = false;
                }
                else
                {
                    Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                }

                if(At[-1] == 'R')
                {
                    Token->BytesIn = At - FileStart - 1;
                }
                else
                {
                    Token->BytesIn = At - FileStart;
                }
                ++At;
                while(At < FileEnd && At[0] != '"')
                {
                    if(At[0] == '\\' &&
                        At + 1 < FileEnd)
                    {
                        ++At;
                    }
                    ++At;
                }
                ++At;
                Token->Type = Token_Literal;
                Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                Assert(Token->BytesLong > 0);
            } break;

            default:
            {
                if(SkipToken)
                {
                    SkipToken = false;
                }
                else
                {
                    Token = (token *)ArrayInsert(LexerAssemblage->Tokens, LexerAssemblage->Count, LexerAssemblage->MaxCount);
                }

                Token->BytesIn = At - FileStart;
                if(IsWhitespace(*At))
                {
                    ++At;
                    while(At < FileEnd && IsWhitespace(*At))
                    {
                        ++At;
                    }
                    Token->Type = Token_Whitespace;
                    Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                    Assert(Token->BytesLong > 0);
                }
                else if(IsAlphabetical(*At) || *At == '_')
                {
                    ++At;
                    if(At[-1] == 'R' && At[0] == '"')
                    { // Note: The R in R" is the start of a multiline literal
                        SkipToken = true;
                        break;
                    }
                    while(At < FileEnd && (IsAlphabetical(*At) || IsNumeric(*At) || *At == '_'))
                    {
                        ++At;
                    }

                    Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                    Assert(Token->BytesLong > 0);

                    // Note: Can't have an identifier that's over 1023 characters long, for now
                    umm ComputedTokenLength = Min(sizeof(Buffer) - 1, Token->BytesLong);
                    StringCopyLength(Buffer, FileBuffer->Memory + Token->BytesIn, ComputedTokenLength);
                    Buffer[ComputedTokenLength] = 0;

                    if(IsKeyword(FileBuffer->Language, Dictionary, Buffer))
                    {
                        Token->Type = Token_Keyword;
                    }
                    else
                    {
                        Token->Type = Token_Identifier;
                    }
                }
                else if(IsNumeric(*At))
                {
                    ++At;
                    while(At < FileEnd && IsNumeric(*At) || *At == '.' || *At == 'x' || *At == 'X' || (*At >= 'a' && *At <= 'f') || (*At >= 'A' && *At <= 'F'))
                    {
                        ++At;
                    }

                    Token->Type = Token_Number;
                    Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                    Assert(Token->BytesLong > 0);
                }
                else
                { // Todo: What to do here?
                    ++At;
                    Token->Type = Token_Unknown;
                    Token->BytesLong = (u16)((At - FileStart) - Token->BytesIn);
                    Assert(Token->BytesLong > 0);
                }
            } break;
        }

#if 0
        if(Token->Type == Token_unknown)
        {
            FormatString(sizeof(Buffer), Buffer, "LEXER Token %2d %c BytesIn:%6d, BytesLong:%4d\n", Token->Type, *(FileBuffer->Memory + Token->BytesIn), Token->BytesIn, Token->BytesLong);
            OutputDebugStringA(Buffer);
        }
#endif

        if(!SkipToken)
        {
            CycleCountingPoint(Tokens);
        }
    }
    EndCountingPoint(Tokens);

    FileBuffer->Parsed = true;

    EndTimedPoint(Lexer);
}

token *
GetTokenAtPoint(file_buffer *FileBuffer, umm BytesIn, umm *Index)
{
    token *Result = 0;
    lexer_assemblage *LexerAssemblage = &FileBuffer->LexerAssemblage;

    if(FileBuffer->Size && LexerAssemblage->Count && BytesIn >= 0 && BytesIn < FileBuffer->Size)
    {
        umm At = 0;
        b32 LocalRunning = true;
        u32x Num = 1;
        u32x Denom = 2;
        while(LocalRunning)
        {
            At = (LexerAssemblage->Count*Num/Denom);

            token *Token = LexerAssemblage->Tokens + At;

            if(BytesIn >= Token->BytesIn && BytesIn < Token->BytesIn + Token->BytesLong)
            {
                Result = Token;
                if(Index)
                {
                    *Index = At;
                }
                LocalRunning = false;
            }
            else
            {
                Denom *= 2;
                if(BytesIn <= Token->BytesIn)
                {
                    Num += Num - 1;
                }
                else
                {
                    Num += Num + 1;
                }
            }
        }
    }

    return Result;
}

local inline b32
TokenContainsNewline(file_buffer *FileBuffer, token *Token)
{
    b32 Result = false;

    if(Token->Type == Token_Whitespace)
    {
        for(u8 *At = FileBuffer->Memory + Token->BytesIn;
            At < FileBuffer->Memory + Token->BytesIn + Token->BytesLong;
            ++At)
        {
            if(*At == '\n')
            {
                Result = true;
                break;
            }
        }
    }

    return Result;
}

local inline void
GetTokenString(file_buffer *FileBuffer, token *Token, u8 *String)
{
    u8 *EndOfToken = FileBuffer->Memory + Token->BytesIn + Token->BytesLong;
    for(u8 *At = FileBuffer->Memory + Token->BytesIn;
        At < EndOfToken;
        ++At)
    {
        *String++ = *At;
    }
    *String = 0;
}

local inline void
AddDictionaryWords(file_buffer *FileBuffer, trie_node *Dictionary)
{
    StartCountingPoint(AddDictionaryWords);

    line_assemblage *LineAssemblage = &FileBuffer->LineAssemblage;
    lexer_assemblage *LexerAssemblage = &FileBuffer->LexerAssemblage;

    if(FileBuffer->Memory)
    {
        u8 Buffer[512];
        for(umm Index = 0;
            Index < LexerAssemblage->Count;
            ++Index)
        {
            token *Token = LexerAssemblage->Tokens + Index;

            if(Token->Type == Token_Keyword ||
               Token->Type == Token_Identifier)
            {
                u8 *Dest = Buffer;
                for(u8 *At = FileBuffer->Memory + Token->BytesIn;
                    At < FileBuffer->Memory + Token->BytesIn + Token->BytesLong;
                    ++At)
                {
                    *Dest++ = *At;
                }
                *Dest = 0;

                InsertNode(Dictionary, Buffer);
                CycleCountingPoint(AddDictionaryWords);
            }
        }
    }
    EndCountingPoint(AddDictionaryWords);
}

local inline void
RebuildDictionaryForLanguage(file_hash *FileHash, trie_node *Dictionary, language Language)
{
    StartTimedPoint(RebuildDictionary);

    trie_node *TempDictionary ={};
    switch(Language)
    {
        case Language_Jai:
        {
            TempDictionary = &Dictionary[Dictionary_jai];
        } break;
        case Language_Odin:
        {
            TempDictionary = &Dictionary[Dictionary_odin];
        } break;
        case Language_Cpp:
        {
            TempDictionary = &Dictionary[Dictionary_cpp];
        } break;
        default:
        {
            // Todo: 'Parse' this kinda like cpp maybe and then open this up?
            // Maybe without the comments/literal parts, probably what is wanted anyway
            return;
#if 0
            TempDictionary = &Dictionary[Dictionary_normal];
#endif
        } break;
    }

    StartTimedPoint(BurnTree);
    BurnTree(TempDictionary);
    EndTimedPoint(BurnTree);

    for(umm BufferIndex = 0;
        BufferIndex < FileHash->FileBufferCount;
        ++BufferIndex)
    {
        file_hash_slot *FileHashSlot = (FileHash->FileHashArray + BufferIndex);

        if(FileHashSlot->File)
        {
            for(file_hash_slot *At = FileHashSlot;
                At;
                At = At->Next)
            {
                file_buffer *FileBuffer = At->File;

                if(FileBuffer->Language == Language)
                {
                    AddDictionaryWords(FileBuffer, TempDictionary);
                }
            }
        }
    }

    EndTimedPoint(RebuildDictionary);
}

local inline void
PlaceCommentKeywordIntoHash(comment_keyword_hash *CommentKeywordHash, u8 *Keyword, b32 IsCaseSensitive, v4 Color)
{
    StartTimedPoint(PlaceCommentKeywordIntoHash);
    umm ArrayIndex = ProvideValidHashIndexInSize(Keyword, CommentKeywordHash->CommentKeywordCount);
    umm Length = StringLength(Keyword);

    for(comment_hash_slot *At = CommentKeywordHash->CommentHashArray + ArrayIndex;
        ;
        At = At->Next)
    {
        if(!At->Keyword)
        {
            At->Keyword = (u8 *)Allocate(Length + 1);
            StringCopy(At->Keyword, Keyword);
            At->IsCaseSensitive = IsCaseSensitive;
            At->Color = Color;
            At->Next = 0;
            break;
        }
        else if(CompareString(At->Keyword, Keyword))
        {
            break;
        }
        else if(!At->Next)
        {
            At->Next = (comment_hash_slot *)Allocate(sizeof(comment_hash_slot));
            At->Next->Keyword = (u8 *)Allocate(Length + 1);
            StringCopy(At->Next->Keyword, Keyword);
            At->Next->IsCaseSensitive = IsCaseSensitive;
            At->Next->Color = Color;
            At->Next->Next = 0;
            break;
        }
    }

    EndTimedPoint(PlaceCommentKeywordIntoHash);
}

local inline void
GetCommentKeywordInfoFromHash(comment_keyword_hash *CommentKeywordHash, u8 *Keyword, v4 *ColorInfo)
{
    if(!ColorInfo)
    {
        Assert(!"Um, what?");
        return;
    }

    u8 LowerString[1024];
    ToLowerString(LowerString, Keyword);

    for(b32 Found = 0;
        Found < 2;
        ++Found)
    {
        umm ArrayIndex = ProvideValidHashIndexInSize(Found == 0 ? Keyword : LowerString, CommentKeywordHash->CommentKeywordCount);

        for(comment_hash_slot *At = CommentKeywordHash->CommentHashArray + ArrayIndex;
            At;
            At = At->Next)
        {
            if(At->IsCaseSensitive)
            {
                if(CompareString(At->Keyword, Keyword))
                {
                    *ColorInfo = At->Color;
                    Found = true;
                    break;
                }
            }
            else
            {
                if(CompareString(At->Keyword, LowerString))
                {
                    *ColorInfo = At->Color;
                    Found = true;
                    break;
                }
            }
        }
    }
}
