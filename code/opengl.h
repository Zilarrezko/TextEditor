global unsigned int OpenGLBuffer;

typedef void *gl_gen_buffers(GLsizei Num, GLuint *Buffers);
global gl_gen_buffers *glGenBuffers;
typedef void *gl_bind_buffer(GLenum Target, GLuint Buffer);
global gl_bind_buffer *glBindBuffer;
typedef void gl_buffer_data(GLenum Target, GLsizeiptr Size, const GLvoid *Data, GLenum Usage);
global gl_buffer_data *glBufferData;
typedef void gl_enable_vertex_attrib_array(GLuint Index);
global gl_enable_vertex_attrib_array *glEnableVertexAttribArray;
typedef void gl_vertex_attrib_pointer(GLuint Index, GLint Size, GLenum Type, GLboolean Normalized, GLsizei Stride, const GLvoid *Pointer);
global gl_vertex_attrib_pointer *glVertexAttribPointer;
typedef GLuint gl_create_program(void);
global gl_create_program *glCreateProgram;
typedef GLuint gl_create_shader(GLenum ShaderType);
global gl_create_shader *glCreateShader;
typedef void gl_shader_source(GLuint Shader, GLsizei Count, char **String, GLint *Length);
global gl_shader_source *glShaderSource;
typedef void gl_compile_shader(GLuint Shader);
global gl_compile_shader *glCompileShader;
typedef void gl_get_shader_iv(GLuint Shader, GLenum ParameterName, GLint *Parameters);
global gl_get_shader_iv *glGetShaderiv;
typedef void gl_get_shader_info_log(GLuint Shader, GLsizei MaxLength, GLsizei *Length, char *InfoLog);
global gl_get_shader_info_log *glGetShaderInfoLog;
typedef void gl_attach_shader(GLuint Program, GLuint Shader);
global gl_attach_shader *glAttachShader;
typedef void gl_link_program(GLuint Program);
global gl_link_program *glLinkProgram;
typedef void gl_use_program(GLuint Program);
global gl_use_program *glUseProgram;
typedef GLint gl_get_uniform_location(GLuint Program, const char *Name);
global gl_get_uniform_location *glGetUniformLocation;
typedef void gl_uniform_2f(GLint Location, GLfloat F0, GLfloat F1);
global gl_uniform_2f *glUniform2f;
typedef void gl_uniform_1i(GLint Location, GLint I1);
global gl_uniform_1i *glUniform1i;
typedef void gl_buffer_sub_data(GLenum Target, GLintptr Offset, GLsizeiptr Size, const GLvoid *Data);
global gl_buffer_sub_data *glBufferSubData;

typedef struct opengl_info
{
    int OutputTargetDimLocation;
    // int IsTexturedLocation;
} opengl_info;

typedef struct render_buffer
{
    u8 *Vertices;
    u32 Size;
    u32 Used;
} render_buffer;

global render_buffer GlobalRenderBuffer;

local void
InitOpenGL()
{
    u32 PrimitiveSize = sizeof(r32)*48;

    glGenBuffers(1, &OpenGLBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, OpenGLBuffer);
    glBufferData(GL_ARRAY_BUFFER, PrimitiveSize, 0, GL_DYNAMIC_DRAW);

    GlobalRenderBuffer.Vertices = (u8 *)Allocate(sizeof(r32)*48);
    GlobalRenderBuffer.Size = sizeof(r32)*48;

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, 0, sizeof(r32) * 8, 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, 0, sizeof(r32) * 8, (void *)(sizeof(r32) * 2));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, 0, sizeof(r32) * 8, (void *)(sizeof(r32) * 6));
}

local void
CompileShaders(opengl_info *OpenGLInfo)
{
    u32 ShaderProgram = glCreateProgram();

    u32 VertexShader = glCreateShader(GL_VERTEX_SHADER);
    u32 FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    char *VertexCode =
        R"FOO(
        #version 330 core

        uniform vec2 OutputTargetDim;        

        layout(location = 0) in vec2 Position;
        
        layout(location = 1) in vec4 VertexColorIn;
        out vec4 VertexColor;

        layout(location = 2) in vec2 TexCoordIn;
        out vec2 TexCoord;
        
        void 
        main()
        {
            VertexColor = VertexColorIn;
            TexCoord = TexCoordIn;
            
            vec2 Projection = Position*vec2(2.0f, -2.0f)/OutputTargetDim - vec2(1.0f, -1.0f); 
            gl_Position = vec4(Projection, 0.0f, 1.0f);
        }
        )FOO";
    char *FragmentCode =
        R"FOO(
        #version 330 core

        in vec2 TexCoord;
        uniform sampler2D Texture;

        in vec4 VertexColor;
        out vec4 FinalColor;

        uniform bool IsTextured;

        void 
        main()
        {
            vec4 TextureColor = texture(Texture, TexCoord);
            // vec4 UnpackedTextureColor = vec4(TextureColor.r, TextureColor.r, TextureColor.r, TextureColor.r);
            FinalColor = TextureColor*VertexColor;
        }
        )FOO";

    glShaderSource(VertexShader, 1, &VertexCode, 0);
    glShaderSource(FragmentShader, 1, &FragmentCode, 0);
    glCompileShader(VertexShader);
    glCompileShader(FragmentShader);

    int CompileStatusVertex = 0;
    int CompileStatusFragment = 0;
    glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &CompileStatusVertex);
    glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &CompileStatusFragment);
    if(!CompileStatusVertex || !CompileStatusFragment)
    {
        int LengthVertex, LengthFragment;
        glGetShaderiv(VertexShader, GL_INFO_LOG_LENGTH, &LengthVertex);
        glGetShaderiv(FragmentShader, GL_INFO_LOG_LENGTH, &LengthFragment);
        
        char *LogInfo = (char *)Allocate(LengthVertex + LengthFragment + 1);
        glGetShaderInfoLog(VertexShader, LengthVertex, &LengthVertex, LogInfo);
        glGetShaderInfoLog(FragmentShader, LengthFragment, &LengthFragment, LogInfo + LengthVertex);

        InvalidCodePath;
    }

    glAttachShader(ShaderProgram, VertexShader);
    glAttachShader(ShaderProgram, FragmentShader);
    glLinkProgram(ShaderProgram);

    glUseProgram(ShaderProgram);

    OpenGLInfo->OutputTargetDimLocation = glGetUniformLocation(ShaderProgram, "OutputTargetDim");
    // OpenGLInfo->IsTexturedLocation = glGetUniformLocation(ShaderProgram, "IsTextured");
}

void
OpenGLRectangle(r32 *Vertices, v2 Position, v2 Position2, v4 Color, v2 UVMin, v2 UVMax)
{
    // 6 vertices, 48 total elements
    // X, Y, ColorR, ColorG, ColorB, TexU, TexV
    // X, Y, ColorR, ColorG, ColorB, TexU, TexV
    // X, Y, ColorR, ColorG, ColorB, TexU, TexV
    // X, Y, ColorR, ColorG, ColorB, TexU, TexV
    // X, Y, ColorR, ColorG, ColorB, TexU, TexV
    // X, Y, ColorR, ColorG, ColorB, TexU, TexV

    r32 MinPointX = Position.x;
    r32 MinPointY = Position.y;
    r32 MaxPointX = Position2.x;
    r32 MaxPointY = Position2.y;

    // Positions
    Vertices[0] =  MinPointX;
    Vertices[1] =  MinPointY;
    Vertices[8] =  MaxPointX;
    Vertices[9] =  MinPointY;
    Vertices[16] = MaxPointX;
    Vertices[17] = MaxPointY;
    Vertices[24] = MinPointX;
    Vertices[25] = MinPointY;
    Vertices[32] = MaxPointX;
    Vertices[33] = MaxPointY;
    Vertices[40] = MinPointX;
    Vertices[41] = MaxPointY;

    // Colors
    for(u32 At = 0;
        At < 48;
        At += 8)
    {
        Vertices[At + 2] = Color.r;
        Vertices[At + 3] = Color.g;
        Vertices[At + 4] = Color.b;
        Vertices[At + 5] = Color.a;
    }

    // Texture Coords
    Vertices[6] =  UVMin.u; Vertices[7] =  UVMin.v;
    Vertices[14] = UVMax.u; Vertices[15] = UVMin.v;
    Vertices[22] = UVMax.u; Vertices[23] = UVMax.v;
    Vertices[30] = UVMin.u; Vertices[31] = UVMin.v;
    Vertices[38] = UVMax.u; Vertices[39] = UVMax.v;
    Vertices[46] = UVMin.u; Vertices[47] = UVMax.v;
}

global u32 TextureBindCount = 0;

void
RenderGroupToBuffer(render_group *RenderGroup, font_atlas *FontAtlas, opengl_info *OpenGLInfo, bitmap *OutputTarget)
{
    // StartTimedPoint(RenderGroupToBuffer);
 
    if(FontAtlas->Handle)
    {
        glBindTexture(GL_TEXTURE_2D, FontAtlas->Handle);

        if(FontAtlas->Changed)
        {
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, FontAtlas->Width, FontAtlas->Height,
                            GL_RED, GL_UNSIGNED_BYTE, FontAtlas->Atlas);

            FontAtlas->Changed = false;
        }
    }
    else
    {
        FontAtlas->Handle = ++TextureBindCount;

        glBindTexture(GL_TEXTURE_2D, FontAtlas->Handle);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, FontAtlas->Width, FontAtlas->Height, 0,
                     GL_RED, GL_UNSIGNED_BYTE, FontAtlas->Atlas); // GL_BGRA_EXT

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

        int FontSwizzle[] = {GL_RED, GL_RED, GL_RED, GL_RED};
        glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, FontSwizzle);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    }

    glViewport(0, 0, OutputTarget->Width, OutputTarget->Height);
    
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glEnable(GL_SCISSOR_TEST);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    glUniform2f(OpenGLInfo->OutputTargetDimLocation, OutputTarget->Width, OutputTarget->Height);
    // glUniform1i(OpenGLInfo->IsTexturedLocation, true);

    GlobalRenderBuffer.Used = 0;
#if 0
    glClearColor(0.50f, 0.0f, 0.50f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // glUniform1i(OpenGLInfo->IsTexturedLocation, true);

    r32 MiddleX = RoundR32((r32)OutputTarget->Width/2.0f - (r32)FontAtlas->Width/2.0f);
    r32 MiddleY = RoundR32((r32)OutputTarget->Height/2.0f - (r32)FontAtlas->Height/2.0f);

    OpenGLRectangle((r32 *)GlobalRenderBuffer.Vertices, V2(MiddleX, MiddleY), V2(MiddleX + (r32)FontAtlas->Width, MiddleY + (r32)FontAtlas->Height), V4(1.0f, 1.0f, 1.0f, 1.0f),
                    V2(0.0f, 0.0f), V2(1.0f, 1.0f));

    glBufferData(GL_ARRAY_BUFFER, sizeof(r32)*48, GlobalRenderBuffer.Vertices, GL_DYNAMIC_DRAW);

    glDrawArrays(GL_TRIANGLES, 0, 6);
#else
    u32 PrimitiveSize = sizeof(r32)*48;
    u32 QuadCount = 0;

    u32 RenderBufferSizeBefore = GlobalRenderBuffer.Size;

    for(u32 EntryAddress = 0;
        EntryAddress < RenderGroup->GroupSize;
        )
    {
        render_group_entry_header *Header = (render_group_entry_header *)(RenderGroup->GroupBase + EntryAddress);
        EntryAddress += sizeof(*Header);

        void *Data = (u8 *)Header + sizeof(*Header);

        if(Header->Type != RenderEntryType_render_entry_clear)
        {
            if(GlobalRenderBuffer.Size < GlobalRenderBuffer.Used + PrimitiveSize)
            {
                GlobalRenderBuffer.Vertices = (u8 *)Reallocate(GlobalRenderBuffer.Vertices, GlobalRenderBuffer.Size, GlobalRenderBuffer.Size*2);
                GlobalRenderBuffer.Size *= 2;
            }
        }

        switch(Header->Type)
        {
        case RenderEntryType_render_entry_clear:
        {
            render_entry_clear *Entry = (render_entry_clear *)Data;

            glScissor(0, 0, OutputTarget->Width, OutputTarget->Height);

            Entry->Color = Entry->Color/255.0f;

            glClearColor(Entry->Color.r, Entry->Color.g, Entry->Color.b, Entry->Color.a);

            glClear(GL_COLOR_BUFFER_BIT);

            EntryAddress += sizeof(*Entry);
        } break;

        case RenderEntryType_render_entry_glyph:
        {
            render_entry_glyph *Entry = (render_entry_glyph *)Data;

            Entry->Color = Entry->Color/255.0f;
            
            OpenGLRectangle((r32 *)(GlobalRenderBuffer.Vertices + GlobalRenderBuffer.Used),
                            Entry->Position, Entry->Position2,
                            Entry->Color, Entry->Glyph->UVMin, Entry->Glyph->UVMax);

            EntryAddress += sizeof(*Entry);
            ++QuadCount;
            GlobalRenderBuffer.Used += PrimitiveSize;
        } break;
        case RenderEntryType_render_entry_bitmap:
        {
            render_entry_bitmap *Entry = (render_entry_bitmap *)Data;

            Assert(Entry->Bitmap->Memory);

            Entry->Color = Entry->Color/255.0f;
            Entry->Position = Entry->Position;

            OpenGLRectangle((r32 *)(GlobalRenderBuffer.Vertices + GlobalRenderBuffer.Used),
                            Entry->Position, Entry->Position + V2((r32)Entry->Bitmap->Width, (r32)Entry->Bitmap->Height), Entry->Color,
                            V2(0.0f, 0.0f), V2(1.0f, 1.0f));

            EntryAddress += sizeof(*Entry);
            ++QuadCount;
            GlobalRenderBuffer.Used += PrimitiveSize;
        } break;

        case RenderEntryType_render_entry_rectangle:
        {
            render_entry_rectangle *Entry = (render_entry_rectangle *)Data;

            Entry->Color = Entry->Color/255.0f;

            OpenGLRectangle((r32 *)(GlobalRenderBuffer.Vertices + GlobalRenderBuffer.Used),
                            Entry->Position, Entry->Position + Entry->Dim, Entry->Color,
                            V2(((r32)FontAtlas->Width - 1.0f)/(r32)FontAtlas->Width, ((r32)FontAtlas->Height - 1.0f)/(r32)FontAtlas->Height), V2(1.0f, 1.0f));

            EntryAddress += sizeof(*Entry);
            ++QuadCount;
            GlobalRenderBuffer.Used += PrimitiveSize;
        } break;

        InvalidDefaultCase;
        }
    }
 
    u32 RenderBufferSizeAfter = GlobalRenderBuffer.Size;
    if(RenderBufferSizeAfter > RenderBufferSizeBefore)
    {
        glBufferData(GL_ARRAY_BUFFER, GlobalRenderBuffer.Size, 0, GL_DYNAMIC_DRAW);
    }
    
    glScissor(0, 0, OutputTarget->Width, OutputTarget->Height);

    glBufferSubData(GL_ARRAY_BUFFER, 0, GlobalRenderBuffer.Used, GlobalRenderBuffer.Vertices);
    glDrawArrays(GL_TRIANGLES, 0, 6*QuadCount);
#endif

    // EndTimedPoint(RenderGroupToBuffer);
}