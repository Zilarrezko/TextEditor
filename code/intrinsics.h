
// #include <stdarg.h>
#include <intrin.h>

extern "C"
{
    int _fltused;
}


#define va_start __crt_va_start
#define va_arg   __crt_va_arg
#define va_end   __crt_va_end
#define va_copy(destination, source) ((destination) = (source))

#ifndef __cplusplus
#define false 0
#define true  1
#endif

typedef signed char  s8;
typedef short        s16;
typedef int          s32;
typedef long long    s64;
typedef s32          b32;
typedef s64          b64;

typedef unsigned char      u8;
typedef unsigned short     u16;
typedef unsigned int       u32;
typedef unsigned long long u64;

typedef float  r32;
typedef double r64;

// Note: __x86_64__ || __ppc64__ == GNU 64bit defines
#if _WIN64 || __x86_64__ || __ppc64__
typedef s64 s32x;
typedef u64 u32x;
typedef b64 b32x;
#else
typedef s32 s32x;
typedef u32 u32x;
typedef b32 b32x;
#endif

typedef uintptr_t umm;
typedef intptr_t  smm;

#define local      static
#define global     static
#define persistant static

#define InvalidCodePath *(int *)0 = 0
#define InvalidDefaultCase default: {InvalidCodePath;} break

#if defined(DEBUG)
#define Assert(Expression) if(!(Expression)){InvalidCodePath;}
#else
#define Assert(Expression)
#endif

// Note: It only works on arrays, not allocated pointers to data.
#define ArrayCount(Array) (sizeof((Array))/sizeof((Array)[0]))

#define Kilo(val) ((val)*1024)
#define Mega(val) (Kilo(val)*1024LL)
#define Giga(val) (Mega(val)*1024LL)

#define SwapValue(Val1, Val2) Val1^Val2;\
    Val2^Val1;\
    Val1^Val2

#define Abs(Val) (((Val) < 0) ? -(Val) : (Val))

#define Max(Val1, Val2) (((Val1) > (Val2)) ? (Val1) : (Val2))
#define Min(Val1, Val2) (((Val1) < (Val2)) ? (Val1) : (Val2))

#define HasSameSign(Val1, Val2) (((Val1) ^ (Val2)) >= 0)

#define PI32 3.14159265f
#define PI64 3.14159265358979323

#define BitfieldTest(Bitfield, Mask)   ((Bitfield) &  (Mask))
#define BitfieldSet(Bitfield, Mask)    ((Bitfield) |= (Mask))
#define BitfieldUnset(Bitfield, Mask)  ((Bitfield) &= (~(Mask)))
#define BitfieldToggle(Bitfield, Mask) ((Bitfield) ^= (Mask))

#define S32_MAX ((s32)(0x7FFFFFFF))
#define S32_MIN ((s32)(0x80000000))
#define S64_MAX ((s64)(0x7FFFFFFFFFFFFFFF))
#define S64_MIN ((s64)(0x8000000000000000))

#define U32_MAX ((u32)(0xFFFFFFFF))
#define U32_MIN ((u32)(0x00000000))
#define U64_MAX ((u32)(0xFFFFFFFFFFFFFFFF))
#define U64_MIN ((u32)(0x0000000000000000))

// Note: To preserve transparency
// ArrayInsert takes an item pointer,
// The array count (that will be changed),
// The maximum items it can hold (may change if reallocated),
// An Item that matches the type of the array, this Item will be inserted into the array.
// It will return a pointer to the item once in the array
#define ArrayInsert(Array, Count, MaxCount) _ArrayInsert((void **)&Array, sizeof(*Array), (umm *)&Count, (umm *)&MaxCount)
// Note: For when you can't take the size of an item in the array, namely void *
#define ArrayInsertWithSize(Array, Size, Count, MaxCount) _ArrayInsert((void **)&Array, Size, (umm *)&Count, (umm *)&MaxCount)

#if 0 // Note: This is some weird cpp black magic, I dare not unleash it
template <typename F>
struct privDefer
{
    F f;
    privDefer(F f) : f(f) {}
    ~privDefer() { f(); }
};

template <typename F>
privDefer<F> defer_func(F f)
{
    return privDefer<F>(f);
}

#define DEFER_1(x, y) x##y
#define DEFER_2(x, y) DEFER_1(x, y)
#define DEFER_3(x)    DEFER_2(x, __COUNTER__)
#define Defer(code)   auto DEFER_3(_defer_) = defer_func([&](){code;})
#endif

extern "C"
{
    void *memset(void *Dest, int Value, size_t Size);

    void *memcpy(void *Dest, void *Source, size_t Size);
}

#pragma function(memset)
#pragma function(memcpy)

void *memset(void *Dest, int Value, size_t Size)
{
    void *Result = Dest;

    u8 CastedValue = *(u8 *)&Value;
    u8 *At = (u8 *)Dest;
    while(Size--)
    {
        *At++ = CastedValue;
    }

    return Result;
}

void *memcpy(void *Dest, void *Source, size_t Size)
{
    void *Result = Dest;

    u8 *DestAt = (u8 *)Dest;
    u8 *SourceAt = (u8 *)Source;
    while(Size--)
    {
        *DestAt++ = *SourceAt++;
    }

    return Result;
}

typedef union
{
    struct
    {
        r32 x, y;
    };
    struct
    {
        r32 u, v;
    };
} v2;

typedef struct v2r64
{
    r64 x, y;
} v2r64;

typedef struct v2i
{
    s32 x, y;
} v2i;

typedef union
{
    struct
    {
        r32 x, y, z;
    };
    struct
    {
        r32 r, g, b;
    };
    struct
    {
        r32 xy;
        r32 _IGNORED0_;
    };
    struct
    {
        r32 _IGNORED1_;
        r32 yz;
    };
} v3;

typedef struct v3i
{
    s32 x, y, z;
} v3i;

typedef union
{
    struct
    {
        r32 x, y, z, w;
    };
    struct
    {
        r32 r, g, b, a;
    };
    struct
    {
        v3 rgb;
        r32 _IGNORED0_;
    };
    r32 E[4];
} v4;

typedef struct v4i
{
    s32 x, y, z, w;
} v4i;

typedef union
{
    struct
    {
        r32 x, y, z, w;
    };
    struct
    {
        v3 xyz;
        r32 _IGNORED0_;
    };
} quaternion;

typedef struct rect
{
    v2 P1;
    v2 P2;
} rect;

typedef union
{
    struct
    {
        v2i P1;
        v2i P2;
    };
    struct
    {
        s32 MinX, MinY;
        s32 MaxX, MaxY;
    };
} recti;

/*========================================

               Stuff That Is
             Required Somewhere
                  In Here

========================================*/

void *CopyMemory(void *Dest, void *Source, umm Size);
void *Allocate(umm Size);
void Deallocate(void *Memory);

/*========================================
                                
                Initializers

========================================*/

v2
V2(r32 X, r32 Y)
{
    v2 Result = {};

    Result.x = X;
    Result.y = Y;

    return Result;
}

v2
V2i(s32 X, s32 Y)
{
    v2 Result = {};

    Result.x = (r32)X;
    Result.y = (r32)Y;

    return Result;
}

v3
V3(r32 X, r32 Y, r32 Z)
{
    v3 Result = {};

    Result.x = X;
    Result.y = Y;
    Result.z = Z;

    return Result;
}

v4
V4(r32 X, r32 Y, r32 Z, r32 W)
{
    v4 Result = {};

    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    Result.w = W;

    return Result;
}

rect
RectV2(v2 Point1, v2 Point2)
{
    rect Result = {};

    Result.P1.x = Point1.x;
    Result.P1.y = Point1.y;
    Result.P2.x = Point2.x;
    Result.P2.y = Point2.y;

    return Result;
}

rect
RectR32(r32 x1, r32 y1, r32 x2, r32 y2)
{
    rect Result = {};

    Result.P1.x = x1;
    Result.P1.y = y1;
    Result.P2.x = x2;
    Result.P2.y = y2;

    return Result;
}

recti
Recti(s32 x1, s32 y1, s32 x2, s32 y2)
{
    recti Result = {};

    Result.MinX = x1;
    Result.MinY = y1;
    Result.MaxX = x2;
    Result.MaxY = y2;

    return Result;
}

/*========================================
                 Conversions
                     and
                 Intrinsics
========================================*/

u32x
ClampU32X(u32x Value, u32x Low, u32x High)
{
    u32x Result = Value;

    if(Value < Low)
    {
        Result = Low;
    }
    else if(Value > High)
    {
        Result = High;
    }

    return Result;
}

r32
DegreeToRadian(r32 Angle)
{
    r32 Result = {0};

    Result = Angle*PI32/180.0f;

    return Result;
}

r32
RadianToDegree(r32 Radian)
{
    r32 Result = {0};

    Result = Radian*180.0f/PI32;

    return Result;
}

u16
SafeTruncateToU16(s32 Value)
{
    u16 Result = 0;

    Assert(Value <= 0xFFFF &&
           Value >= 0x0000);
    Result = (u16)Value;

    return Result;
}

u32x
RoundS32xToU32x(s32x Value)
{
    u32x Result = 0;

    Assert(Value >= 0);
    Result = Value;

    return Result;
}

s32
RoundR32ToS32(r32 Value)
{
    s32 Result = 0;

    // Todo: Fix this for negative values (Currently a Ceil for negatives)
    Result = (s32)(Value + 0.5f);

    return Result;
}

s64
RoundR32ToS64(r32 Value)
{
    s64 Result = 0;

    // Todo: Fix this for negative values (Currently a Ceil for negatives)
    Result = (s64)(Value + 0.5f);

    return Result;
}

s32x
RoundR32ToS32x(r32 Value)
{
    s32x Result = 0;

    // Todo: Fix this for negative values (Currently a Ceil for negatives)
    Result = (s32x)(Value + 0.5f);

    return Result;
}

u32x
RoundR32ToU32x(r32 Value)
{
    u32x Result = 0;

    Assert(Value >= 0.0f);

    Result = (u32x)(Value + 0.5f);

    return Result;
}

local r32
RoundR32(r32 Value)
{
    r32 Result = 0;

    // Todo: Checking
    Result = (r32)((s32)Value);

    return Result;
}

r64
Floor(r64 Value)
{
    r64 Result = 0.0;

    Assert(Value >= S64_MIN && Value <= S64_MAX);
    
    if(Value < 0.0)
    {
        Result = (r64)((s64)(Value - 1.0));
    }
    else
    {
        Result = (r64)((s64)(Value));
    }

    return Result;
}

r32
Floor(r32 Value)
{
    r32 Result = 0.0f;

    Assert(Value >= S32_MIN && Value <= S32_MAX);

    if(Value < 0.0f)
    {
        Result = (r32)((s32)(Value - 1.0f));
    }
    else
    {
        Result = (r32)((s32)(Value));
    }

    return Result;
}

s32
FloorR32ToS32(r32 Value)
{
    s32 Result = 0;

    Result = (s32)Floor(Value);

    return Result;
}

r64
Ceil(r64 Value)
{
    r64 Result = 0.0;

    Assert(Value >= S64_MIN && Value <= S64_MAX);

    if(Value < 0.0)
    {
        Result = (r64)((s64)(Value));
    }
    else
    {
        Result = (r64)((s64)(Value + 1.0));
    }

    return Result;
}

r32
Ceil(r32 Value)
{
    r32 Result = 0.0f;

    Assert(Value >= S32_MIN && Value <= S32_MAX);

    if(Value < 0.0f)
    {
        Result = (r32)((s32)(Value));
    }
    else
    {
        Result = (r32)((s32)(Value + 1.0f));
    }

    return Result;
}

s32
CeilR32ToS32(r32 Value)
{
    s32 Result = 0;

    Result = (s32)Ceil(Value);

    return Result;
}

r64
Cos(r64 Angle)
{
    r64 Result = 0.0;

#if 0
    Result = cos(Angle);
#else
    Result = 0.0;
#endif

    return Result;
}

r32
Cos(r32 Angle)
{
    r32 Result = 0.0f;

#if 0
    Result = cos(Angle);
#else
    Result = 0.0f;
#endif

    return Result;
}

r64
ArcCos(r64 Angle)
{
    r64 Result = 0.0;

#if 0
    Result = acos(Angle);
#else
    Result = 0.0;
#endif

    return Result;
}

r64
Sin(r64 Angle)
{
    r64 Result = 0.0;

#if 0
    Result = sin(Angle);
#else
    Result = 0.0;
#endif

    return Result;
}

r32
Sin(r32 Angle)
{
    r32 Result = 0.0f;

#if 0
    Result = sinf(Angle);
#else
    Result = 0.0f;
#endif

    return Result;
}

s64
ClampS64(s64 Value, s64 High, s64 Low)
{
    s64 Result = Value;

    if(Value > High)
    {
        Result = High;
    }
    else if(Value < Low)
    {
        Result = Low;
    }

    return Result;
}

u64
ClampU64(u64 Value, u64 High, u64 Low)
{
    u64 Result = Value;

    if(Value > High)
    {
        Result = High;
    }
    else if(Value < Low)
    {
        Result = Low;
    }

    return Result;
}

inline b32
IsNaN(r32 Value)
{
    b32 Result = false;

    u32 CastedFloat = 0;
    CopyMemory(&CastedFloat, &Value, sizeof(r32));
    if((CastedFloat & 0x7F800000) == 0x7F800000 &&
       (CastedFloat & 0x007FFFFF) != 0x00000000)
    {
        Result = true;
    }

    return Result;
}

inline b32
IsNaN(r64 Value)
{
    b32 Result = false;

    u64 CastedFloat = 0;
    CopyMemory(&CastedFloat, &Value, sizeof(r64));
    if((CastedFloat & 0x7FF0000000000000) == 0x7FF0000000000000 &&
       (CastedFloat & 0xFFFFFFFFFFFFF) != 0x0000000000000000)
    {
        Result = true;
    }

    return Result;
}

inline b32
IsInfinite(r32 Value)
{
    b32 Result = false;

    u32 CastedFloat = 0;
    CopyMemory(&CastedFloat, &Value, sizeof(r32));
    if((CastedFloat & 0x7F800000) == 0x7F800000 &&
        (CastedFloat & 0x007FFFFF) == 0x00000000)
    {
        Result = true;
    }

    return Result;
}

inline b32
IsInfinite(r64 Value)
{
    b32 Result = false;

    u64 CastedFloat = 0;
    CopyMemory(&CastedFloat, &Value, sizeof(r64));
    if((CastedFloat & 0x7FF0000000000000) == 0x7FF0000000000000 &&
       (CastedFloat & 0xFFFFFFFFFFFFF) == 0x00000000)
    {
        Result = true;
    }

    return Result;
}

r64
SquareRoot(r64 Value)
{
    r64 Result = 0.0;

#if 0
    Result = sqrt(Value);
#else
    // Note: sqrt(x) = 10^(log10(x)/2.0)
    __m128d ValuePack = _mm_set_sd(Value);
    _mm_store_sd(&Result, _mm_sqrt_sd(ValuePack, ValuePack));
#endif

    return Result;
}

r32
SquareRoot(r32 Value)
{
    r32 Result = 0.0f;

#if 0
    Result = sqrtf(Value);
#else
    // Note: sqrt(x) = 10^(log10(x)/2.0)
    __m128 ValuePack = _mm_set_ss(Value);
    _mm_store_ss(&Result, _mm_sqrt_ss(ValuePack));
#endif

    return Result;
}

r64
Power(r64 Value, r64 Exponent)
{
    r64 Result = 0.0;

#if 0
    Result = pow(Value, Exponent);
#else
    // Note: x^y = exp(y*ln(x))
    Result = 0.0;
#endif

    return Result;
}

r32
Power(r32 Value, r32 Exponent)
{
    r32 Result = 0.0f;

#if 0
    Result = pow(Value, Exponent);
#else
    // Note: x^y = exp(y*ln(x))
    Result = 0.0f;
#endif

    return Result;
}

r64
Fmod(r64 Numerator, r64 Denominator)
{
    r64 Result = 0.0;

#if 0
    Result = fmod(Numerator, Denominator);
#else
    Result = 0.0;
#endif

    return Result;
}

r32
Fmod(r32 Numerator, r32 Denominator)
{
    r32 Result = 0.0f;

#if 0
    Result = fmod(Numerator, Denominator);
#else
    Result = 0.0f;
#endif

    return Result;
}

/*========================================

           C String/Char Operations

========================================*/

inline s32x
AsciiToInteger(char *String)
{
    s32x Result = 0;
    
    b32 MakeNegative = false;

    for(char *Source = String;
        *Source != 0;
        ++Source)
    {
        if(*Source == '-')
        {
            MakeNegative = !MakeNegative;
        }
        else if(*Source >= '0' && *Source <= '9')
        {
            Result *= 10;
            Result += (*Source - '0');
        }
        else if(*Source >= 'a' && *Source <= 'f')
        {
            Result *= 10;
            Result += 10 + (*Source - 'a');
        }
        else if(*Source >= 'A' && *Source <= 'F')
        {
            Result *= 10;
            Result += 10 + (*Source - 'A');
        }
    }

    if(MakeNegative)
    {
        Result *= -1;
    }

    return Result;
}

inline s32x
AsciiToInteger(u8 *String)
{
    s32x Result = 0;

    b32 MakeNegative = false;

    for(u8 *Source = String;
        *Source != 0;
        ++Source)
    {
        if(*Source == '-')
        {
            MakeNegative = !MakeNegative;
        }
        else if(*Source >= '0' && *Source <= '9')
        {
            Result *= 10;
            Result += (*Source - '0');
        }
        else if(*Source >= 'a' && *Source <= 'f')
        {
            Result *= 10;
            Result += 10 + (*Source - 'a');
        }
        else if(*Source >= 'A' && *Source <= 'F')
        {
            Result *= 10;
            Result += 10 + (*Source - 'A');
        }
    }

    if(MakeNegative)
    {
        Result *= -1;
    }

    return Result;
}

r32
// Todo: Make an AsciiToReal32Precise?
AsciiToReal32(char *String)
{
    // Note: This is actually pretty good
    // Todo: Hex Floats

    // Note: We could if we wanted more precision for large Integer or large Decimal values to use r64,
    //       or use an actual Integer for integer accumulation for numbers like: 123456700000.0.
    //       For now it will be r32 for performance reasons
    r32 Result = 0.0f;
    r32 Decimal = 0.0f;
    r32 Exponent = 1;

    b32 MakeNegative = false;
    b32 DecimalMode = false;

    for(char *Source = String;
        *Source != 0;
        ++Source)
    {
        if(*Source == '-')
        {
            MakeNegative = !MakeNegative;
        }
        else if(*Source >= '0' && *Source <= '9')
        {
            if(DecimalMode)
            {
                Exponent *= 10;
                Decimal *= 10;
                Decimal += (*Source - '0');
            }
            else
            {
                Result *= 10;
                Result += (*Source - '0');
            }
        }
        else if(*Source == '.')
        {
            DecimalMode = true;
        }
    }

    if(MakeNegative)
    {
        Result *= -1;
    }

    Decimal /= Exponent;
    Result += Decimal;

    return Result;
}

inline b32
IsWhitespace(char Character)
{
    b32 Result = false;

    switch(Character)
    {
        case ' ':
        case '\t':
        case '\n':
        case '\r':
        {
            Result = true;
        } break;
    }

    return Result;
}

void
ToLowerString(char *Dest, char *Source)
{
    char *DestScanner = Dest;
    char *SourceScanner = Source;
    for(;
        *SourceScanner;
        )
    {
        *DestScanner++ = *SourceScanner++ | ' ';
    }
    *DestScanner = 0;
}

void
ToLowerString(u8 *Dest, char *Source)
{
    u8 *DestScanner = Dest;
    u8 *SourceScanner = (u8 *)Source;
    for(;
        *SourceScanner;
        )
    {
        *DestScanner++ = *SourceScanner++ | ' ';
    }
    *DestScanner = 0;
}

void
ToLowerString(u8 *Dest, u8 *Source)
{
    u8 *DestScanner = Dest;
    u8 *SourceScanner = Source;
    for(;
        *SourceScanner;
        )
    {
        *DestScanner++ = *SourceScanner++ | ' ';
    }
    *DestScanner = 0;
}

char
ToLowerChar(char Input)
{
    char Result = 'Z';

    Result = Input | ' ';

    return Result;
}

void
ToUpperString(char *Dest, char *Source)
{
    char *DestScanner = Dest;
    char *SourceScanner = Source;
    for(;
        SourceScanner != '\0';
        )
    {
        *DestScanner++ = *SourceScanner++ & '_';
    }
    *DestScanner = '\0';
}

char
ToUpperChar(char Character)
{
    char Result = 'Z';

    Result = Character & '_';

    return Result;
}

void
InvertCaseString(char *Dest, char *Source)
{
    char *DestScanner = Dest;
    char *SourceScanner = Source;
    for(;
        SourceScanner != '\0';
        )
    {
        *DestScanner++ = *SourceScanner++ ^ ' ';
    }
    *DestScanner = 0;
}

char
InvertCaseChar(char Input)
{
    char Result = 'Z';

    Result = Input ^ ' ';

    return Result;
}

int
GematriaNumber(char Letter)
{
    int Result = 0;

    Result = (Letter | ' ') & '?';

    return Result;
}

u32
StringLength(char *String)
{
    u32 Result = 0;
    if(String)
    {
        for(char *At = String;
            *At != 0;
            ++At, ++Result)
        {
        }
    }

    return Result;
}

u32
StringLength(u8 *String)
{
    u32 Result = 0;
    if(String)
    {
        for(u8 *At = String;
            *At != 0;
            ++At, ++Result)
        {
        }
    }

    return Result;
}

b32
CompareString(char *A, char *B)
{
    // Note:
    // With lengths checking:
    // Different Lengths: O(n + m)
    // Same Lengths:      O(2n + n)
    //
    // Without length checking:
    // Different Strings: O(n + d) | d is the first character index that is different 
    // Same Strings:      O(n)
    //
    // Conclusion: Not worth the length checking

    b32 Result = true;
    
    if(A && B)
    {
        umm At = 0;
        do
        {
            if(*(A + At) != *(B + At))
            {
                Result = false;
                break;
            }
        }
        while(*(A + At) == *(B + At) && 
              *(A + At) != 0 && *(B + At) != 0 && 
              ++At);
        // Note: The && ++At is so that we do the check before the next iteration, not really attempting to do any comparison
    }
    else
    {
        Result = false;
    }

    return Result;
}

b32
CompareString(u8 *A, char *B)
{
    b32 Result = true;

    if(A && B)
    {
        umm At = 0;
        do
        {
            if(*(A + At) != *(B + At))
            {
                Result = false;
                break;
            }
        }
        while(*(A + At) == *(B + At) &&
              *(A + At) != 0 && *(B + At) != 0 &&
              ++At);
    }
    else
    {
        Result = false;
    }

    return Result;
}

b32
CompareString(u8 *A, u8 *B)
{
    b32 Result = true;

    if(A && B)
    {
        umm At = 0;
        do
        {
            if(*(A + At) != *(B + At))
            {
                Result = false;
                break;
            }
        }
        while(*(A + At) == *(B + At) &&
            *(A + At) != 0 && *(B + At) != 0 &&
            ++At);
        // Note: The && ++At is so that we do the check before the next iteration, not really attempting to do any comparison
    }
    else
    {
        Result = false;
    }

    return Result;
}

b32
CompareStringLength(char *A, char *B, umm Length)
{
    b32 Result = true;

    if(A && B)
    {
        for(umm At = 0;
            At < Length;
            ++At)
        {
            if(*(A + At) != *(B + At))
            {
                Result = false;
                break;
            }
        }
    }
    else
    {
        Result = false;
    }

    return Result;
}

b32
CompareStringLength(u8 *A, u8 *B, umm Length)
{
    b32 Result = true;

    if(A && B)
    {
        for(umm At = 0;
            At < Length;
            ++At)
        {
            if(*(A + At) != *(B + At))
            {
                Result = false;
                break;
            }
        }
    }
    else
    {
        Result = false;
    }

    return Result;
}

void
StringCopy(char *Dest, char *Source)
{
#if 1
    umm Length = StringLength(Source);
    umm LengthToLastByte = (Length & ~7);
    __movsq((u64 *)Dest, (u64 *)(Source), Length/8);
    __movsb(((u8 *)Dest + LengthToLastByte), (u8 *)(Source + LengthToLastByte), Length & 7);

    Dest += Length;
#else
    for(char *At = Source;
        *At != 0;
        )
    {
        *Dest++ = *At++;
    }
#endif
    *Dest = 0;
}

void
StringCopy(u8 *Dest, char *Source)
{
#if 1
    umm Length = StringLength(Source);
    umm LengthToLastByte = (Length & ~7);
    __movsq((u64 *)Dest, (u64 *)(Source), Length/8);
    __movsb((Dest + LengthToLastByte), (u8 *)(Source + LengthToLastByte), Length & 7);

    Dest += Length;
#else
    for(char *At = Source;
        *At != 0;
        )
    {
        *Dest++ = *At++;
    }
#endif
    *Dest = 0;
}

void
StringCopy(u8 *Dest, u8 *Source)
{
#if 1
    umm Length = StringLength(Source);
    umm LengthToLastByte = (Length & ~7);
    __movsq((u64 *)Dest, (u64 *)(Source), Length/8);
    __movsb((Dest + LengthToLastByte), Source + LengthToLastByte, Length & 7);

    Dest += Length;
#else
    for(char *At = Source;
        *At != 0;
        )
    {
        *Dest++ = *At++;
    }
#endif
    *Dest = 0;
}

void
StringCopyLength(char *Dest, char *Source, umm Length)
{
#if 0 // Note: This is probably the recommended method, however it possibly has a startup cost
    umm LengthToLastByte = (Length & ~7);
    __movsq((u64 *)Dest, (u64 *)(Source), Length/8);
    __movsb(((u8 *)Dest + LengthToLastByte), (u8 *)(Source + LengthToLastByte), Length & 7);

    Dest += Length;

#else // Note: Simple SIMD implementation, could be faster with if added the inbetween cases of 15 - 2
    __m128i Atx16;
    for(char *At = Source;
        At < Source + Length;
        )
    {
        if(Source + Length - At >= 16)
        {
            Atx16 = _mm_lddqu_si128((const __m128i *)At);
            _mm_store_si128((__m128i *)Dest, Atx16);
            At += 16;
            Dest += 16;
        }
        else
        {
            *Dest++ = *At++;
        }
    }
#endif
    *Dest = 0;
}

void
StringCopyLength(u8 *Dest, u8 *Source, umm Length)
{
#if 0 // Note: This is probably the recommended method, however it possibly has a startup cost
    umm LengthToLastByte = (Length & ~7);
    __movsq((u64 *)Dest, (u64 *)(Source), Length/8);
    __movsb(((u8 *)Dest + LengthToLastByte), (u8 *)(Source + LengthToLastByte), Length & 7);

    Dest += Length;

#else // Note: Simple SIMD implementation, could be faster with if added the inbetween cases of 15 - 2
    __m128i Atx16;
    for(u8 *At = Source;
        At < Source + Length;
        )
    {
        if(Source + Length - At >= 16)
        {
            Atx16 = _mm_lddqu_si128((const __m128i *)At);
            _mm_store_si128((__m128i *)Dest, Atx16);
            At += 16;
            Dest += 16;
        }
        else
        {
            *Dest++ = *At++;
        }
    }
#endif
    *Dest = 0;
}

//
// Note: Start to End inclusive. Start and End are offsets of the Source string
// This function is for substrings from a known start to end.
// For a function for searching a string for a string pattern see: SearchString
//
void
SubString(char *Dest, char *Source, umm Start, umm End)
{
    // Note/Todo: The SIMD method may tend to be faster on some machines than the __movsx set of intrinsics
    // It is worth coming back and checking cycle counts on various lengths of input strings

#if 0 // Note: This is probably the recommended method, however it's known to possibly have a startup cost
    u32x Length = End - Start;
    u32x LengthToLastByte = (Length & ~7);
    __movsq((u64 *)Dest, (u64 *)(Source + Start), Length/8);
    __movsb(((u8 *)Dest + LengthToLastByte), (u8 *)(Source + Start + LengthToLastByte), Length & 7);

    Dest += Length;

#else // Note: Simple SIMD implementation, could be faster with the inbetween cases of 15 - 2
    __m128i Atx16;
    for(char *At = Source + Start;
        At < Source + End;
        )
    {
        if(Source + End - At >= 16)
        {
            Atx16 = _mm_lddqu_si128((const __m128i *)At);
            _mm_store_si128 ((__m128i *)Dest, Atx16);
            At += 16;
            Dest += 16;
        }
        else
        {
            *Dest++ = *At++;
        }
    }
#endif

    *Dest = 0;
}
void
SubString(u8 *Dest, u8 *Source, umm Start, umm End)
{
    // Note/Todo: The SIMD method may tend to be faster on some machines than the __movsx set of intrinsics
    // It is worth coming back and checking cycle counts on various lengths of input strings

#if 0 // Note: This is probably the recommended method, however it's known to possibly have a startup cost
    u32x Length = End - Start;
    u32x LengthToLastByte = (Length & ~7);
    __movsq((u64 *)Dest, (u64 *)(Source + Start), Length/8);
    __movsb(((u8 *)Dest + LengthToLastByte), (u8 *)(Source + Start + LengthToLastByte), Length & 7);

    Dest += Length;

#else // Note: Simple SIMD implementation, could be faster with the inbetween cases of 15 - 2
    __m128i Atx16;
    for(u8 *At = Source + Start;
        At < Source + End;
        )
    {
        if(Source + End - At >= 16)
        {
            Atx16 = _mm_lddqu_si128((const __m128i *)At);
            _mm_store_si128 ((__m128i *)Dest, Atx16);
            At += 16;
            Dest += 16;
        }
        else
        {
            *Dest++ = *At++;
        }
    }
#endif

    *Dest = 0;
}

void
StringCat(char *Dest, char *Source)
{ // Note: Assumes Dest has enough room for both.
    char *DestAt;

    for(DestAt = Dest;
        *DestAt != '\0';
        ++DestAt)
    {}

    for(char *SourceAt = Source;
        *SourceAt != '\0';
        )
    {
        *DestAt++ = *SourceAt++;
    }
    *DestAt = '\0';
}

void
StringCat(u8 *Dest, char *Source)
{ // Note: Assumes Dest has enough room for both.
    u8 *DestAt;

    for(DestAt = Dest;
        *DestAt != '\0';
        ++DestAt)
    {
    }

    for(u8 *SourceAt = (u8 *)Source;
        *SourceAt != '\0';
        )
    {
        *DestAt++ = *SourceAt++;
    }
    *DestAt = '\0';
}

void
StringCat(u8 *Dest, u8 *Source)
{ // Note: Assumes Dest has enough room for both.
    u8 *DestAt;

    for(DestAt = Dest;
        *DestAt != '\0';
        ++DestAt)
    {
    }

    for(u8 *SourceAt = Source;
        *SourceAt != '\0';
        )
    {
        *DestAt++ = *SourceAt++;
    }
    *DestAt = '\0';
}

void
StringNCharCopy(char *Dest, char Char, umm N)
{
    for(char *At = Dest;
        At < Dest + N;
        ++At)
    {
        *At = Char;
    }
}

void
StringNCharCopy(u8 *Dest, u8 Char, umm N)
{
    for(u8 *At = Dest;
        At < Dest + N;
        ++At)
    {
        *At = Char;
    }
}

char *
SearchString(char *Source, char *Pattern)
{ // Todo: SIMDizable?
    char *Result = 0;

    umm PatternLength = StringLength(Pattern);

    for(char *AtSrc = Source;
        *AtSrc != '\0';
        ++AtSrc)
    {
        b32x NotFound = false;
        for(umm Index = 0;
            Index < PatternLength;
            ++Index)
        {
            if(AtSrc[Index] != Pattern[Index])
            {
                NotFound = true;
                break;
            }
        }

        if(!NotFound)
        { // Note: If not not found, it must be found!
            Result = AtSrc;
            break;
        }
    }

    return Result;
}

s32
S32FromASCII(char *At)
{
    s32 Result = 0;

    while(*At >= '0' && *At <= '9')
    {
        Result *= 10;
        Result += (*At - '0');
        ++At;
    }

    return Result;
}

// Note: This will advance the Input pointer past the numerical values
s32
S32FromASCIIAdvance(char **At)
{
    s32 Result = 0;

    char *Source = *At;

    while(*Source >= '0' && *Source <= '9')
    {
        Result *= 10;
        Result += (*Source - '0');
        ++Source;
    }
    *At = Source;

    return Result;
}

// Note: Doesn't put a null terminator on the end of the string
void
ASCIIFromU64(char *Dest, u64 Value, u32 Base, char *Digits)
{
    Assert(Base != 0);

    char *Start = Dest;
    do
    {
        *Dest++ = Digits[Value%Base];
        Value /= Base;
    }
    while(Value == 0);
    char *End = Dest;
    while(Start < End)
    {
        --End;
        char Temp = *End;
        *End = *Start;
        *Start = Temp;
        ++Start;
    }
}

typedef struct format_dest
{
    umm Size;
    char *At;
} format_dest;

inline void
InsertChar(format_dest *Dest, char Value)
{
    if(Dest->Size)
    {
        --Dest->Size;
        *Dest->At++ = Value;
    }
}

void
ASCIIFromU64(format_dest *Dest, u64 Value, u32 Base, char *Digits)
{
    Assert(Base != 0);

    char *Start = Dest->At;
    do
    {
        InsertChar(Dest, *(Digits + (Value%Base)));
        Value /= Base;
    } while(Value != 0);
    char *End = Dest->At;
    while(Start < End)
    {
        --End;
        char Temp = *End;
        *End = *Start;
        *Start = Temp;
        ++Start;
    }
}

char DecChars[] =      "0123456789";
char LowerHexChars[] = "0123456789abcdef";
char UpperHexChars[] = "0123456789ABCDEF";
void
ASCIIFromF64(char *Dest, r64 Value)
{

}

void
ASCIIFromF64(format_dest *Dest, r64 Value, u32 Precision)
{
    if(Value < 0)
    {
        InsertChar(Dest, '-');
        Value = -Value;
    }
    
    if(IsNaN(Value))
    {
        InsertChar(Dest, 'n');
        InsertChar(Dest, 'a');
        InsertChar(Dest, 'n');
    }
    else if(IsInfinite(Value))
    {
        InsertChar(Dest, 'i');
        InsertChar(Dest, 'n');
        InsertChar(Dest, 'f');
    }
    else
    {
        u64 IntegerPart = (u64)Value;
        Value -= (r64)IntegerPart;
        ASCIIFromU64(Dest, IntegerPart, 10, DecChars);

        InsertChar(Dest, '.');

        for(u32 Index = 0;
            Index < Precision;
            ++Index)
        {
            Value *= 10;
            IntegerPart = (u64)Value;
            Value -= (r64)IntegerPart;
            InsertChar(Dest, *(DecChars + IntegerPart));
        }
    }
}

inline void
InsertChars(format_dest *Dest, char *Value)
{
    while(*Value)
    {
        InsertChar(Dest, *Value++);
    }
}

inline u64
ReadVarArgUnsignedInteger(u32 Length, va_list *ArgList)
{
    u64 Result = 0;

    switch(Length)
    {
        case 1:
        {
            Result = va_arg(*ArgList, u8);
        } break;
        case 2:
        {
            Result = va_arg(*ArgList, u16);
        } break;
        case 4:
        {
            Result = va_arg(*ArgList, u32);
        } break;
        case 8:
        {
            Result = va_arg(*ArgList, u64);
        } break;
    }

    return Result;
}

inline s64
ReadVarArgSignedInteger(u32 Length, va_list *ArgList)
{
    s64 Result = 0;

    switch(Length)
    {
        case 1:
        {
            Result = va_arg(*ArgList, s8);
        } break;
        case 2:
        {
            Result = va_arg(*ArgList, s16);
        } break;
        case 4:
        {
            Result = va_arg(*ArgList, s32);
        } break;
        case 8:
        {
            Result = va_arg(*ArgList, s64);
        } break;
    }

    return Result;
}

inline r64
ReadVarArgFloat(u32 Length, va_list *ArgList)
{
    r64 Result = 0;

    switch(Length)
    {
        case 4:
        {
            Result = va_arg(*ArgList, r32);
        } break;
        case 8:
        {
            Result = va_arg(*ArgList, r64);
        } break;
    }

    return Result;
}

local umm
FormatStringList(umm DestSize, char *DestInit, char *Format, va_list ArgList)
{
    format_dest Dest = {DestSize, DestInit};

    if(Dest.Size)
    {
        char *At = Format;
        while(*At)
        {
            if(*At == '%')
            {
                ++At;

                b32 ForceSign = false;
                b32 PadWithZeros = false;
                b32 LeftJustified = false;
                b32 PositiveSignIsBlank = false;
                b32 AnnotateIfNotZero = false;

                b32 Parsing = true;
                
                // Note: Flags
                while(Parsing)
                {
                    switch(*At)
                    {
                        case '+': { ForceSign = true; }break;
                        case '0': { PadWithZeros = true; } break;
                        case '-': { LeftJustified = true; } break;
                        case ' ': { PositiveSignIsBlank = true; } break;
                        case '#': { AnnotateIfNotZero = true; } break;
                        default: { Parsing = false; } break;
                    }

                    if(Parsing)
                    {
                        ++At;
                    }
                }

                // Note: Widths
                b32 WidthSpecified = false;
                u32 Width = 0;
                if(*At == '*')
                {
                    ++At;
                    Width = va_arg(ArgList, int);
                    WidthSpecified = true;
                }
                else if(*At >= '0' && *At <= '9')
                {
                    Width = S32FromASCIIAdvance(&At);
                    WidthSpecified = true;
                }

                // Note: Precision
                b32 PrecisionSpecified = false;
                s32 Precision = 6;
                if(*At == '.')
                {
                    ++At;
                    if(*At == '*')
                    {
                        ++At;
                        Precision = va_arg(ArgList, int);
                        PrecisionSpecified = true;
                    }
                    else if(*At >= '0' && *At <= '9')
                    {
                        Precision = S32FromASCIIAdvance(&At);
                        PrecisionSpecified = true;
                    }
                    else
                    {
                        Assert(!"Malformed precision specifier!");
                    }
                }

                // Note: Length
                u32 IntegerLength = 4; // Note: In Bytes
                u32 FloatLength = 8; // Note: In Bytes

                if(*At == 'h' && *(At + 1) == 'h')
                {
                    IntegerLength = 1;
                    At += 2;
                }
                else if(*At == 'l' && *(At + 1) == 'l')
                {
                    IntegerLength = 8;
                    At += 2;
                }
                else if(*At == 'h')
                {
                    IntegerLength = 2;
                    ++At;
                }
                else if(*At == 'l')
                {
                    ++At;
                }
                else if(*At == 'j')
                {
                    ++At;
                }
                else if(*At == 'z')
                {
                    ++At;
                }
                else if(*At == 't')
                {
                    ++At;
                }
                else if(*At == 'L')
                {
                    ++At;
                }


                // Note: Specifier
                char TempBuffer[128];
                char *Temp = TempBuffer;
                format_dest TempDest = {ArrayCount(TempBuffer), Temp};
                char *Prefix = "";
                b32 IsFloat = false;

                switch(*At)
                {
                    case 'd':
                    case 'i':
                    {
                        s64 Value = ReadVarArgSignedInteger(IntegerLength, &ArgList);
                        b32 IsNegative = (Value < 0);

                        if(IsNegative)
                        {
                            Value = -Value;
                        }
                        ASCIIFromU64(&TempDest, (u64)Value, 10, DecChars);
                        if(IsNegative)
                        {
                            Prefix = "-";
                        }
                        else if(ForceSign)
                        {
                            Assert(!PositiveSignIsBlank); // Note: Conflicting things?
                            Prefix = "+";
                        }
                        else if(PositiveSignIsBlank)
                        {
                            Prefix = " ";
                        }
                    } break;

                    case 'u':
                    {
                        u64 Value = ReadVarArgUnsignedInteger(IntegerLength, &ArgList);
                        ASCIIFromU64(&TempDest, (u64)Value, 10, DecChars);
                    } break;

                    case 'o':
                    {
                        u64 Value = ReadVarArgUnsignedInteger(IntegerLength, &ArgList);
                        ASCIIFromU64(&TempDest, (u64)Value, 8, DecChars);

                        if(AnnotateIfNotZero && Value != 0)
                        {
                            Prefix = "0";
                        }
                    } break;

                    case 'x':
                    {
                        u64 Value = ReadVarArgUnsignedInteger(IntegerLength, &ArgList);
                        ASCIIFromU64(&TempDest, (u64)Value, 16, LowerHexChars);

                        if(AnnotateIfNotZero && Value != 0)
                        {
                            Prefix = "0x";
                        }
                    } break;

                    case 'X':
                    {
                        u64 Value = ReadVarArgUnsignedInteger(IntegerLength, &ArgList);
                        ASCIIFromU64(&TempDest, (u64)Value, 16, UpperHexChars);

                        if(AnnotateIfNotZero && Value != 0)
                        {
                            Prefix = "0X";
                        }
                    } break;

                    case 'f':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'F':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'e':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'E':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'g':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'G':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'a':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'A':
                    {
                        r64 Value = ReadVarArgFloat(FloatLength, &ArgList);
                        ASCIIFromF64(&TempDest, Value, Precision);
                        IsFloat = true;
                    } break;

                    case 'c':
                    {
                        char Value = (char)va_arg(ArgList, int);
                        InsertChar(&Dest, Value);
                    } break;

                    case 's':
                    { // Todo: Precision and Width handling
                        char *String = va_arg(ArgList, char *);

                        Temp = String;
                        if(PrecisionSpecified)
                        {
                            for(char *Scan = String;
                                *Scan && TempDest.Size < Precision;
                                ++Scan)
                            {
                                ++TempDest.Size;
                            }
                        }
                        else
                        {
                            TempDest.Size = StringLength(String);
                        }
                        TempDest.At = String + TempDest.Size;
                    } break;

                    case 'p':
                    {
                        void *Value = va_arg(ArgList, void *);
                        ASCIIFromU64(&TempDest, (umm)Value, 16, LowerHexChars);
                    } break;

                    case 'n':
                    {
                        int *TabDest = va_arg(ArgList, int *);
                        *TabDest = (int)(Dest.At - DestInit);
                    } break;

                    case '%':
                    {
                        InsertChar(&Dest, '%');
                    } break;

                    default: { Assert(!"Invalid Format Specifier"); } break;
                }
                
                if(TempDest.At - Temp)
                {
                    smm UsePrecision = Precision;
                    if(IsFloat || !PrecisionSpecified)
                    {
                        UsePrecision = TempDest.At - Temp;
                    }

                    smm PrefixLength = StringLength(Prefix);
                    smm UseWidth = Width;
                    smm ComputedWidth = UsePrecision + PrefixLength;
                    if(UseWidth < ComputedWidth)
                    {
                        UseWidth = ComputedWidth;
                    }

                    if(PadWithZeros)
                    {
                        Assert(!LeftJustified); // Note: ?
                        LeftJustified = false;
                    }

                    if(!LeftJustified)
                    {
                        while(UseWidth > UsePrecision + PrefixLength)
                        {
                            InsertChar(&Dest, PadWithZeros ? '0' : ' ');
                            --UseWidth;
                        }
                    }

                    for(char *Pre = Prefix;
                        *Pre && UseWidth;
                        ++Pre)
                    {
                        InsertChar(&Dest, *Pre);
                        --UseWidth;
                    }

                    if(UsePrecision > UseWidth)
                    {
                        UsePrecision = UseWidth;
                    }
                    while(UsePrecision > (TempDest.At - Temp))
                    {
                        InsertChar(&Dest, '0');
                        --UsePrecision;
                        --UseWidth;
                    }
                    while(UsePrecision && TempDest.At != Temp)
                    {
                        InsertChar(&Dest, *Temp++);
                        --UsePrecision;
                        --UseWidth;
                    }
                    if(LeftJustified)
                    {
                        while(UseWidth)
                        {
                            InsertChar(&Dest, ' ');
                            --UseWidth;
                        }
                    }
                }

                if(*At)
                {
                    ++At;
                }
            }
            else
            {
                InsertChar(&Dest, *At++);
            }
        }

        if(Dest.Size)
        {
            *Dest.At = 0;
        }
        else
        {
            Dest.At[-1] = 0;
        }
    }

    umm Result = Dest.At - DestInit;
    return Result;
}

local inline umm
FormatString(umm DestSize, char *Dest, char *Format, ...)
{
    umm Result = 0;
    va_list ArgList;

    va_start(ArgList, Format);
    Result = FormatStringList(DestSize, Dest, Format, ArgList);
    va_end(ArgList);

    return Result;
}

local inline umm
FormatString(umm DestSize, u8 *Dest, char *Format, ...)
{
    umm Result = 0;
    va_list ArgList;

    va_start(ArgList, Format);
    Result = FormatStringList(DestSize, (char *)Dest, Format, ArgList);
    va_end(ArgList);

    return Result;
}


/*========================================

             UTF16 Operations

========================================*/

inline void
UTF16ToCharString(char *Dest, char *Source)
{
    // Todo: We need a datatype for UTF16?
    for(char *At = Source;
        *At;
        At += 2)
    {
        *Dest++ = *At;
    }
    *Dest = 0;
}

/*========================================

             Memory Operations

========================================*/

void *
SetMemory(void *Dest, int Value, umm Size)
{
    void *Result = Dest;

    u8 CastedValue = *(u8 *)&Value;
    u8 *At = (u8 *)Dest;
    while(Size--)
    {
        *At++ = CastedValue;
    }

    return Result;
}

void *
CopyMemory(void *Dest, void *Source, umm Size)
{
    void *Result = Dest;

    u8 *DestAt = (u8 *)Dest;
    u8 *SourceAt = (u8 *)Source;

    while(Size--)
    {
        *DestAt++ = *SourceAt++;
    }

    return Result;
}

void *
// Note: For overlapping memory
MoveMemory(void *Dest, void *Source, umm Size)
{
    void *Result = Dest;

    u8 *Buffer = (u8 *)Allocate(Size);
    CopyMemory((void *)Buffer, Source, Size);

    u8 *DestAt = (u8 *)Dest;

    while(Size--)
    {
        *DestAt++ = *Buffer++;
    }

    Deallocate(Buffer);

    return Result;
}

typedef struct mandala
{
    u8 *Base;
    umm Size;
    umm Used;
    b32x Expands;
} mandala;

void *
Allocate(umm Size)
{
    void *Result = 0;

#if _WIN32
    Result = VirtualAlloc(0, Size, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
#elif _linux_
    // Todo: Look up mmap
    Result = mmap();
#else
    // Note: I guess our last option is something from a standard library
    Result = malloc(Size);
#endif
    return Result;
}

void *
Reallocate(void *Memory, umm OldSize, umm NewSize)
{
    void *Result = 0;

#if _WIN32
    Result = VirtualAlloc(0, NewSize, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
    if(Memory)
    {
        if(Result) CopyMemory(Result, Memory, Min(OldSize, NewSize));
        VirtualFree(Memory, 0, MEM_RELEASE);
    }
#elif _linux_
    // Todo: linux's version of Reallocate?
    // Todo: Look up mmap stuff
    Result = mmap();
#else
    // Note: I guess our last option is something from a standard library
    Result = malloc(NewSize);

    if(Memory)
    {
        CopyMemory(Result, Memory, Min(OldSize, NewSize));
        free(Memory);
    }
#endif

    return Result;
}

void
Deallocate(void *Memory)
{
#if _WIN32 // Note: Does this _actually_ mean that I'm compiling on Windows?
    int VirtualFreeReturn = VirtualFree(Memory, 0, MEM_RELEASE);
#elif _linux_
    // Todo: linux's version of Deallocate?

#else
    // Note: I guess out last option is something from a standard library
    free(Memory);
#endif
}

mandala
InitMandala(void *Memory, umm Size)
{
    mandala Result = {};

    Result.Base = (u8 *)Memory;
    Result.Size = Size;
    Result.Used = 0;

    return Result;
}

void
ExpandMandala(mandala *Mandala)
{
    umm NewMemorySize = Mandala->Size/2 + Mandala->Size;
    Mandala->Base = (u8 *)Reallocate((void *)Mandala->Base, Mandala->Size, NewMemorySize);
    Mandala->Size = NewMemorySize;
}

#define PushStruct(Mandala, type) PushSize_(Mandala, sizeof(type))
#define PushSize(Mandala, Size) PushSize_(Mandala, Size)
#define PushArray(Mandala, type, Count) PushSize_(Mandala, sizeof(type)*Count)

void *
PushSize_(mandala *Mandala, umm Size)
{
    void *Result = 0;
    if(Size + Mandala->Used > Mandala->Size)
    {
        InvalidCodePath;
    }

    Result = Mandala->Base + Mandala->Used;
    Mandala->Used += Size;

    return Result;
}

void *
PushString(mandala *Mandala, char *String)
{
    void *Result = 0;
    umm StringSize = StringLength(String) + 1;
    if(StringSize + Mandala->Used > Mandala->Size)
    {
        InvalidCodePath;
    }

    Result = Mandala->Base + Mandala->Used;
    Mandala->Used += StringSize;

    return Result;
}

mandala
InitSubMandala(mandala *FromMandala, umm Size)
{
    mandala Result ={};

    Result.Base = (u8 *)PushSize(FromMandala, Size);
    Result.Used = 0;
    Result.Size = Size;

    return Result;
}

/*========================================

                V2 Operations

========================================*/

r32
Inner(v2 Vector1, v2 Vector2)
{
    r32 Result = 0;

    Result = Vector1.x*Vector2.x + Vector1.y*Vector2.y;

    return Result;
}

r32
LengthSquared(v2 Vector)
{
    r32 Result = 0.0f;

    Result = Inner(Vector, Vector);

    return Result;
}

v2
operator+(v2 V1, v2 V2)
{
    v2 Result = {};

    Result.x = V1.x + V2.x;
    Result.y = V1.y + V2.y;

    return Result;
}

v2
operator+(v2 V1, r32 Scalar)
{
    v2 Result = {};

    Result.x = V1.x + Scalar;
    Result.y = V1.y + Scalar;

    return Result;
}

v2
operator-(v2 V1, r32 Scalar)
{
    v2 Result = {};

    Result.x = V1.x - Scalar;
    Result.y = V1.y - Scalar;

    return Result;
}

v2
operator-(r32 Scalar, v2 V1)
{
    v2 Result = {};

    Result.x = Scalar - V1.x;
    Result.y = Scalar - V1.y;

    return Result;
}

v2
operator-(v2 V1, v2 V2)
{
    v2 Result = {};

    Result.x = V1.x - V2.x;
    Result.y = V1.y - V2.y;

    return Result;
}

v2
operator*(v2 Vector, r32 Scalar)
{
    v2 Result = {};

    Result.x = Vector.x*Scalar;
    Result.y = Vector.y*Scalar;

    return Result;
}

v2
operator/(v2 Vector, r32 Scalar)
{
    v2 Result = {};

    Result.x = Vector.x/Scalar;
    Result.y = Vector.y/Scalar;

    return Result;
}

v2
operator/(v2 A, v2 B)
{
    v2 Result = {};

    Result.x = A.x/B.x;
    Result.y = A.y/B.y;

    return Result;
}

v2
operator-(v2 V1)
{
    v2 Result = {};

    Result.x = V1.x*-1;
    Result.y = V1.y*-1;

    return Result;
}

v2
Perp(v2 Vector)
{
    v2 Result = {-Vector.y, Vector.x};

    return Result;
}

v2
Lerp(v2 A, v2 B, r32 t)
{
    v2 Result = {};

    Result = A*(1.0f - t) + B*t;

    return Result;
}

v2
Hadamard(v2 A, v2 B)
{
    v2 Result = {};

    Result.x = A.x*B.x;
    Result.y = A.y*B.y;

    return Result;
}


/*========================================

              V3 Operations

========================================*/

r32
Inner(v3 Vector1, v3 Vector2)
{
    r32 Result = 0;

    Result = Vector1.x*Vector2.x + Vector1.y*Vector2.y + Vector1.z*Vector2.z;

    return Result;
}

v3
Hadamard(v3 A, v3 B)
{
    v3 Result = {};

    Result.x = A.x*B.x;
    Result.y = A.y*B.y;
    Result.z = A.z*B.z;

    return Result;
}

r32
LengthSquared(v3 Vector)
{
    r32 Result = 0.0f;

    Result = Inner(Vector, Vector);

    return Result;
}

r32
Length(v3 Vector)
{
    r32 Result = 0;

    Result = SquareRoot(LengthSquared(Vector));

    return Result;
}

v3
operator+(v3 V1, r32 Scalar)
{
    v3 Result = {};

    Result.x = V1.x + Scalar;
    Result.y = V1.y + Scalar;
    Result.z = V1.z + Scalar;

    return Result;
}

v3
operator-(v3 V1, r32 Scalar)
{
    v3 Result = {};

    Result.x = V1.x - Scalar;
    Result.y = V1.y - Scalar;
    Result.z = V1.z - Scalar;

    return Result;
}

v3
operator-(r32 Scalar, v3 V1)
{
    v3 Result = {};

    Result.x = Scalar - V1.x;
    Result.y = Scalar - V1.y;
    Result.z = Scalar - V1.z;

    return Result;
}

v3
operator+(v3 V1, v3 V2)
{
    v3 Result = {};

    Result.x = V1.x + V2.x;
    Result.y = V1.y + V2.y;
    Result.z = V1.z + V2.z;

    return Result;
}

v3
operator*(v3 Vector, r32 Scalar)
{
    v3 Result = {};

    Result.x = Vector.x*Scalar;
    Result.y = Vector.y*Scalar;
    Result.z = Vector.z*Scalar;

    return Result;
}

v3
operator/(v3 Vector, r32 Scalar)
{
    v3 Result = {};

    Result.x = Vector.x/Scalar;
    Result.y = Vector.y/Scalar;
    Result.z = Vector.z/Scalar;

    return Result;
}

v3
operator-(v3 V1)
{
    v3 Result = {};

    Result.x = V1.x*-1;
    Result.y = V1.y*-1;
    Result.z = V1.z*-1;

    return Result;
}

v3
NormalizeV3(v3 Vector)
{
    v3 Result = {};

    r32 IVectorLength = 1.0f/Length(Vector);
    Result = Vector*IVectorLength;

    return Result;
}

/*========================================

              V4 Operations

========================================*/

v4
operator-(v4 V1, r32 Scalar)
{
    v4 Result = {};

    Result.x = V1.x - Scalar;
    Result.y = V1.y - Scalar;
    Result.z = V1.z - Scalar;
    Result.w = V1.w - Scalar;

    return Result;
}

v4
operator-(v4 V1, v4 V2)
{
    v4 Result = {};

    Result.x = V1.x - V2.x;
    Result.y = V1.y - V2.y;
    Result.z = V1.z - V2.z;
    Result.w = V1.w - V2.w;

    return Result;
}

v4
operator+(v4 V1, r32 Scalar)
{
    v4 Result = {};

    Result.x = V1.x + Scalar;
    Result.y = V1.y + Scalar;
    Result.z = V1.z + Scalar;
    Result.w = V1.w + Scalar;

    return Result;
}

v4
operator+(v4 V1, v4 V2)
{
    v4 Result = {};

    Result.x = V1.x + V2.x;
    Result.y = V1.y + V2.y;
    Result.z = V1.z + V2.z;
    Result.w = V1.w + V2.w;

    return Result;
}

v4
operator*(v4 V1, r32 Scalar)
{
    v4 Result = {};

    Result.x = V1.x*Scalar;
    Result.y = V1.y*Scalar;
    Result.z = V1.z*Scalar;
    Result.w = V1.w*Scalar;

    return Result;
}

v3
VectorProduct(v3 Vector1, v3 Vector2)
{
    v3 Result = {};

    Result.x = Vector1.y*Vector2.z - Vector1.z*Vector2.y;
    Result.y = Vector1.z*Vector2.x - Vector1.x*Vector2.z;
    Result.z = Vector1.x*Vector2.y - Vector1.y*Vector2.x;

    return Result;
}

v4
operator/(v4 Vector, r32 Scalar)
{
    v4 Result = {};

    Result.x = Vector.x/Scalar;
    Result.y = Vector.y/Scalar;
    Result.z = Vector.z/Scalar;
    Result.w = Vector.w/Scalar;

    return Result;
}

v4
Lerp(v4 A, v4 B, r32 t)
{
    v4 Result = {};

    Result = A*(1.0f - t) + B*t;

    return Result;
}

local v4
NormalizedToRGB255(v4 Input)
{
    v4 Result = {};

    Result = Input*255.0f;

    return Result;
}

/*========================================

           Quaternion Operations

========================================*/

r32
Length(quaternion Quat)
{
    r32 Result = 0.0f;

    Result = SquareRoot(Quat.x*Quat.x + Quat.y*Quat.y + Quat.z*Quat.z + Quat.w*Quat.w);

    return Result;
}

quaternion
Quaternion(v3 Axis, r32 Angle)
{
    quaternion Result = {};

    Axis = NormalizeV3(Axis);
    Angle = DegreeToRadian(Angle);

    Result.x = Sin(Angle/2.0f)*Axis.x;
    Result.y = Sin(Angle/2.0f)*Axis.y;
    Result.z = Sin(Angle/2.0f)*Axis.z;
    Result.w = Cos(Angle/2.0f);

    return Result;
}

quaternion
operator*(quaternion Q, r32 Scalar)
{
    quaternion Result = {};

    Result.xyz = Q.xyz*Scalar;
    Result.w = Q.w*Scalar;

    return Result;
}

quaternion
operator*(quaternion Q1, quaternion Q2)
{
    quaternion Result = {};

    // Q1.xyz*Q2.w + Q2.xyz*Q1.w + Q1.xyz [x] Q2.xyz
    Result.xyz = Q1.xyz*Q2.w + Q2.xyz*Q1.w + VectorProduct(Q1.xyz, Q2.xyz);
    Result.w = Q1.w*Q2.w - Inner(Q1.xyz, Q2.xyz);

    return Result;
}

quaternion
QuaternionConjugate(quaternion Quat)
{
    quaternion Result = {};

    Result = Quat;
    Result.xyz = Result.xyz*(-1.0f);

    return Result;
}

quaternion
QuaternionInverse(quaternion Quat)
{
    quaternion Result = {};

    Result = QuaternionConjugate(Quat);

    return Result;
}

v3
Rotate(quaternion Q, v3 Vector)
{
    v3 Result = {};

    quaternion Point = {};
    Point.xyz = Vector;
    Point.w = 0;

    quaternion QInv = QuaternionInverse(Q);

    Result = (Q*Point*QInv).xyz;

    return Result;
}

/*========================================

              Uncategorized

========================================*/

local v4
NormalizedToRGB255(r32 R, r32 G, r32 B, r32 A)
{
    v4 Result = {};

    Result.r = R*255.0f;
    Result.g = G*255.0f;
    Result.b = B*255.0f;
    Result.a = A*255.0f;

    return Result;
}

local v4
RGB255ToNormalizedWithV4(v4 Input)
{
    v4 Result = {};

    Result = Input/255.0f;

    return Result;
}

local v4
RGB255ToNormalized(s32 R, s32 G, s32 B, s32 A)
{
    v4 Result = {};

    Result.r = R/255.0f;
    Result.g = G/255.0f;
    Result.b = B/255.0f;
    Result.a = A/255.0f;

    return Result;
}

r32
Squared(r32 Value)
{
    r32 Result = Value*Value;

    return Result;
}

v4
SRGB255ToLinear1Fast(v4 Color)
{
    v4 Result = {};

    Result.r = Squared(Color.r/255.0f);
    Result.g = Squared(Color.g/255.0f);
    Result.b = Squared(Color.b/255.0f);
    Result.a = Color.a/255.0f;

    return Result;
}

v4
Linear1ToSRGB255Fast(v4 Color)
{
    v4 Result = {};

    Result.r = 255.0f*SquareRoot(Color.r);
    Result.g = 255.0f*SquareRoot(Color.g);
    Result.b = 255.0f*SquareRoot(Color.b);
    Result.a = 255.0f*Color.a;

    return Result;
}

v4
Linear1ToSRGB255Accurate(v4 Color)
{
    v4 Result = {};

    r32 Exponent = 1.0f/2.2f;

    // Note: Proper Gamma correction here uses 1/2.2 as an exponent rather than 1/2
    Result.r = 255.0f*Power(Color.r, Exponent);
    Result.g = 255.0f*Power(Color.g, Exponent);
    Result.b = 255.0f*Power(Color.b, Exponent);
    Result.a = 255.0f*Color.a;

    return Result;
}

b32x
PointInRectangle(recti A, v2 B)
{
    b32x Result = false;

    if(B.x >= A.MinX && B.y >= A.MinY &&
       B.x <= A.MaxX && B.y <= A.MaxY)
    {
        Result = true;
    }

    return Result;
}

recti
Intersection(recti A, recti B)
{
    recti Result = {};

    Result.MinX = (A.MinX < B.MinX) ? B.MinX : A.MinX;
    Result.MinY = (A.MinY < B.MinY) ? B.MinY : A.MinY;
    Result.MaxX = (A.MaxX > B.MaxX) ? B.MaxX : A.MaxX;
    Result.MaxY = (A.MaxY > B.MaxY) ? B.MaxY : A.MaxY;

    return Result;
}

local b32
RectangleIntersects(rect A, rect B)
{
    b32 Result = false;


    // Todo: Fix for MaxPoints less than MinPoints?
    if(A.P1.x >= B.P1.x && A.P1.x <=  B.P2.x && A.P1.y >= B.P1.y && A.P1.y <= B.P2.y ||
       A.P2.x >= B.P1.x && A.P2.x <=  B.P2.x && A.P2.y >= B.P1.y && A.P2.y <= B.P2.y)
    {
        Result = true;
    }

    return Result;
}

recti
Union(recti A, recti B)
{
    recti Result = {};

    Result.MinX = (A.MinX < B.MinX) ? A.MinX : B.MinX;
    Result.MinY = (A.MinY < B.MinY) ? A.MinY : B.MinY;
    Result.MaxX = (A.MaxX > B.MaxX) ? A.MaxX : B.MaxX;
    Result.MaxY = (A.MaxY > B.MaxY) ? A.MaxY : B.MaxY;

    return Result;
}

r32
Lerp(r32 A, r32 B, r32 t)
{
    r32 Result = 0.0f;

    Result = (1.0f - t)*A + t*B;

    return Result;
}

r32
Lerp(r32 Start, r32 End, r32 Duration, r32 Time)
{
    r32 Result = 0.0f;

    r32 Difference = End - Start;

    Result = Duration*Difference/Time + Start;

    return Result;
}

/*========================================

            Data Structures

========================================*/

local inline umm
GetHashIndex(char *String)
{
    umm Result = 5381;

    u32 Codepoint;

    while(*String)
    {
		Codepoint = *String++;
        // Todo: Percent Distribution
        Result = ((Result << 5) + Result) + Codepoint; // hash * 33 + Codepoint
    }

    return Result;
}

local inline umm
GetHashIndex(u8 *String)
{
    umm Result = 5381;

    u32 Codepoint;

    while(*String)
    {
        Codepoint = *String++;
        // Todo: Percent Distribution
        Result = ((Result << 5) + Result) + Codepoint; // hash * 33 + Codepoint
    }

    return Result;
}

local inline umm
ProvideValidHashIndexInSize(char *String, umm Size)
{
    umm Result = GetHashIndex(String);

    Result %= Size;

    return Result;
}

local inline umm
ProvideValidHashIndexInSize(u8 *String, umm Size)
{
    umm Result = GetHashIndex(String);

    Result %= Size;

    return Result;
}

typedef struct trie_node
{
    // Note: Only for ascii characters, Todo: for now?
    trie_node *Branches[223];

    b32 EndOfWord;
} trie_node;

inline void
InsertNode(trie_node *Head, char *String)
{
    // Todo: This is pretty slow, probably because of the allocation, maybe
    // make it into a dynamic array or memory arena
    umm NodeStringLength = StringLength(String);
    for(umm At = 0;
        At < NodeStringLength;
        ++At)
    {
        if(!Head->Branches[*(String + At) - '0'])
        {
            Head->Branches[*(String + At) - '0'] = (trie_node *)Allocate(sizeof(trie_node));
        }

        Head = Head->Branches[*(String + At) - '0'];
    }

    Head->EndOfWord = true;
}

inline void
InsertNode(trie_node *Head, u8 *String)
{
    // Todo: This is pretty slow, probably because of the allocation, maybe
    // make it into a dynamic array or memory arena
    umm NodeStringLength = StringLength(String);
    for(umm At = 0;
        At < NodeStringLength;
        ++At)
    {
        if(!Head->Branches[*(String + At) - '0'])
        {
            Head->Branches[*(String + At) - '0'] = (trie_node *)Allocate(sizeof(trie_node));
        }

        Head = Head->Branches[*(String + At) - '0'];
    }

    Head->EndOfWord = true;
}

local inline b32
IsInTree(trie_node *Head, u8 *String)
{
    b32 Result = false;

    umm NodeStringLength = StringLength(String);
    for(umm At = 0;
        At < NodeStringLength;
        ++At)
    {
        if(!Head->Branches[*(String + At) - '0'])
        {
            break;
        }

        Head = Head->Branches[*(String + At) - '0'];
    }

    if(Head->EndOfWord)
    {
        Result = true;
    }

    return Result;
}

inline void
BurnBranch(trie_node *Head)
{
    if(Head->EndOfWord)
    {
        return;
    }

    for(umm At = 0;
        At < 75;
        ++At)
    {
        if(Head->Branches[At])
        {
            BurnBranch(Head->Branches[At]);
            Deallocate(Head->Branches[At]);
        }
    }
}

inline void
BurnTree(trie_node *Head)
{
    for(umm At = 0;
        At < 75;
        ++At)
    {
        if(Head->Branches[At])
        {
            BurnBranch(Head->Branches[At]);
            Deallocate(Head->Branches[At]);
            Head->Branches[At] = 0;
        }
    }
}

inline void *
_ArrayInsert(void **Array, umm ElementSize, umm *Count, umm *MaxCount)
{
    void *Result = 0;

    *Count = *Count + 1;

    if(*Count > *MaxCount)
    {
        *MaxCount = *Count + *Count/2;
        *Array = Reallocate(*Array, ElementSize*(*Count - 1), ElementSize*(*MaxCount));
    }

    // Note: In case we want to return a pointer to the element we can return this result
    Result = (char *)*Array + ElementSize*(*Count - 1);

    return Result;
}

void
SelectionSort(void *Base, umm Count, umm ElementSize, int(*Comparison)(const void *A, const void *B))
{
    umm SwitchIndex = 0;

    void *SwapBuffer = Allocate(ElementSize);

    for(umm Index = 0;
        Index < Count;
        ++Index)
    {
        SwitchIndex = Index;

        for(umm Scan = Index + 1;
            Scan < Count;
            ++Scan)
        {
            if(Comparison(((u8 *)Base + SwitchIndex*ElementSize), ((u8 *)Base + Scan*ElementSize)) > 0)
            {
                SwitchIndex = Scan;
            }
        }

        if(SwitchIndex != Index)
        {
            CopyMemory(SwapBuffer,                       ((u8 *)Base + Index*ElementSize),       ElementSize);
            CopyMemory(((u8 *)Base + Index*ElementSize), ((u8 *)Base + SwitchIndex*ElementSize), ElementSize);
            CopyMemory(((u8 *)Base + SwitchIndex*ElementSize), SwapBuffer,                       ElementSize);
        }
    }
}
