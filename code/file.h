typedef enum undo_type
{
    UndoType_Add,
    UndoType_Delete
} undo_type;
typedef struct undo_node
{
    undo_type UndoType;
    u8 *String;
    umm Length;
    umm BytesIn;
} undo_node;
typedef struct undo_stack
{
    umm UndoCount;
    undo_node *Stack;
    umm UndoMaxCount;
    smm Cursor;
} undo_stack;
typedef enum line_ending
{
    LineEnding_Dos,
    LineEnding_Unix
} line_ending;
typedef struct line_cache
{
    u8 *Line;
    umm BytesIn;
} line_cache;
typedef struct line_assemblage
{
    line_cache *LineCache;
    umm Count;
    umm MaxCount;
} line_assemblage;
typedef struct lexer_assemblage
{
    struct token *Tokens;
    umm Count;
    umm MaxCount;
} lexer_assemblage;
typedef enum language
{
    Language_None,
    Language_Cpp,
    Language_Jai,
    Language_Odin
} language;
typedef struct file_buffer
{
    undo_stack UndoStack;
    line_assemblage LineAssemblage;
    
    lexer_assemblage LexerAssemblage;
    language Language;
    b32 IsBinaryFile;
    b32 Parsed;
    
    // Todo: Unicode support
    u8 *Memory;
    umm Size;
    
    u8 Filename[MAX_PATH];
    u8 Filepath[MAX_PATH];
    line_ending LineEnding;
    
    b32x HasChanges;
} file_buffer;

typedef struct search_assemblage
{
    umm *WordLocation;
    smm Count;
    smm MaxCount;

    smm Cursor;
} search_assemblage;

typedef struct paste_location_data
{
    umm BytesIn;
    umm BytesLong;
    r32 Time;
} paste_location_data;
typedef struct paste_location_assemblage
{
    paste_location_data *PasteLocationData;
    smm Count;
    smm MaxCount;
} paste_location_assemblage;

typedef enum command_type
{
    CommandType_None,
    CommandType_Open,
    CommandType_ForwardSearch,
    CommandType_ReverseSearch,
    CommandType_QueryReplace,
    CommandType_SelectionQueryReplace,
    CommandType_Save,
    CommandType_GoToLine
} command_type;
typedef struct file_instance
{
    // Note: This is used to store previous instances of an Instance for
    // various purposes
    file_instance *Buffer;
    
    b32x Active;
    
    rect Window;
    
    umm CachePoint;
    file_buffer *FileBuffer;
    
    smm Cursor;
    smm Marker;
    b32 MarkerIsDown;
    b32 Insert;
    
    smm CursorBias;
    
    r32 CameraPositionYOffset;
    s32x CameraPositionY;
    r32 CameraPositionX;
    
    search_assemblage SearchAssemblage;

    paste_location_assemblage PasteLocationAssemblage;

    u8 QueryReplacee[1024];
    u8 QueryReplacer[1024];

    u8 InfoBuffer[512];
    r32 SaveIndicatorTime;
    r32 SaveIndicatorDuration = 0.20f;

    command_type CommandType;
    b32x CommandFocus;
    u8 CommandPredicate[128];
    u8 CommandBuffer[1024];
} file_instance;

typedef struct file_hash_slot file_hash_slot;

struct file_hash_slot
{
    file_buffer *File;
    
    file_hash_slot *Next;
};

typedef struct file_hash
{
    umm FileBufferCount;
    file_hash_slot *FileHashArray;
} file_hash;

typedef struct word_list
{
    u8 *Words;
    umm *WordLocations;
    umm Count;
    umm Cursor;

    umm Size;
} word_list;

#include "lexer.h"

b32
IsPastedAtBytesIn(file_instance *FileInstance, umm BytesIn, paste_location_data *OutData)
{
    b32 Result = false;

    for(smm Index = 0;
        Index < FileInstance->PasteLocationAssemblage.Count;
        ++Index)
    {
        paste_location_data *PasteData = FileInstance->PasteLocationAssemblage.PasteLocationData + Index;
        if(PasteData->BytesIn <= BytesIn && PasteData->BytesIn + PasteData->BytesLong >= BytesIn)
        {
            Result = true;
            if(OutData)
            {
                *OutData = *PasteData;
            }
        }
    }

    return Result;
}

#if 0
umm
GetPreviousSearchIndexLocationFromBytesIn(file_instance *FileInstance, umm BytesIn)
{
    umm Result = 0;
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    search_assemblage *SearchAssemblage = &FileInstance->SearchAssemblage;

    if(FileBuffer->Size && SearchAssemblage->Count && BytesIn >= 0 && BytesIn < FileBuffer->Size)
    {
        umm CommandLength = StringLength(FileInstance->CommandBuffer);

        umm At = 0;
        b32 LocalRunning = true;
        u32x Num = 1;
        u32x Denom = 2;
        while(LocalRunning)
        {
            At = (SearchAssemblage->Count*Num/Denom);

            umm *WordLocation = SearchAssemblage->WordLocation + At;

            if(BytesIn >= *WordLocation && BytesIn < *WordLocation + CommandLength)
            {
                Result = *WordLocation;
                LocalRunning = false;
            }
            else
            {
                Denom *= 2;
                if(BytesIn <= *WordLocation)
                {
                    Num += Num - 1;
                }
                else
                {
                    Num += Num + 1;
                }
            }
        }
    }

    return Result;
}
#endif

void
GetWordsForPrefix(trie_node *Head, u8 *Prefix, word_list *WordList, u8 *Buffer)
{
    if(Head->EndOfWord)
    {
        umm NewSize = StringLength(Buffer) + 1;
        WordList->Words = (u8 *)Reallocate(WordList->Words, WordList->Size, WordList->Size + NewSize);
        WordList->WordLocations = (umm *)Reallocate(WordList->WordLocations, WordList->Count*sizeof(umm), (WordList->Count + 1)*sizeof(umm));
        *(WordList->WordLocations + WordList->Count) = WordList->Size;
        WordList->Size += NewSize;
        StringCopy(WordList->Words + *(WordList->WordLocations + WordList->Count), Buffer);
        ++WordList->Count;
    }

    b32 IsLeaf = true;

    for(umm At = 0;
        At < 75;
        ++At)
    {
        if(Head->Branches[At])
        {
            IsLeaf = false;
            break;
        }
    }

    if(IsLeaf)
    {
        // Note: Rake up those leaves!
        return;
    }

    for(umm At = 0;
        At < 75;
        ++At)
    {
        if(Head->Branches[At])
        {
            umm Length = StringLength(Buffer);
            Buffer[Length] = (char)(At + '0');
            Buffer[Length + 1] = 0;

            GetWordsForPrefix(Head->Branches[At], Prefix, WordList, Buffer);

            Buffer[Length] = 0;
        }
    }
}

void
ListWordsFromPrefix(trie_node *Head, u8 *Prefix, word_list *WordList)
{
    if(WordList)
    {
        umm Length = StringLength(Prefix);
        trie_node *Node = Head;
        for(umm At = 0;
            At < Length;
            ++At)
        {
            if(!Node->Branches[*(Prefix + At) - '0'])
            {
                return;
            }

            Node = Node->Branches[*(Prefix + At) - '0'];
        }

        u8 Buffer[256]; // Note: This is to just keep track of words
        StringCopy(Buffer, Prefix);
        GetWordsForPrefix(Node, Prefix, WordList, Buffer);
    }
}

inline void
ConstructLineAssemblage(file_buffer *FileBuffer)
{
    StartTimedPoint(ContructLineAssemblage);
    line_assemblage *LineAssemblage = &FileBuffer->LineAssemblage;
    
    if(LineAssemblage->LineCache)
    {
        Deallocate(LineAssemblage->LineCache);
    }
    
    StartCountingPoint(LineAssemblage);
    LineAssemblage->LineCache = (line_cache *)Allocate(sizeof(line_cache));
    line_cache *Buffer = LineAssemblage->LineCache;
    Buffer->BytesIn = 0;
    Buffer->Line = FileBuffer->Memory;
    LineAssemblage->Count = 1;
    LineAssemblage->MaxCount = 1;
    
    if(FileBuffer->Memory)
    {
        for(u8 *At = FileBuffer->Memory;
            At < FileBuffer->Memory + FileBuffer->Size;
            ++At)
        {
            if(*At == '\n')
            { // Note: New line
                Buffer = (line_cache *)ArrayInsert(LineAssemblage->LineCache, LineAssemblage->Count, LineAssemblage->MaxCount);
                Buffer->BytesIn = (At + 1) - FileBuffer->Memory;
                Buffer->Line = At + 1;

                CycleCountingPoint(LineAssemblage);
            }
        }
    }
    EndCountingPoint(LineAssemblage);
    EndTimedPoint(ContructLineAssemblage);
}

inline void
ConstructLineAssemblageFromTokens(file_buffer *FileBuffer)
{
    StartTimedPoint(ContructLineAssemblageFromTokens);
    line_assemblage *LineAssemblage = &FileBuffer->LineAssemblage;
    lexer_assemblage *LexerAssemblage = &FileBuffer->LexerAssemblage;
    
    if(LineAssemblage->LineCache)
    {
        Deallocate(LineAssemblage->LineCache);
    }
    
    StartCountingPoint(LineAssemblageFromTokens);
    LineAssemblage->LineCache = (line_cache *)Allocate(sizeof(line_cache));
    line_cache *Buffer = LineAssemblage->LineCache;
    Buffer->BytesIn = 0;
    Buffer->Line = FileBuffer->Memory;
    LineAssemblage->Count = 1;
    LineAssemblage->MaxCount = 1;
    
    if(FileBuffer->Memory)
    {
        for(umm Index = 0;
            Index < LexerAssemblage->Count;
            ++Index)
        {
            token *Token = LexerAssemblage->Tokens + Index;
            
            if(Token->Type == Token_Literal ||
               Token->Type == Token_Comment ||
               Token->Type == Token_Whitespace)
            {
                for(u8 *At = FileBuffer->Memory + Token->BytesIn;
                    At < FileBuffer->Memory + Token->BytesIn + Token->BytesLong;
                    ++At)
                {
                    if(*At == '\n')
                    { // Note: New line
                        // Note: Reallocate and resize
                        Buffer = (line_cache *)ArrayInsert(LineAssemblage->LineCache, LineAssemblage->Count, LineAssemblage->MaxCount);
                        Buffer->BytesIn = (At + 1) - FileBuffer->Memory;
                        Buffer->Line = At + 1;
                        
                        CycleCountingPoint(LineAssemblageFromTokens);
                    }
                }
            }
        }
    }
    EndCountingPoint(LineAssemblageFromTokens);
    EndTimedPoint(ContructLineAssemblageFromTokens);
}

local inline void
ConstructAssemblages(file_buffer *FileBuffer, comment_keyword_hash *CommentKeywordHash, file_hash *FileHash, trie_node *Dictionary)
{
    if(FileBuffer->Language == Language_Cpp)
    {
        LexateCpp(FileBuffer, CommentKeywordHash, Dictionary);
        ConstructLineAssemblageFromTokens(FileBuffer);
        RebuildDictionaryForLanguage(FileHash, Dictionary, FileBuffer->Language);
    }
    else if(FileBuffer->Language == Language_Jai)
    {
#if 0
        LexateJAI(FileBuffer);
        ConstructLineAssemblageFromTokens(FileBuffer);
#else
        ConstructLineAssemblage(FileBuffer);
        RebuildDictionaryForLanguage(FileHash, Dictionary, FileBuffer->Language);
#endif
    }
    else if(FileBuffer->Language == Language_Odin)
    {
#if 0
        LexateODIN(FileBuffer);
        ConstructLineAssemblageFromTokens(FileBuffer);
#else
        ConstructLineAssemblage(FileBuffer);
        RebuildDictionaryForLanguage(FileHash, Dictionary, FileBuffer->Language);
#endif
    }
    else
    {
        ConstructLineAssemblage(FileBuffer);
        RebuildDictionaryForLanguage(FileHash, Dictionary, Language_None);
    }
}

local inline void
DeconstructAssemblages(file_buffer *FileBuffer)
{
    if(FileBuffer->LexerAssemblage.Tokens)
    {
        Deallocate(FileBuffer->LexerAssemblage.Tokens);
    }
    FileBuffer->LexerAssemblage.Count = 0;
    FileBuffer->LexerAssemblage.MaxCount = 0;

    if(FileBuffer->LineAssemblage.LineCache)
    {
        Deallocate(FileBuffer->LineAssemblage.LineCache);
    }
    FileBuffer->LineAssemblage.Count = 0;
    FileBuffer->LineAssemblage.MaxCount = 0;
}

// Note: These prototypes are here because AddString/DeleteSection and Undo call eachother
void AddString(file_buffer *FileBuffer, u8 *String, umm BytesIn, b32 AddUndoNode);
void DeleteSection(file_buffer *FileBuffer, umm FirstPoint, umm SecondPoint, b32 AddUndoNode);

void
PushUndoNode(undo_stack *UndoStack, undo_type UndoType, u8 *String, u32x Length, u32x BytesIn, b32x MoveCursor = true)
{
    if(Length && StringLength(String))
    {
        if(UndoStack->UndoCount == UndoStack->UndoMaxCount)
        {
            Deallocate(UndoStack->Stack->String);
            MoveMemory(UndoStack->Stack, UndoStack->Stack + 1, sizeof(undo_node)*(UndoStack->UndoMaxCount - 1));
            
            undo_node *Node = (UndoStack->Stack + (UndoStack->UndoCount - 1));
            Node->BytesIn = BytesIn;
            Node->UndoType = UndoType;
            Node->String = (u8 *)Allocate(StringLength(String) + 1);
            StringCopy(Node->String, String);
            Node->Length = Length;
        }
        else
        {
            undo_node *Node = (UndoStack->Stack + UndoStack->UndoCount++);
            Node->BytesIn = BytesIn;
            Node->UndoType = UndoType;
            Node->String = (u8 *)Allocate(StringLength(String) + 1);
            StringCopy(Node->String, String);
            Node->Length = Length;
        }
        if(UndoStack->UndoCount > 0)
        {
            UndoStack->Cursor = UndoStack->UndoCount - 1;
        }
    }
}

void
Undo(file_buffer *FileBuffer)
{
    StartTimedPoint(Undo);
    undo_stack *UndoStack = &FileBuffer->UndoStack;
    if(UndoStack->UndoCount && UndoStack->Cursor >= 0 && UndoStack->Cursor < (smm)UndoStack->UndoCount)
    {
        undo_node *Node = (UndoStack->Stack + UndoStack->Cursor);
        
        if(Node->UndoType == UndoType_Delete)
        {
            AddString(FileBuffer, Node->String, Node->BytesIn, false);
            
            Node->UndoType = UndoType_Add;
        }
        else
        {
            DeleteSection(FileBuffer, Node->BytesIn, Node->BytesIn + Node->Length, false);
            
            Node->UndoType = UndoType_Delete;
        }
        
        if((u32x)UndoStack->Cursor < UndoStack->UndoCount - 1)
        {
            undo_node CopyNode = *Node;
            
            MoveMemory(UndoStack->Stack + UndoStack->Cursor, UndoStack->Stack + UndoStack->Cursor + 1, sizeof(undo_node)*((UndoStack->UndoCount - 1) - UndoStack->Cursor));
            undo_node *CopyToNode = (UndoStack->Stack + UndoStack->UndoCount - 1);
            *CopyToNode = CopyNode;
        }
        --UndoStack->Cursor;
    }
    EndTimedPoint(Undo);
}

local inline void
FromLastSlash(u8 *Dest, u8 *Source)
{
    StringCopy(Dest, Source);
    u8 *At = Source + StringLength(Source);
    for(;
        At >= Source;
        --At)
    {
        if(*At == '\\' ||
           *At == '/')
        {
            StringCopy(Dest, ++At);
            break;
        }
    }
}

local inline void
ToLastSlash(u8 *Dest, u8 *Source)
{
    StringCopy(Dest, Source);
    b32x HasSlash = false;
    u8 *At = Source + StringLength(Source);
    for(;
        At >= Source;
        --At)
    {
        if(*At == '\\' ||
           *At == '/')
        {
            *(Dest + (At - Source) + 1) = '\0';
            HasSlash = true;
            break;
        }
    }
    
    if(!HasSlash)
    {
        StringCopy(Dest, ".\\");
    }
}

local file_buffer *
ReadFileIntoFileBuffer(u8 *Filename, comment_keyword_hash *CommentKeywordHash, file_hash *FileHash, trie_node *Dictionary, config *Config)
{
    file_buffer *Result = (file_buffer *)Allocate(sizeof(file_buffer));
    
    debug_read_file_result ReadResult = DebugReadFile(Filename);
    
    if(ReadResult.Memory)
    {
        Result->Memory = (u8 *)ReadResult.Memory;
        Result->Size = ReadResult.Size;

        b32 HasUTFByteMarkers = false;
        b32 ChosenLineEnding = false;
        StartTimedPoint(CheckFile);
        // Todo: Do this check in a seperate thread?
        for(u8 *At = (u8 *)ReadResult.Memory;
            At < (u8 *)ReadResult.Memory + ReadResult.Size;
            ++At)
        {
            // Todo: Check for UTF ByteOrderMarks
            if(!ChosenLineEnding)
            {
                if(*At == '\r')
                {
                    if((At + 1) < (u8 *)ReadResult.Memory + ReadResult.Size &&
                        *(At + 1) == '\n')
                    {
                        Result->LineEnding = LineEnding_Dos;
                    }
                    else
                    {
                        Log("Remember that one thing that you'd be surprised if it happened? Yeah it _just_ happened.");
                    }

                    ChosenLineEnding = true;
                }
                else if(*At == '\n')
                {
                    Result->LineEnding = LineEnding_Unix;
                    ChosenLineEnding = true;
                }
            }

            if((*At < 32 || *At > 127) && *At != '\r' && *At != '\n' && *At != '\t')
            {
                // Todo: Should probably do more testing with this to see if there's other tell tale signs that
                //       a file is a binary file
                Log("Binary File Flag set, Byte Found: 0x%hhx -> %c \n", *At, *At);
                Result->IsBinaryFile = true;
                break;
            }
        }
        EndTimedPoint(CheckFile);

        Result->UndoStack.UndoMaxCount = Config->UndoMaxCount;
        Result->UndoStack.Stack = (undo_node *)Allocate(sizeof(undo_node)*Result->UndoStack.UndoMaxCount);

        u8 *FileExtension = Filename + StringLength(Filename) - 4;
        if(CompareString(FileExtension, ".cpp") ||
            CompareString(FileExtension + 2, ".c") ||
            CompareString(FileExtension + 2, ".h"))
        {
            Result->Language = Language_Cpp;
            LexateCpp(Result, CommentKeywordHash, Dictionary);
            ConstructLineAssemblageFromTokens(Result);
        }
        else if(CompareString(FileExtension, ".jai"))
        {
            Result->Language = Language_Jai;
            LexateCpp(Result, CommentKeywordHash, Dictionary);
            ConstructLineAssemblageFromTokens(Result);
        }
        else if(CompareString(FileExtension - 1, ".odin"))
        {
            Result->Language = Language_Odin;
            LexateCpp(Result, CommentKeywordHash, Dictionary);
            ConstructLineAssemblageFromTokens(Result);
        }
        else
        {
            Result->Language = Language_None;
            ConstructLineAssemblage(Result);
        }

    }
    else
    {
        Result->Memory = 0;
        Result->Size = 0;
        Result->LineEnding = LineEnding_Unix;
        Result->UndoStack.UndoMaxCount = Config->UndoMaxCount;
        Result->UndoStack.Stack = (undo_node *)Allocate(sizeof(undo_node)*Result->UndoStack.UndoMaxCount);
    }
    FromLastSlash(Result->Filename, Filename);
    ToLastSlash(Result->Filepath, Filename);
    
    return Result;
}

local inline void
PlaceFileBufferIntoHash(file_hash *FileHash, file_buffer *FileBuffer)
{
    StartTimedPoint(PlaceFileBufferIntoHash);
    umm ArrayIndex = ProvideValidHashIndexInSize(FileBuffer->Filename, FileHash->FileBufferCount);
    
    for(file_hash_slot *At = FileHash->FileHashArray + ArrayIndex;
        ;
        At = At->Next)
    {
        if(!At->File)
        {
            At->File = FileBuffer;
            At->Next = 0;
            break;
        }
        else if(CompareString(At->File->Filename, FileBuffer->Filename))
        {
            // Note/Todo: Can't think of any reason to have multiples of a file in different filebuffers
            break;
        }
        else if(!At->Next)
        {
            At->Next = (file_hash_slot *)Allocate(sizeof(file_hash_slot));
            At->Next->File = FileBuffer;
            At->Next->Next = 0;
            break;
        }
    }
    
    EndTimedPoint(PlaceFileBufferIntoHash);
}

local inline file_buffer *
GetFileBufferFromHash(file_hash *FileHash, u8 *String)
{
    file_buffer *Result = 0;
    umm ArrayIndex = ProvideValidHashIndexInSize(String, FileHash->FileBufferCount);
    
    for(file_hash_slot *At = FileHash->FileHashArray + ArrayIndex;
        At;
        At = At->Next)
    {
        if(At->File && CompareString(At->File->Filename, String))
        {
            Result = At->File;
            break;
        }
    }
    
    // Assert(Result != 0);
    return Result;
}

u8 *
// IMPORTANT: This is platform DEPENDANT
GetDirectoryListing(file_hash *FileHash, u8 *Path)
{
    // Note: Allocate 5 for at least "->..\n"
    u8 *Result = (u8 *)Allocate(5);
    
    u8 ResultingSearch[MAX_PATH] = "";
    
    StringCat(ResultingSearch, Path);
    StringCat(ResultingSearch, (u8 *)"*");
    
    WIN32_FIND_DATA FindFileData;
    
    // Todo: Use FindFirstFileW
    HANDLE FindHandle = FindFirstFile((char *)ResultingSearch, &FindFileData);
    
    size_t StringSize = 0;
    if(FindHandle != INVALID_HANDLE_VALUE)
    {
        do
        {
            if(!CompareString(FindFileData.cFileName, "."))
            {
                StringSize = StringLength(Result) + StringLength(FindFileData.cFileName) + 4;
                Result = (u8 *)Reallocate(Result, StringLength(Result), StringSize);
                
                if(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                {
                    StringCat(Result, "->");
                }
                else if(GetFileBufferFromHash(FileHash, (u8 *)FindFileData.cFileName))
                {
                    StringCat(Result, "# ");
                }
                else
                {
                    StringCat(Result, "  ");
                }
                StringCat(Result, (u8 *)FindFileData.cFileName);
                StringCat(Result, "\n");
            }
        }
        while(FindNextFile(FindHandle, &FindFileData));
    }
    if(StringLength(Result))
    {
        *(Result + StringLength(Result) - 1) = 0;
    }
    
    FindClose(FindHandle);
    return Result;
}

local file_buffer *
ReadFileIntoFileHash(comment_keyword_hash *CommentKeywordhash, file_hash *FileHash, u8 *Filename, trie_node *Dictionary, config *Config)
{
    file_buffer *FileBuffer = ReadFileIntoFileBuffer(Filename, CommentKeywordhash, FileHash, Dictionary, Config);
    PlaceFileBufferIntoHash(FileHash, FileBuffer);
    RebuildDictionaryForLanguage(FileHash, Dictionary, FileBuffer->Language);
    return FileBuffer;
}

local u32x
ListFileBuffers(file_hash *FileHash, u8 *Output)
{ // Todo: Do something with this?
    u32x BytesWrote = 0;
    
    u32x Index = 0;
    
    u8 *Dest = Output;
    for(file_hash_slot *At = FileHash->FileHashArray;
        At < FileHash->FileHashArray + FileHash->FileBufferCount;
        ++At, ++Index)
    {
        if(At->File)
        {
            u32x SideIndex = 0;
            for(file_hash_slot *AtLink = At;
                AtLink;
                AtLink = AtLink->Next, ++SideIndex)
            {
                u32x Length = StringLength(AtLink->File->Filename);
                StringCopy(Dest, (u8 *)AtLink->File->Filename);
                Dest += Length;
                *Dest++ = '\n';
                BytesWrote += Length + 1;
            }
        }
    }
    
    return BytesWrote;
}

local void
ListLoadedFileBuffersInFileBuffer(file_hash *FileHash, file_buffer *FileBuffer)
{
    // Note: Size if we had the entire array filled, the max each filename could be
    // and then some flexibility for if there were 3 collisions in every slot
    u32x TempSize = FileHash->FileBufferCount*MAX_PATH*4;
    FileBuffer->Memory = (u8 *)Allocate(TempSize);
    FileBuffer->Size = ListFileBuffers(FileHash, FileBuffer->Memory);
    FileBuffer->Memory = (u8 *)Reallocate(FileBuffer->Memory, TempSize, FileBuffer->Size);
    ConstructLineAssemblage(FileBuffer);
}

void
FreeFileBuffer(file_buffer **FileBuffer)
{
    if(*FileBuffer)
    {
        // Deallocate(FileBuffer->Memory);
        Deallocate((*FileBuffer)->UndoStack.Stack);
        Deallocate((*FileBuffer)->LineAssemblage.LineCache);
        Deallocate((*FileBuffer)->LexerAssemblage.Tokens);
        Deallocate(*FileBuffer);
        *FileBuffer = 0;
    }
}

void
ResetFileInstance(file_instance *FileInstance)
{
    FileInstance->Cursor = 0;
    FileInstance->Marker = 0;
    FileInstance->MarkerIsDown = false;
    FileInstance->Insert = false;
    
    FileInstance->CursorBias = 1;
    
    FileInstance->CameraPositionX = 0;
    FileInstance->CameraPositionY = 0;
    FileInstance->CameraPositionYOffset = 0;
    
    FileInstance->CommandType = CommandType_None;
    FileInstance->CommandFocus = false;
    StringCopy(FileInstance->CommandPredicate, "");
    StringCopy(FileInstance->CommandBuffer, "");
}

file_instance
MakeInstanceOfFileBuffer(file_buffer *FileBuffer, config *Config)
{
    file_instance Result = {};
    
    if(FileBuffer)
    {
        Result.FileBuffer = FileBuffer;
        Result.CursorBias = 1;
        Result.Active = false;
    }
    else
    {
        InvalidCodePath;
    }
    
    return Result;
}

file_buffer *
MakeNullFileBuffer(config *Config)
{
    file_buffer *Result = 0;
    Result = (file_buffer *)Allocate(sizeof(file_buffer));
    StringCopy(Result->Filename, "*empty*");
    
    Result->Memory = 0;
    Result->Size = 0;
    
    Result->UndoStack.UndoMaxCount = Config->UndoMaxCount;
    Result->UndoStack.Stack = (undo_node *)Allocate(sizeof(undo_node)*Result->UndoStack.UndoMaxCount);
    
    return Result;
}

file_instance
MakeNullFileInstance(config *Config)
{
    file_instance Result = {};
    Result.FileBuffer = (file_buffer *)Allocate(sizeof(file_buffer));
    
    StringCopy(Result.FileBuffer->Filename, "*empty*");
    Result.FileBuffer->Memory = 0;
    Result.FileBuffer->Size = 0;
    
    Result.FileBuffer->UndoStack.UndoMaxCount = Config->UndoMaxCount;
    Result.FileBuffer->UndoStack.Stack = (undo_node *)Allocate(sizeof(undo_node)*Result.FileBuffer->UndoStack.UndoMaxCount);
    
    Result.CursorBias = 1;
    
    return Result;
}

void
InsertCarriageReturns(file_buffer *FileBuffer)
{
    file_buffer Result = {};
    
    StringCopy(Result.Filename, FileBuffer->Filename);
    Result.LineEnding = FileBuffer->LineEnding;
    Result.Size = FileBuffer->Size + FileBuffer->LineAssemblage.Count - 1;
    Result.Memory = (u8 *)Allocate(Result.Size);
    
    umm ResultSize = 0;
    u8 *Dest = Result.Memory;
    for(u8 *At = FileBuffer->Memory;
        At < FileBuffer->Memory + FileBuffer->Size;
        ++At, ++ResultSize)
    {
        if(*At == '\n')
        {
            ++ResultSize;
            *Dest++ = '\r';
        }
        *Dest++ = *At;
    }
    
    Deallocate(FileBuffer->Memory);
    *FileBuffer = Result;
}

file_buffer *
// Note: Allocates a file buffer, make sure if not using a file buffer to remove with
// FreeFileBuffer(file_buffer *)
CopyFileBuffer(file_buffer *FileBuffer)
{
    file_buffer *Result = (file_buffer *)Allocate(sizeof(file_buffer));
    
    Result->Memory = (u8 *)Allocate(FileBuffer->Size);
    CopyMemory(Result->Memory, FileBuffer->Memory, FileBuffer->Size);
    Result->Size = FileBuffer->Size;
    Result->LineEnding = FileBuffer->LineEnding;
    StringCopy(Result->Filename, FileBuffer->Filename);
    
#if 0 // Note: When something needs this, I will have it be done, but for now only save uses this and yeah...
    if(FileBuffer->Language != Language_none)
    {
        Result->LexerAssemblage = FileBuffer->LexerAssemblage;
        Result->LexerAssemblage.Tokens = (token *)Allocate(FileBuffer->LexerAssemblage.MaxCount*sizeof(token));
        CopyMemory(Result->LexerAssemblage.Tokens, FileBuffer->LexerAssemblage.Tokens, FileBuffer->LexerAssemblage.Count*sizeof(token));
    }
#endif
    
    Result->LineAssemblage = FileBuffer->LineAssemblage;
    Result->LineAssemblage.LineCache = (line_cache *)Allocate(FileBuffer->LineAssemblage.MaxCount*sizeof(line_cache));
    CopyMemory(Result->LineAssemblage.LineCache, FileBuffer->LineAssemblage.LineCache, FileBuffer->LineAssemblage.Count*sizeof(line_cache));
    
    return Result;
}

r32
GetFontXOffsetInPixels(file_instance *FileInstance, font *Font, u8 *StartOfLine, s32x LineLength, b32 LineWrapping)
{
    r32 Result = 0.0f;

    for(u8 *At = StartOfLine;
        At < StartOfLine + LineLength;
        ++At)
    {
        u32 Codepoint = *At;
        
        r32 AdvanceX = GetAdvanceX(Font, Codepoint);
        
        if(*At == '\t') AdvanceX *= 4;
        
#if 1
        r32 KerningAmount = 0;
        if(*(At + 1))
        {
            KerningAmount = GetKerning(Font, Codepoint, (u32)*(At + 1));
        }

        Result += KerningAmount + AdvanceX;
#else
        r32 _UnusedY = 0.0f;
        stbtt_aligned_quad Quad;
        stbtt_GetPackedQuad(Font->FontAtlas.CharData, Font->FontAtlas.Width, Font->FontAtlas.Height,
            *At,
            &Result, &_UnusedY,
            &Quad,
            false);
#endif

        if(LineWrapping)
        {
            if(Result + (2*AdvanceX - 4.0f) - FileInstance->CameraPositionX > FileInstance->Window.P2.x)
            {
                Result = 0.0f;
            }
        }
    }
    
    return Result;
}

umm
GetLineLength(file_buffer *FileBuffer, umm Line)
{
    umm Result = 0;
    
    if(FileBuffer->Size)
    {
        umm LineStart = (FileBuffer->LineAssemblage.LineCache + Line - 1)->BytesIn;
        for(u8 *At = FileBuffer->Memory + LineStart;
            At >= FileBuffer->Memory && At < FileBuffer->Memory + FileBuffer->Size;
            ++At, ++Result)
        {
            if(*At == '\n')
            {
                ++Result;
                break;
            }
        }
    }
    
    return Result;
}

umm
GetLineAtBytesIn(file_buffer *FileBuffer, umm BytesIn)
{
    umm Result = 1;
    
    if(FileBuffer->Size)
    {
        Assert(BytesIn >= 0 && BytesIn <= FileBuffer->Size);
        if(BytesIn == FileBuffer->Size)
        {
            Result = FileBuffer->LineAssemblage.Count;
        }
        else
        {
#if 1
            umm At = 0;
            
            line_cache *Line = 0;
#if 1
            b32 LocalRunning = true;
            umm Num = 1;
            umm Denom = 2;
            while(LocalRunning)
            {
                At = (FileBuffer->LineAssemblage.Count*Num/Denom);
                
                Line = FileBuffer->LineAssemblage.LineCache + At;
                umm LineLength = GetLineLength(FileBuffer, At + 1);
                if(BytesIn >= Line->BytesIn && BytesIn < Line->BytesIn + LineLength)
                {
                    Result = At + 1;
                    LocalRunning = false;
                }
                else
                {
                    Denom *= 2;
                    if(BytesIn <= Line->BytesIn)
                    {
                        Num += Num - 1;
                    }
                    else
                    {
                        Num += Num + 1;
                    }
                }
            }
        }
#else
        for(Line = FileBuffer->LineAssemblage.LineCache;
            At < FileBuffer->LineAssemblage.Count;
            ++At, ++Line)
        {
            if(BytesIn < Line->BytesIn)
            {
                Result = At;
                break;
            }
        }
        
        if(BytesIn <= FileBuffer->Size &&
           BytesIn > Line->BytesIn)
        {
            Result = At;
        }
#endif
        
#else
        for(char *At = FileBuffer->Memory;
            At < (FileBuffer->Memory + BytesIn);
            ++At)
        {
            if(*At == '\n')
            {
                ++Result;
            }
        }
#endif
    }
    
    return Result;
}

u32x
GetBytesInAtLine(file_buffer *FileBuffer, u32x Line)
{
    u32x Result = 0;
    
    if(FileBuffer->Size)
    {
        Result = (FileBuffer->LineAssemblage.LineCache + Line - 1)->BytesIn;
    }
    
    return Result;
}

r32
GetBytesInXOffsetInPixels(file_instance *FileInstance, font *Font, u32x BytesIn, b32 LineWrapping)
{
    r32 Result = 0.0f;
    
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    if(FileBuffer->Size)
    {
        s32x LengthToPoint = 0;
        
        u8 *At;
        for(At = FileBuffer->Memory + BytesIn - 1;
            At >= FileBuffer->Memory;
            --At, ++LengthToPoint)
        {
            if(*At == '\n')
            {
                ++At;
                break;
            }
        }
        
        if(At < FileBuffer->Memory)
        {
            At = FileBuffer->Memory;
        }
        
        Result = GetFontXOffsetInPixels(FileInstance, Font, At, LengthToPoint, LineWrapping);
    }
    
    return Result;
}

void
AddString(file_buffer *FileBuffer, u8 *String, umm BytesIn, b32 AddUndoNode)
{
    umm StringSize = StringLength(String);
    FileBuffer->Memory = (u8 *)Reallocate(FileBuffer->Memory, FileBuffer->Size, FileBuffer->Size + StringSize);
    u8 *Memory = FileBuffer->Memory + BytesIn;
    MoveMemory(Memory + StringSize, Memory, (umm)(FileBuffer->Size - BytesIn));
    
    for(u8 *At = String;
        *At;
        ++At)
    {
        *Memory++ = *At;
    }
    
    if(AddUndoNode)
    {
        PushUndoNode(&FileBuffer->UndoStack, UndoType_Add, String, StringSize, (u32x)BytesIn);
    }
    
    FileBuffer->Size += StringSize;
    FileBuffer->HasChanges = true;
}

void
AddCharacter(file_buffer *FileBuffer, u8 Character, umm BytesIn, b32 AddUndoNode)
{
    FileBuffer->Memory = (u8 *)Reallocate(FileBuffer->Memory, FileBuffer->Size, FileBuffer->Size + 1);
    u8 *Memory = FileBuffer->Memory + BytesIn;
    MoveMemory(Memory + 1, Memory, (umm)(FileBuffer->Size - BytesIn));
    *Memory = Character;
    ++FileBuffer->Size;
    
    u8 AddedCharacter[2];
    AddedCharacter[0] = Character;
    AddedCharacter[1] = '\0';
    
    if(AddUndoNode)
    {
        PushUndoNode(&FileBuffer->UndoStack, UndoType_Add, AddedCharacter, 1, (u32x)BytesIn);
    }
    
    FileBuffer->HasChanges = true;
}

void
// Note: Inclusive left, exclusive right [FirstPoint, SecondPoint)
DeleteSection(file_buffer *FileBuffer, umm FirstPoint, umm SecondPoint, b32 AddUndoNode)
{
    umm MinPoint = Min(FirstPoint, SecondPoint);
    umm MaxPoint = Max(FirstPoint, SecondPoint);
    umm Diff = (MaxPoint - MinPoint);
    
    u8 *Memory = FileBuffer->Memory + MinPoint;
    
    if(AddUndoNode)
    {
        u8 *TempString = (u8 *)Allocate(Diff + 1);
        
        // Note: For storing the characters for undo
        umm At = 0;
        for(;
            At < Diff;
            ++At)
        {
            TempString[At] = Memory[At];
        }
        TempString[At] = '\0';
        PushUndoNode(&FileBuffer->UndoStack, UndoType_Delete, TempString, Diff, MinPoint);
        
        Deallocate(TempString);
    }
    
    MoveMemory(Memory, Memory + Diff, (umm)(FileBuffer->Size - MaxPoint));
    FileBuffer->Memory = (u8 *)Reallocate(FileBuffer->Memory, FileBuffer->Size, FileBuffer->Size - Diff);
    FileBuffer->Size -= Diff;
    
    FileBuffer->HasChanges = true;
}

void
DeleteCharacter(file_buffer *FileBuffer, umm BytesIn, b32 AddUndoNode)
{
    u8 *Memory = FileBuffer->Memory + BytesIn;
    u8 DeletedCharacter[2];
    DeletedCharacter[0] = *Memory;
    DeletedCharacter[1] = '\0';
    
    MoveMemory(Memory, Memory + 1, FileBuffer->Size - BytesIn - 1);
    FileBuffer->Memory = (u8 *)Reallocate(FileBuffer->Memory, FileBuffer->Size, FileBuffer->Size - 1);
    --FileBuffer->Size;
    
    if(AddUndoNode)
    {
        PushUndoNode(&FileBuffer->UndoStack, UndoType_Delete, DeletedCharacter, 1, BytesIn);
    }
    
    FileBuffer->HasChanges = true;
}

void
DeleteLine(file_buffer *FileBuffer, umm Line, b32 AddUndoNode)
{
    line_cache *LineCache = (FileBuffer->LineAssemblage.LineCache + Line - 1);

    if(FileBuffer->Size && Line - 1 >= 0 && Line - 1 < FileBuffer->LineAssemblage.Count && LineCache->Line)
    {
        umm EndOfLine = 0;
        if(Line < FileBuffer->LineAssemblage.Count && (LineCache + 1)->Line)
        {
            EndOfLine = (LineCache + 1)->BytesIn;
        }
        else
        {
            EndOfLine = FileBuffer->Size;
        }

        DeleteSection(FileBuffer, LineCache->BytesIn, EndOfLine, AddUndoNode);
    }
}

local smm
BeginningOfCursorLine(file_instance *FileInstance)
{
    smm Result = 0;
    
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    for(u8 *At = FileBuffer->Memory + FileInstance->Cursor - 1;
        At < FileBuffer->Memory + FileBuffer->Size && At >= FileBuffer->Memory;
        --At)
    {
        if(*At == '\n')
        {
            Result = (smm)(At - FileBuffer->Memory) + 1;
            break;
        }
    }
    
    return Result;
}

local smm
EndOfCursorLine(file_instance *FileInstance)
{
    smm Result = 0;
    
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    if(FileBuffer->Size)
    {
        for(u8 *At = FileBuffer->Memory + FileInstance->Cursor;
            At <= FileBuffer->Memory + FileBuffer->Size && At >= FileBuffer->Memory;
            ++At)
        {
            if(*At == '\n')
            {
                Result = (smm)(At - FileBuffer->Memory);
                break;
            }
            if(At == FileBuffer->Memory + FileBuffer->Size)
            {
                Result = FileBuffer->Size;
            }
        }
    }
    
    return Result;
}

local void
AdvanceWord(file_instance *FileInstance)
{
    b32 CrossedWhitespace = false;
    b32 CrossedNonwhitespace = false;
    
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    for(u8 *At = FileBuffer->Memory + FileInstance->Cursor;
        At <= FileBuffer->Memory + FileBuffer->Size;
        ++At)
    {
        if(At == FileBuffer->Memory + FileBuffer->Size)
        {
            FileInstance->Cursor = FileBuffer->Size;
            break;
        }
        
        if(*At == '\n')
        {
            if(CrossedNonwhitespace)
            {
                FileInstance->Cursor = (smm)(At - FileBuffer->Memory);
                break;
            }
            else
            {
                if(*(At + 1))
                {
                    ++At;
                }
                CrossedWhitespace = true;
            }
        }
        
        if(CrossedWhitespace && (!IsWhitespace(*At) || *At == '\n'))
        {
            FileInstance->Cursor = (smm)(At - FileBuffer->Memory);
            break;
        }
        else if(IsWhitespace(*At))
        {
            CrossedWhitespace = true;
        }
        else
        {
            CrossedNonwhitespace = true;
        }
    }
}

local void
PrecursorWord(file_instance *FileInstance)
{
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    if(FileBuffer->Size)
    {
        b32 CrossedWhitespace = false;
        b32 CrossedNonwhitespace = false;
        
        for(u8 *At = FileBuffer->Memory + FileInstance->Cursor - 1;
            At >= FileBuffer->Memory;
            --At)
        {
            if(At == FileBuffer->Memory)
            {
                FileInstance->Cursor = 0;
                break;
            }
            
            if((CrossedNonwhitespace && IsWhitespace(*At)) ||
               At == FileBuffer->Memory)
            {
                FileInstance->Cursor = (smm)(At - FileBuffer->Memory) + 1;
                break;
            }
            
            if(IsWhitespace(*At))
            {
                CrossedWhitespace = true;
            }
            else
            {
                CrossedNonwhitespace = true;
            }
            
            if(*At == '\n')
            {
                FileInstance->Cursor = (smm)(At - FileBuffer->Memory);
                break;
            }
        }
    }
}

local inline void
AdvanceLexicalWord(file_instance *FileInstance)
{
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    lexer_assemblage *LexerAssemblage = &FileBuffer->LexerAssemblage;
    
    if(FileBuffer->Size)
    {
        umm Index = 0;
        token *Token = GetTokenAtPoint(FileBuffer, FileInstance->Cursor, &Index);
        token *NextToken = (Token + 1);
        
        if(Token && NextToken < (LexerAssemblage->Tokens + LexerAssemblage->Count))
        {
            if(Token->Type == Token_Literal)
            {
                // Note: We want to advance by word rather than skipping the entire token
                AdvanceWord(FileInstance);
                if(FileInstance->Cursor >= (smm)NextToken->BytesIn)
                {
                    // Note: e.g. "Hello, Sailor!";
                    //       If you advance by word from "Sailor" this will 'stop' _before_ the semi-colon
                    FileInstance->Cursor = (smm)(NextToken->BytesIn);
                }
            }
            else if(NextToken->Type == Token_Whitespace)
            {
                u8 *At;
                for(At = FileBuffer->Memory + NextToken->BytesIn;
                    At < FileBuffer->Memory + FileBuffer->Size && At - FileBuffer->Memory - NextToken->BytesIn < NextToken->BytesLong;
                    ++At)
                {
                    if(*At == '\n')
                    {
                        break;
                    }
                }
                FileInstance->Cursor = (smm)(At - FileBuffer->Memory);
            }
            else
            {
                if(NextToken->SubType == Token_Whitespace)
                {
                    u8 *At;
                    for(At = FileBuffer->Memory + NextToken->BytesIn;
                        At < FileBuffer->Memory + FileBuffer->Size && At - FileBuffer->Memory - NextToken->BytesIn < NextToken->BytesLong;
                        ++At)
                    {
                        if(*At == '\n')
                        {
                            break;
                        }
                    }
                    FileInstance->Cursor = (smm)(At - FileBuffer->Memory);
                }
                else
                {
                    FileInstance->Cursor = (smm)(NextToken->BytesIn);
                }
            }
        }
        else if(NextToken == (LexerAssemblage->Tokens + LexerAssemblage->Count))
        {
            FileInstance->Cursor = FileBuffer->Size;
        }
    }
}

local inline void
PrecursorLexicalWord(file_instance *FileInstance)
{
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    lexer_assemblage *LexerAssemblage = &FileBuffer->LexerAssemblage;
    
    if(FileBuffer->Size)
    {
        umm Index = 0;
        token *Token = 0;
        if((umm)FileInstance->Cursor == FileBuffer->Size)
        {
            Token = GetTokenAtPoint(FileBuffer, FileInstance->Cursor - 1, &Index);
        }
        else
        {
            Token = GetTokenAtPoint(FileBuffer, FileInstance->Cursor, &Index);
        }
        token *PrevToken = (Token - 1);
        
        if(PrevToken >= LexerAssemblage->Tokens)
        {
            if(Token->Type == Token_Literal ||
               PrevToken->Type == Token_Literal)
            {
                PrecursorWord(FileInstance);
            }
            else if(FileInstance->Cursor - 1 >= (smm)Token->BytesIn && Token->Type != Token_Whitespace)
            {
                FileInstance->Cursor = (smm)(Token->BytesIn);
            }
            else if(PrevToken >= LexerAssemblage->Tokens)
            {
                u8 *At = 0;

                if(Token->Type == Token_Whitespace || PrevToken->Type == Token_Whitespace ||
                   Token->SubType == Token_Whitespace || PrevToken->SubType == Token_Whitespace)
                {
                    b32 Found = false;

                    if(Token->Type == Token_Whitespace || Token->SubType == Token_Whitespace)
                    {
                        At = FileBuffer->Memory + FileInstance->Cursor - 1;
                        if(At >= FileBuffer->Memory && *At == '\n')
                        {
                            --At;
                        }
                        for(;
                            At >= FileBuffer->Memory && (umm)(At - FileBuffer->Memory) >= Token->BytesIn;
                            --At)
                        {
                            if(*At == '\n')
                            {
                                Found = true;
                                FileInstance->Cursor = (smm)(At - FileBuffer->Memory + 1);
                                break;
                            }
                        }

                        if(!Found)
                        {
                            FileInstance->Cursor = PrevToken->BytesIn;
                        }
                    }
                    else if(PrevToken->Type == Token_Whitespace || PrevToken->SubType == Token_Whitespace)
                    {
                        token *PrevPrevToken = (Token - 2);
                        if(PrevPrevToken >= LexerAssemblage->Tokens)
                        {
                            for(At = FileBuffer->Memory + PrevToken->BytesIn + PrevToken->BytesLong - 1;
                                At >= FileBuffer->Memory && (umm)(At - FileBuffer->Memory) > PrevPrevToken->BytesIn;
                                --At)
                            {
                                if(At - 1 >= FileBuffer->Memory && *(At - 1) == '\n')
                                {
                                    break;
                                }
                            }
                            FileInstance->Cursor = (smm)(At - FileBuffer->Memory);
                        }
                        else
                        {
                            FileInstance->Cursor = 0;
                        }
                    }
                }
                else
                {
                    FileInstance->Cursor = PrevToken->BytesIn;
                }
            }
        }
    }
}

local inline umm
BytesLeftOfPoint(file_buffer *FileBuffer, umm BytesIn)
{
    umm Result = 0;
    
    if(FileBuffer->Size)
    {
        for(u8 *At = FileBuffer->Memory + BytesIn - 1;
            At >= FileBuffer->Memory;
            --At)
        {
            if(*At == '\n')
            {
                break;
            }
            ++Result;
        }
    }
    
    return Result;
}

local inline umm
GetCursorBias(file_instance *FileInstance)
{
    umm Result = 0;
    
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    if(FileBuffer->Size)
    {
        Result = BytesLeftOfPoint(FileBuffer, FileInstance->Cursor) + 1;
    }
    else
    {
        Result = 1;
    }
    
    return Result;
}

local void
MoveCursorUpLine(file_instance *FileInstance, b32 LineWrapping)
{
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    if(FileBuffer->Size)
    {
        u32x CursorsLine = GetLineAtBytesIn(FileBuffer, FileInstance->Cursor);
        if(CursorsLine > 1)
        {
            u32x LineLength = GetLineLength(FileBuffer, CursorsLine);
            u32x PreviousLineLength = GetLineLength(FileBuffer, CursorsLine - 1);
            
            // Note: -1 for previous line, but LineCache starts indexing at 0 so -2
            u32x StartOfUpperLine = (FileBuffer->LineAssemblage.LineCache + CursorsLine - 2)->BytesIn;
            
            if(PreviousLineLength >= RoundS32xToU32x(FileInstance->CursorBias))
            {
                // Note: Off by 1 because of CursorBias starting at 1 (first character)
                FileInstance->Cursor = StartOfUpperLine + FileInstance->CursorBias - 1;
                if(FileBuffer->Memory[FileInstance->Cursor] == '\n' && FileInstance->Cursor > 0 &&
                   FileBuffer->Memory[FileInstance->Cursor - 1] == '\r')
                {
                    --FileInstance->Cursor;
                }
            }
            else
            {
                // Note: Off by 1 because of the line feed character being counted
                FileInstance->Cursor = StartOfUpperLine + PreviousLineLength - 1;
            }
        }
        else
        {
            FileInstance->Cursor = 0;
            FileInstance->CursorBias = 1;
        }
    }
}

local void
MoveCursorDownLine(file_instance *FileInstance, b32 LineWrapping)
{
    file_buffer *FileBuffer = FileInstance->FileBuffer;
    
    if(FileBuffer->Size)
    {
        u32x CursorsLine = GetLineAtBytesIn(FileBuffer, FileInstance->Cursor);
        
        u32x LineLength = GetLineLength(FileBuffer, CursorsLine);
        u32x NextLineLength = GetLineLength(FileBuffer, CursorsLine + 1);
        
        u32x StartOfCursorsLine = (FileBuffer->LineAssemblage.LineCache + CursorsLine - 1)->BytesIn;
        
        if(StartOfCursorsLine + LineLength >= FileBuffer->Size)
        {
            FileInstance->Cursor = FileBuffer->Size;
            FileInstance->CursorBias = GetLineLength(FileBuffer, FileBuffer->LineAssemblage.Count) + 1;
        }
        else
        {
            // Note: LineCache counts line 1 as 0, 2 as 1 etc...
            u32x StartOfLowerLine = 0;
            if(FileBuffer->LineAssemblage.Count == CursorsLine)
            {
                StartOfLowerLine = (FileBuffer->LineAssemblage.LineCache + FileBuffer->LineAssemblage.Count - 1)->BytesIn;
            }
            else
            {
                StartOfLowerLine = (FileBuffer->LineAssemblage.LineCache + CursorsLine)->BytesIn;
            }
            
            if(NextLineLength >= RoundS32xToU32x(FileInstance->CursorBias))
            {
                // Note: Off by 1 because of CursorBias starting at 1 (first character)
                FileInstance->Cursor = StartOfLowerLine + FileInstance->CursorBias - 1;
            }
            else
            {
                if(CursorsLine + 1 == FileBuffer->LineAssemblage.Count &&
                   LineLength > NextLineLength)
                {
                    FileInstance->Cursor = FileBuffer->Size;
                }
                else
                {
                    // Note: Off by 1 because of the line feed character being counted
                    //       (Then why isn't the \r character being counted?)
                    FileInstance->Cursor = StartOfLowerLine + NextLineLength - 1;
                    if(FileBuffer->Memory[FileInstance->Cursor] == '\n' &&
                       FileBuffer->Memory[FileInstance->Cursor - 1] == '\r')
                    {
                        --FileInstance->Cursor;
                    }
                }
            }
        }
    }
}

local file_instance *
GetActiveFileInstance(file_instance *Instances, size_t InstanceCount)
{
    file_instance *Result = {};
    
    for(size_t Index = 0;
        Index < InstanceCount;
        ++Index)
    {
        if(Instances[Index].Active)
        {
            Result = &Instances[Index];
            break;
        }
    }
    
    return Result;
}

local void
GetCursorLineCharacters(file_instance *FileInstance, u8 *String)
{
    u8 *Dest = String;
    u8 *At = FileInstance->FileBuffer->Memory + FileInstance->Cursor;
    for(;
        At > FileInstance->FileBuffer->Memory;
        --At)
    {
        if(*(At - 1) == '\n')
        {
            break;
        }
    }
    
    for(;
        At < FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size;
        )
    {
        if(*At == '\n')
        {
            break;
        }
        *Dest++ = *At++;
    }
    *Dest = '\0';
}

local u8 *
GetLinePointer(file_buffer *FileBuffer, umm Line, umm *BytesOut)
{
    u8 *Result = FileBuffer->Memory;

    if(FileBuffer->Size &&
        Line - 1 > 0 && Line - 1 <= FileBuffer->LineAssemblage.Count)
    {
        line_cache *LineCache = (FileBuffer->LineAssemblage.LineCache + Line - 1);

        Result = LineCache->Line;
        if(BytesOut)
        {
            *BytesOut = LineCache->BytesIn;
        }
    }

    return Result;
}

local u32
WrapCountOfLine(file_instance *FileInstance, font *Font, umm Line)
{
    u32 Result = 0;

    file_buffer *FileBuffer = FileInstance->FileBuffer;

    if(Line < FileBuffer->LineAssemblage.Count)
    {
        r32 AtX = 0.0f;

        u8 *EndPoint = 0;
        if(Line + 1 <= FileBuffer->LineAssemblage.Count)
        {
            EndPoint = (FileBuffer->LineAssemblage.LineCache + Line + 1)->Line;
        }
        else
        {
            EndPoint = FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size;
        }

        for(u8 *At = (FileBuffer->LineAssemblage.LineCache + Line)->Line;
            At < EndPoint;
            ++At)
        {
            u32 Codepoint = *At;

            r32 AdvanceX = GetAdvanceX(Font, Codepoint);

            if(*At == '\t') AdvanceX *= 4;

#if 1
            r32 KerningAmount = 0;
            if(*(At + 1))
            {
                KerningAmount = GetKerning(Font, Codepoint, (u32)*(At + 1));
            }

            AtX += KerningAmount + AdvanceX;
#else
            r32 _UnusedY = 0.0f;
            stbtt_aligned_quad Quad;
            stbtt_GetPackedQuad(Font->FontAtlas.CharData, Font->FontAtlas.Width, Font->FontAtlas.Height,
                *At,
                &Result, &_UnusedY,
                &Quad,
                false);
#endif

            if(AtX + (2*AdvanceX - 4.0f) - FileInstance->CameraPositionX > FileInstance->Window.P2.x)
            {
                AtX = 0.0f;
                ++Result;
            }
        }
    }

    return Result;
}

local u32
DrawnWrapCountUntilBytesIn(file_instance *FileInstance, font *Font, umm BytesIn)
{
    // TODO: This //
    u32 Result = 0;

    file_buffer *FileBuffer = FileInstance->FileBuffer;

    r32 AtX = 0.0f;

    umm DrawnStartIndex = 0;
    if(FileBuffer->LineAssemblage.LineCache)
    {
        DrawnStartIndex = (FileBuffer->LineAssemblage.LineCache + FileInstance->CameraPositionY)->BytesIn;
    }

    for(u8 *At = FileBuffer->Memory + DrawnStartIndex;
        At < FileBuffer->Memory + BytesIn;
        ++At)
    {
        u32 Codepoint = *At;

        r32 AdvanceX = GetAdvanceX(Font, Codepoint);

        if(*At == '\t') AdvanceX *= 4;

#if 1
        r32 KerningAmount = 0;
        if(*(At + 1))
        {
            KerningAmount = GetKerning(Font, Codepoint, (u32)*(At + 1));
        }

        AtX += KerningAmount + AdvanceX;
#else
        r32 _UnusedY = 0.0f;
        stbtt_aligned_quad Quad;
        stbtt_GetPackedQuad(Font->FontAtlas.CharData, Font->FontAtlas.Width, Font->FontAtlas.Height,
            *At,
            &Result, &_UnusedY,
            &Quad,
            false);
#endif

        if(Codepoint == '\n' || Codepoint == '\r')
        {
            AtX = 0.0f;
        }

        if(AtX + (2*AdvanceX - 4.0f) - FileInstance->CameraPositionX > FileInstance->Window.P2.x)
        {
            AtX = 0.0f;
            ++Result;
        }
    }

    return Result;
}

local void
MoveToPrevParagraph(file_instance *FileInstance)
{
    b32 WeHaveAWinner = false;
    u8 *At;
    u8 *EndOfLine;
    
    for(At = FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1;
        At >= FileInstance->FileBuffer->Memory;
        --At)
    {
        if(*At == '\n')
        {
            break;
        }
    }
    
    // At = FileInstance->FileBuffer->LineAssemblage.LineCache + GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor) - 1);
    
    for(;
        At >= FileInstance->FileBuffer->Memory;
        --At)
    {
        if(*At == '\n')
        {
            EndOfLine = At;
            --At;
            
            for(;
                At >= FileInstance->FileBuffer->Memory;
                --At)
            {
                if(!IsWhitespace(*At))
                {
                    break;
                }
                if(*At == '\n')
                {
                    WeHaveAWinner = true;
                    break;
                }
            }
            
            if(WeHaveAWinner)
            {
                FileInstance->Cursor = (EndOfLine - FileInstance->FileBuffer->Memory);

                if(FileInstance->FileBuffer->Size >= 2 && FileInstance->Cursor - 1 > 0 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\n' && FileInstance->Cursor - 2 >= 0 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 2) == '\r')
                {
                    --FileInstance->Cursor;
                }
                else if(FileInstance->FileBuffer->Size >= 2 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor)     == '\n' && FileInstance->Cursor > 0 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\r')
                {
                    --FileInstance->Cursor;
                }

                FileInstance->CursorBias = GetCursorBias(FileInstance);
                break;
            }
        }
    }
    
    if(At <= FileInstance->FileBuffer->Memory)
    {
        FileInstance->Cursor = 0;
        FileInstance->CursorBias = 1;
    }
}

local void
MoveToNextParagraph(file_instance *FileInstance)
{
    u8 *EndOfFile = FileInstance->FileBuffer->Memory + FileInstance->FileBuffer->Size;
    
    b32 WeHaveAWinner = false;
    u8 *At;
    for(At = FileInstance->FileBuffer->Memory + FileInstance->Cursor;
        At < EndOfFile;
        ++At)
    {
        if(*At == '\n')
        {
            ++At;
            if(At == EndOfFile)
            { // Note: Edge cases...
                break;
            }
            
            for(;
                At < EndOfFile;
                ++At)
            {
                if(!IsWhitespace(*At))
                {
                    break;
                }
                if(*At == '\n')
                { // Note: A full line of text that's whitespace... Jackpot
                    WeHaveAWinner = true;
                    break;
                }
            }
            
            if(WeHaveAWinner)
            {
                FileInstance->Cursor = (At - FileInstance->FileBuffer->Memory);

                if(FileInstance->FileBuffer->Size >= 2 && FileInstance->Cursor - 1 > 0 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\n' && FileInstance->Cursor - 2 >= 0 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 2) == '\r')
                {
                    --FileInstance->Cursor;
                }
                else if(FileInstance->FileBuffer->Size >= 2 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor)     == '\n' && FileInstance->Cursor > 0 &&
                    *(FileInstance->FileBuffer->Memory + FileInstance->Cursor - 1) == '\r')
                {
                    --FileInstance->Cursor;
                }

                FileInstance->CursorBias = GetCursorBias(FileInstance);
                break;
            }
        }
    }
    
    if(At == EndOfFile)
    {
        FileInstance->Cursor = (At - FileInstance->FileBuffer->Memory);
        FileInstance->CursorBias = GetCursorBias(FileInstance);
    }
}

local void
SaveBufferToFile(file_instance *FileInstance)
{ // Todo: If working with \r\n line endings works out, then we can clean this up
    StartTimedPoint(SaveBufferToFile);
    file_buffer *OutBuffer = FileInstance->FileBuffer;
    
#if 0
    // Note: if we ever decide that we want to remove \r\n later, then this is here
    if(OutBuffer->LineEnding == LineEnding_Dos)
    {
        OutBuffer = CopyFileBuffer(OutBuffer);
        InsertCarriageReturns(OutBuffer);
    }
#endif
    
    u8 OutgoingPath[MAX_PATH];
    StringCopy(OutgoingPath, OutBuffer->Filepath);
    StringCat(OutgoingPath, OutBuffer->Filename);
    
    DebugWriteFile(OutgoingPath, OutBuffer->Memory, OutBuffer->Size);
    
#if 0
    if(OutBuffer->LineEnding == LineEnding_Dos)
    {
        FreeFileBuffer(&OutBuffer);
    }
#endif

    EndTimedPoint(SaveBufferToFile);
}

local u32x
GetIndentationAmountAtPoint(file_buffer *FileBuffer, u32x BytesIn)
{
    StartTimedPoint(GetIndentationAmountAtPoint);
    u32x Result = 0;
    
    u32x TabAmount = 4;
    
    umm StackCount = 0;
    u32x Stack[8096] = {}; // Todo: Do heap allocation instead?
    
    u8 ArcanumRune = 0;
    
    for(u8 *At = FileBuffer->Memory;
        At < FileBuffer->Memory + BytesIn; // BytesIn is the end of the cursor's line
        ++At)
    {
        switch(*At)
        {
            case '{':
            {
                Stack[StackCount + 1] = Stack[StackCount] + TabAmount;
                ++StackCount;
                ArcanumRune = '{';
            } break;
            case '}':
            {
                if(StackCount)
                {
                    if(StackCount > 0 && Stack[StackCount] > 0)
                    {
                        Stack[StackCount] = 0;
                        --StackCount;
                    }
                }
            } break;
            case '(':
            {
                u32x IndentAmount = BytesLeftOfPoint(FileBuffer, At - FileBuffer->Memory);
                Stack[StackCount + 1] = IndentAmount + 1;
                ++StackCount;
            } break;
            case ')':
            {
                if(StackCount)
                {
                    Stack[StackCount] = 0;
                    --StackCount;
                }
            } break;
            case '\n':
            {
                ArcanumRune = '\n';
            } break;
            case '\'':
            {
                ++At;
                for(;
                    At < FileBuffer->Memory + BytesIn && At < FileBuffer->Memory + FileBuffer->Size;
                    ++At)
                {
                    if(*At == '\\')
                    {
                        ++At;
                        if(*At == '\'')
                        {
                            ++At;
                        }
                    }
                    if(*At == '\'')
                    {
                        break;
                    }
                }
            } break;
            case '"':
            {
                ++At;
                for(;
                    At < FileBuffer->Memory + BytesIn && At < FileBuffer->Memory + FileBuffer->Size;
                    ++At)
                {
                    if(*At == '\\')
                    {
                        ++At;
                        if(*At == '"')
                        {
                            ++At;
                        }
                    }
                    if(*At == '"')
                    {
                        break;
                    }
                }
            } break;
            case '/':
            {
                if(*(At + 1) == '/')
                {
                    At += 2;
                    for(;
                        At < FileBuffer->Memory + BytesIn && At < FileBuffer->Memory + FileBuffer->Size;
                        ++At)
                    {
                        if(*At == '\n')
                        {
                            break;
                        }
                    }
                }
            } break;
        }
    }
    
    b32 EncounteredNonWhitespace = false;
    
    for(u8 *At = FileBuffer->Memory + BytesIn;
        At < FileBuffer->Memory + FileBuffer->Size && *At != '\n';
        ++At)
    { // Note: This parses the current line that is being indented. We want it to behave differently
        if(*At == '}')
        {
            if(StackCount && 
               (ArcanumRune == '{' || ArcanumRune == '\n'))
            {
                if(StackCount > 0 && Stack[StackCount] > 0)
                {
                    Stack[StackCount] = 0;
                    --StackCount;
                }
            }
        }
        else if(*At == '{')
        {
            break;
        }
        else if(*At == '#' && !EncounteredNonWhitespace)
        { // Note: We are assuming that we left Stack[0] as 0
            StackCount = 0;
            break;
        }
        else if (*At == '\'')
        {
            ++At;
            for(;
                At < FileBuffer->Memory + BytesIn && At < FileBuffer->Memory + FileBuffer->Size;
                ++At)
            {
                if(*At == '\\')
                {
                    ++At;
                    if(*At == '\'')
                    {
                        ++At;
                    }
                }
                if(*At == '\'')
                {
                    break;
                }
            }
        }
        else if(*At == '"')
        {
            ++At;
            for(;
                At < FileBuffer->Memory + BytesIn && At < FileBuffer->Memory + FileBuffer->Size;
                ++At)
            {
                if(*At == '\\')
                {
                    ++At;
                    if(*At == '"')
                    {
                        ++At;
                    }
                }
                else if(*At == '"' || *At == '\n')
                {
                    break;
                }
            }
        }
        else if('/')
        {
            if(*(At + 1) == '/')
            {
                At += 2;
                for(;
                    At < FileBuffer->Memory + BytesIn && At < FileBuffer->Memory + FileBuffer->Size;
                    ++At)
                {
                    if(*At == '\n')
                    {
                        break;
                    }
                }
            }
        }
        
		// Todo/Testing: This was not reachable before, let's make sure it won't produce bugs
        if(!IsWhitespace(*At))
        {
            EncounteredNonWhitespace = true;
        }
    }
    
    if(StackCount)
    {
        Result = Stack[StackCount];
    }
    
    EndTimedPoint(GetIndentationAmountAtPoint);
    return Result;
}

local inline void
ReindentLine(file_buffer *FileBuffer, u32x Line)
{
    StartTimedPoint(ReindentLine);
    
    if(FileBuffer->Size)
    {
        u32x OldIndentAmount = 0;
        u32x LeftmostNonwhitespace = 0;
        u32x BeginningOfLine = 0;

        u8 *AtLine = GetLinePointer(FileBuffer, Line, &BeginningOfLine);

        u32x EndOfLine = BeginningOfLine;
        for(u8 *At = AtLine;
            At <= FileBuffer->Memory + FileBuffer->Size;
            ++At, ++EndOfLine)
        {
            if(*At == '\n' || At == FileBuffer->Memory + FileBuffer->Size)
            {
                break;
            }
        }

        for(u8 *At = AtLine;
            At <= FileBuffer->Memory + FileBuffer->Size;
            ++At)
        {
            if(IsWhitespace(*At) && *At != '\n')
            {
                ++OldIndentAmount;
            }
            else
            {
                LeftmostNonwhitespace = At - AtLine;
                break;
            }
        }

        if(LeftmostNonwhitespace > 0)
        {
            DeleteSection(FileBuffer, BeginningOfLine, BeginningOfLine + LeftmostNonwhitespace, true);
        }

        u32x IndentAmount = GetIndentationAmountAtPoint(FileBuffer, BeginningOfLine);
        u8 *AddToString = (u8 *)Allocate(IndentAmount + 1);
        StringNCharCopy(AddToString, ' ', IndentAmount);
        *(AddToString + IndentAmount) = 0;

        AddString(FileBuffer, AddToString, BeginningOfLine, true);

        Deallocate(AddToString);
    }

    EndTimedPoint(ReindentLine);
}
