
global mandala GlobalFontMandala;

typedef struct font_atlas
{
    void *Atlas;
    u32 Width;
    u32 Height;
    u32 Handle;
    b32 Changed;

    stbtt_packedchar CharData[255];
} font_atlas;

typedef struct glyph
{
    v2 Alignment; // 4 4 -> 8
    v2 UVMin;     // 4 4 -> 8
    v2 UVMax;     // 4 4 -> 8
    u32 Character;
    u16 Width;    // 2
    u16 Height;   // 2
} glyph;

typedef struct font
{
    font_atlas FontAtlas;

    stbtt_fontinfo FontInfo;
    r32 STBScale;

    u32 CodepointCount;
    glyph *Codepoints;
    
    r32 Ascent;
    r32 Descent;
    r32 Height;
    r32 LineAdvance;
    r32 TopPadding;
    
    void *Win32Handle;
    debug_read_file_result FontFileResult;
} font;

u32
GetClampedCodepoint(font *Font, u32 Codepoint)
{
    u32 Result = 0;
    
    if(Codepoint < 32)
    {
        Result = 32;
    }
    else if(Codepoint > Font->CodepointCount)
    {
        Result = Font->CodepointCount;
    }
    else
    {
        Result = Codepoint;
    }
    
    return Result;
}

r32
GetAdvanceX(font *Font, u32 Codepoint)
{
    r32 Result = 0.0f;

    r32 AdvanceX = 0.0f;

    int AdvX;
    stbtt_GetCodepointHMetrics(&Font->FontInfo, Codepoint, &AdvX, 0);
    AdvanceX = (r32)AdvX*Font->STBScale;

    Result = AdvanceX;

    return Result;
}

r32
PixelsWideOfString(font *Font, u8 *String)
{
    r32 Result = 0.0f;

    for(u8 *At = (u8 *)String;
        *At;
        ++At)
    {
        Result += GetAdvanceX(Font, *At);
    }

    return Result;
}

r32
GetKerning(font *Font, u32 Codepoint1, u32 Codepoint2)
{
    r32 Result = 0.0f;
    r32 KerningAmount = 0.0f;

    KerningAmount = (r32)stbtt_GetCodepointKernAdvance(&Font->FontInfo, Codepoint1, Codepoint2)*Font->STBScale;

    Result = KerningAmount;

    return Result;
}

void
LoadFontAtlas(font *Font)
{
    StartTimedPoint(LoadFontAtlas);

    font_atlas *FontAtlas = &Font->FontAtlas;

    if(FontAtlas->Atlas)
    {
        Deallocate(FontAtlas->Atlas);
        FontAtlas->Changed = true;
    }

    FontAtlas->Width = 512;
    FontAtlas->Height = 512;
    FontAtlas->Atlas = Allocate(FontAtlas->Width*FontAtlas->Height);

#if 1
    u32 UpperCharLimit = 32 + Font->CodepointCount;

    stbtt_pack_context PackContext;
    stbtt_PackBegin(&PackContext, (u8 *)FontAtlas->Atlas, FontAtlas->Width, FontAtlas->Height, FontAtlas->Width, 1, 0);
    stbtt_PackSetOversampling(&PackContext, 4, 2);
    stbtt_PackFontRange(&PackContext, (u8 *)Font->FontFileResult.Memory, 0, Font->Height, 32, UpperCharLimit - 32, FontAtlas->CharData + 32);
    stbtt_PackEnd(&PackContext);
    
    *((u8 *)FontAtlas->Atlas + FontAtlas->Width*FontAtlas->Height - 1) = 0xFF;

    for(u32 At = 32;
        At < UpperCharLimit;
        ++At)
    {
        glyph *Glyph = &(Font->Codepoints[At]);

        Glyph->Character = At;
        Glyph->Width =  FontAtlas->CharData[At].x1 - FontAtlas->CharData[At].x0;
        Glyph->Height = FontAtlas->CharData[At].y1 - FontAtlas->CharData[At].y0;
        Glyph->Alignment = V2(0.0f, 0.0f);
    }

#else

    r32 AtX = 0;
    r32 AtY = 0;

    for(u32 At = 32;
        At < Font->CodepointCount;
        ++At)
    {
        for(u32 SubPixel = 0;
            SubPixel < 3;
            ++SubPixel)
        {
            glyph *Glyph = &(Font->Codepoints[At*3 + SubPixel]);

            r32 ShiftX = 0.0f;
            if(SubPixel == 1)
            {
                ShiftX = 0.333333f;
            }
            else if(SubPixel == 2)
            {
                ShiftX = 0.666666f;
            }

            int Width, Height;
            int XOffset, YOffset;
            u8 *CodepointBitmap = 0;
            CodepointBitmap = stbtt_GetCodepointBitmapSubpixel(&Font->FontInfo, 0.0f, Font->STBScale, 
                                                               ShiftX, 0.0f,
                                                               At, &Width, &Height, &XOffset, &YOffset);

            if(CodepointBitmap)
            {
                int BitmapWidth = Width + 2;
                int BitmapHeight = Height + 2;
                int BitmapStride = FontAtlas->Width;

                if(AtX + BitmapWidth > FontAtlas->Width)
                {
                    AtX = 0;
                    AtY += (u32)RoundR32(Font->Height);

                    if(AtY + BitmapHeight > FontAtlas->Height)
                    {
                        Assert(!"Not enough minerals!... I mean Font Atlas space...");
                    }
                }

                Glyph->UVMin.u = (r32)AtX/(r32)FontAtlas->Width;
                Glyph->UVMin.v = (r32)AtY/(r32)FontAtlas->Height;

                Glyph->UVMax.u = (r32)(AtX + BitmapWidth)/(r32)FontAtlas->Width;
                Glyph->UVMax.v = (r32)(AtY + BitmapHeight)/(r32)FontAtlas->Height;

                u8 *GlyphMemory = ((u8 *)FontAtlas->Atlas + AtX + AtY*FontAtlas->Width);

                AtX += BitmapWidth;

                u8 *DestRow = (u8 *)(GlyphMemory)+BitmapStride;
                u8 *Source = CodepointBitmap;
                for(s32 Y = 0;
                    Y < Height;
                    ++Y)
                {
                    u8 *Dest = DestRow;
                    for(s32 X = 0;
                        X < Width;
                        ++X)
                    {
#if 0
                        u32 Color = (((u32)(*Source)&0xFF) << 24 |
                                     ((u32)(*Source)&0xFF) << 16 |
                                     ((u32)(*Source)&0xFF) <<  8 |
                                     ((u32)(*Source)&0xFF) <<  0);

                        *Dest++ = Color;
#else
                        *Dest++ = *Source;
#endif

                        ++Source;
                    }
                    DestRow += BitmapStride;
                }

                int LeftSideBearing;
                stbtt_GetCodepointHMetrics(Font->FontInfo, At, 0, &LeftSideBearing);
                Glyph->Alignment = V2((r32)LeftSideBearing*Font->STBScale,
                                      YOffset + Font->Ascent);

                stbtt_FreeBitmap(CodepointBitmap, 0);

                Glyph->Width =  BitmapWidth;
                Glyph->Height = BitmapHeight;
            }
        }
    }
#endif

    EndTimedPoint(LoadFontAtlas);
}

glyph *
GetGlyph(font *Font, u32 Codepoint)
{
    glyph *Result = 0;

    Codepoint = GetClampedCodepoint(Font, Codepoint);

    Result = (Font->Codepoints + Codepoint);

    return Result;
}

void
LoadFont(font *Font, config *Config)
{
    StartTimedPoint(LoadFont);

    Font->CodepointCount = 254-32;
    if(Font->FontFileResult.Memory)
    {
        DebugFreeFile(&Font->FontFileResult);
    }
    SetMemory(GlobalFontMandala.Base, 0, GlobalFontMandala.Size);
    Font->Codepoints = (glyph *)PushArray(&GlobalFontMandala, glyph, Font->CodepointCount*3);

    // Todo: FontPath for other operating systems?
    u8 FilePath[MAX_PATH] = "";
    StringCopy(FilePath, FontPath);
    if(*Config->FontFilename)
    {
        StringCat(FilePath, Config->FontFilename);
    }

    Font->FontFileResult = DebugReadFile(FilePath);

    if(!Font->FontFileResult.Memory)
    {
        StringCopy(FilePath, FontPath);
        StringCat(FilePath, "consola.ttf");
        Font->FontFileResult = DebugReadFile(FilePath);
        if(!Font->FontFileResult.Memory)
        { // Note: You don't have Consolas?
          // What font does everyone have? (Mac/Unix/Windows)
            InvalidCodePath;
        }
    }

    int Success = stbtt_InitFont(&Font->FontInfo, (u8 *)Font->FontFileResult.Memory, 0);

    if(!Success)
    {
        InvalidCodePath;
    }

    Font->STBScale = stbtt_ScaleForPixelHeight(&Font->FontInfo, (r32)Config->FontSize);

    Font->TopPadding = 0.0f;// 1.0f/Font->STBScale;
    int Ascent, Descent, LineGap;
    stbtt_GetFontVMetrics(&Font->FontInfo, &Ascent, &Descent, &LineGap);
    Font->Ascent = (Ascent + Font->TopPadding)*Font->STBScale;
    Font->Descent = Descent*Font->STBScale;
    Font->Height = (r32)((Ascent + Font->TopPadding) - Descent)*Font->STBScale;
    Font->LineAdvance = (Font->Height + LineGap*Font->STBScale);

    LoadFontAtlas(Font);

    // Note: We can't free the read ttf file, otherwise it will fubar stbtt_fontinfo
    // DebugFreeFile(&Font->FontFileResult);
    EndTimedPoint(LoadFont);
}
