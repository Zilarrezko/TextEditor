typedef enum render_entry_type
{
    RenderEntryType_render_entry_clear,
    RenderEntryType_render_entry_bitmap,
    RenderEntryType_render_entry_rectangle,
    RenderEntryType_render_entry_glyph,
    RenderEntryType_render_entry_coordinate_system
} render_entry_type;
typedef struct render_group_entry_header
{
    render_entry_type Type;
} render_group_entry_header;

typedef struct render_entry_clear
{
    v4 Color;
} render_entry_clear;

typedef struct render_entry_bitmap
{
    v2 Position;
    v4 Color;
    bitmap *Bitmap;
} render_entry_bitmap;

typedef struct render_entry_rectangle
{
    v2 Position;
    v4 Color;
    v2 Dim;
} render_entry_rectangle;

typedef struct render_entry_glyph
{
    v2 Position;
    v2 Position2;
    v4 Color;
    glyph *Glyph;
} render_entry_glyph;

typedef struct render_entry_coordinate_system
{
    v2 Position;
    v2 XAxis;
    v2 YAxis;
    v4 Color;
    bitmap *Bitmap;
} render_entry_coordinate_system;

typedef struct render_group
{
    v2 Offset;
    
    rect ClipRect;

    u32x MaxGroupSize;
    umm GroupSize;
    u8 *GroupBase;
    
    editor_assets *Assets;
} render_group;

inline render_group *
InitRenderGroup(editor_assets *Assets, mandala *TransientMandala, u64 MaxBufferSize)
{
    render_group *Result = 0;
    
    Assert(TransientMandala->Used + MaxBufferSize <= TransientMandala->Size);
    
    Result = (render_group *)PushStruct(TransientMandala, render_group);
    Result->GroupBase = (u8 *)PushSize(TransientMandala, (umm)MaxBufferSize);
    Result->GroupSize = 0;
    Result->MaxGroupSize = MaxBufferSize;
    Result->Assets = Assets;
    Result->Offset = V2(0.0f, 0.0f);
    
    return Result;
}

inline void
ResetRenderGroup(render_group *RenderGroup)
{
    RenderGroup->Offset = V2(0.0f, 0.0f);
    RenderGroup->GroupSize = 0;
}

#define PushRenderElement(RenderGroup, type) (type *)PushRenderElement_(RenderGroup, sizeof(type), RenderEntryType_##type)
local inline void *
PushRenderElement_(render_group *RenderGroup, u32 Size, render_entry_type Type)
{
    void *Result = 0;
    Assert(RenderGroup->GroupSize + Size <= RenderGroup->MaxGroupSize);
    
    Size += sizeof(render_group_entry_header);
    
    render_group_entry_header *Header = (render_group_entry_header *)(RenderGroup->GroupBase + RenderGroup->GroupSize);
    Header->Type = Type;
    Result = (Header + 1);
    RenderGroup->GroupSize += Size;
    
    return Result;
}

local inline void
PushPiece(render_group *RenderGroup, bitmap *Bitmap, v2 Position, v4 Color)
{
    Position = Position + RenderGroup->Offset;
    
    render_entry_bitmap *RenderEntry = PushRenderElement(RenderGroup, render_entry_bitmap);
    if(RenderEntry)
    {
        if(!Bitmap->Memory)
        { // Todo: return or error?
            InvalidCodePath;
        }

        RenderEntry->Bitmap = Bitmap;
        RenderEntry->Position = Position;
        RenderEntry->Color = Color;
    }
}

local inline void
PushGlyph(render_group *RenderGroup, font *Font, u32 Codepoint, v2 Position, v2 Position2, v2 UVMin, v2 UVMax, v4 Color)
{
    if(Font)
    {
        if(RectangleIntersects(RectV2(Position, Position2), RenderGroup->ClipRect))
        {
            render_entry_glyph *RenderEntry = PushRenderElement(RenderGroup, render_entry_glyph);
            if(RenderEntry)
            {
                glyph *Glyph = GetGlyph(Font, Codepoint);

                RenderEntry->Glyph = Glyph;
                RenderEntry->Glyph->UVMin = UVMin;
                RenderEntry->Glyph->UVMax = UVMax;
                RenderEntry->Position = Position;
                RenderEntry->Position2 = Position2;
                RenderEntry->Color = Color;
            }
        }
    }
    else
    {
        InvalidCodePath;
    }
}

inline void
PushRect(render_group *RenderGroup, v2 Position, v2 Dim, v4 Color)
{
    render_entry_rectangle *RenderEntry = PushRenderElement(RenderGroup, render_entry_rectangle);
    if(RenderEntry)
    {
        RenderEntry->Dim = Dim;
        RenderEntry->Position = Position + RenderGroup->Offset;
        RenderEntry->Color = Color;
    }
}

inline void
Clear(render_group *RenderGroup, v4 Color)
{
    render_entry_clear *RenderEntry = PushRenderElement(RenderGroup, render_entry_clear);
    if(RenderEntry)
    {
        RenderEntry->Color = Color;
    }
}

enum rect_outline_side
{
    RectOutlineSide_Left   = 1 << 0,
    RectOutlineSide_Right  = 1 << 1,
    RectOutlineSide_Top    = 1 << 2,
    RectOutlineSide_Bottom = 1 << 3,
    RectOutlineSide_All    = 0x0F,
};
inline void
PushRectOutline(render_group *RenderGroup, v2 Position, v2 Dim, r32 Border, v4 Color, u32 Sides = RectOutlineSide_All)
{ // Todo: Turn specific parts off? i.e. Only bottom on?
    if(BitfieldTest(Sides, RectOutlineSide_Top))
    {
        PushRect(RenderGroup, Position, V2(Dim.x, Border), Color); // Note: Top
    }
    if(BitfieldTest(Sides, RectOutlineSide_Left))
    {
        PushRect(RenderGroup, V2(Position.x, Position.y + Border), V2(Border, Dim.y - Border), Color); // Note: Left
    }
    if(BitfieldTest(Sides, RectOutlineSide_Right))
    {
        PushRect(RenderGroup, V2(Position.x + Dim.x - Border, Position.y + Border), V2(Border, Dim.y - Border), Color); // Note: Right
    }
    if(BitfieldTest(Sides, RectOutlineSide_Bottom))
    {
        PushRect(RenderGroup, V2(Position.x + Border, Position.y + Dim.y - Border), V2(Dim.x - Border*2, Border), Color); // Note: Bottom
    }
}

local inline void
PushClipRect(render_group *RenderGroup, v2 MinPoint, v2 MaxPoint)
{
    RenderGroup->ClipRect = RectV2(MinPoint, MaxPoint);
}

local inline void
PushClipRect(render_group *RenderGroup, rect ClipRect)
{
    RenderGroup->ClipRect = ClipRect;
}

void
CoordinateSystem(render_group *RenderGroup, v2 Position, v2 XAxis, v2 YAxis, v4 Color, bitmap *Bitmap)
{
    render_entry_coordinate_system *RenderEntry = PushRenderElement(RenderGroup, render_entry_coordinate_system);
    if(RenderEntry)
    {
        RenderEntry->Position = Position + RenderGroup->Offset;
        RenderEntry->XAxis = XAxis;
        RenderEntry->YAxis = YAxis;
        RenderEntry->Color = Color;
        RenderEntry->Bitmap = Bitmap;
    }
}

r32
PushTextLineOfLength(render_group *RenderGroup, font *Font, u8 *String, umm StringLength, v2 Position, v4 Color)
{
    r32 AtX = Position.x + RenderGroup->Offset.x;
    r32 AtY = Position.y + RenderGroup->Offset.y + Font->Ascent;
    u32 PrevCodepoint = 0;
    r32 KerningAmount;

    for(u8 *At = String;
        At < String + StringLength;
        ++At)
    {
        u32 Codepoint = *At;
        
        stbtt_aligned_quad Quad;
        stbtt_GetPackedQuad(Font->FontAtlas.CharData, Font->FontAtlas.Width, Font->FontAtlas.Height,
                            *At,
                            &AtX, &AtY,
                            &Quad,
                            false);

        v2 RenderPoint =  {Quad.x0, Quad.y0};
        v2 RenderPoint2 = {Quad.x1, Quad.y1};

        v2 UVMin = {Quad.s0, Quad.t0};
        v2 UVMax = {Quad.s1, Quad.t1};

        if(!IsWhitespace((u8)Codepoint))
        {
            PushGlyph(RenderGroup, Font, Codepoint, RenderPoint, RenderPoint2,
                      UVMin, UVMax,
                      Color);
        }
        
        if(*(At+1))
        {
            KerningAmount = (r32)stbtt_GetCodepointKernAdvance(&Font->FontInfo, Codepoint, *(At + 1))*Font->STBScale;

            AtX += KerningAmount;
        }
        
        PrevCodepoint = Codepoint;
    }
    
    return AtX - (Position.x + RenderGroup->Offset.x);
}

r32
PushTextLine(render_group *RenderGroup, font *Font, u8 *Text, v2 Position, v4 Color)
{
    r32 Result = 0.0f;
    
    umm TextLength = StringLength(Text);
    Result = PushTextLineOfLength(RenderGroup, Font, Text, TextLength, Position, Color);
    
    return Result;
}

void
PushOffset(render_group *RenderGroup, v2 Offset)
{
    RenderGroup->Offset = Offset;
}

void
PushTextFileInstance(render_group *RenderGroup, file_instance *FileInstance, comment_keyword_hash *CommentKeywordHash, font *Font, config *Config, r32 DeltaTime, b32 IsMaximized)
{
    r32 BorderSize = (IsMaximized) ? Config->BorderSize : 0.0f;

    file_buffer *FileBuffer = FileInstance->FileBuffer;
    rect DrawingRectangle = FileInstance->Window;
    
    r32 DrawingLeft =   Min(DrawingRectangle.P1.x, DrawingRectangle.P2.x);
    r32 DrawingTop =    Min(DrawingRectangle.P1.y, DrawingRectangle.P2.y);
    r32 DrawingRight =  Max(DrawingRectangle.P2.x, DrawingRectangle.P1.x);
    r32 DrawingBottom = Max(DrawingRectangle.P2.y, DrawingRectangle.P1.y);
    
    r32 DrawingWidth = DrawingRight - DrawingLeft;
    r32 DrawingHeight = DrawingBottom - DrawingTop;
    
    s32x LineLength = 0;
    r32 LineAdvance = Font->LineAdvance;
    s32x LineNumber = 1;
    s32x CursorLineNumber = (s32x)GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Cursor);
    s32x MarkerLineNumber = (FileInstance->MarkerIsDown) ? (s32x)GetLineAtBytesIn(FileInstance->FileBuffer, FileInstance->Marker) : 0;
    char Character = ' ';
    b32 CursorCharacter = false;
    
    r32 InfoBarHeight = Font->Height + 2.0f;
    r32 CommandBarHeight = Font->Height + 2.0f;
    
    r32 AtY = 0.0f;
    r32 CursorPositionX = GetBytesInXOffsetInPixels(FileInstance, Font, FileInstance->Cursor, Config->LineWrapping);
    r32 MarkerPositionX = GetBytesInXOffsetInPixels(FileInstance, Font, FileInstance->Marker, Config->LineWrapping);
    r32 CursorWidth = 1.0f;
    
    r32 LinesInAScreen = (DrawingHeight - 2.0f*BorderSize - InfoBarHeight)/LineAdvance;
    
    r32 GutterWidth = 0;
    u32 MaxCharactersForMaxLineNumber = 0;
    {
        u8 Buffer[256] = "";

        s32x MaxLineNumberOnScreen = 0;
        if(Config->LineNumberType == LineNumber_Normal)
        {
            MaxLineNumberOnScreen = FileInstance->CameraPositionY + (s32x)(LinesInAScreen + (FileInstance->CameraPositionYOffset/LineAdvance) + 1.0f);
        }
        else if(Config->LineNumberType == LineNumber_Relative)
        {
            MaxLineNumberOnScreen = RoundR32ToS32x(LinesInAScreen - 1.0f);
        }
        else if(Config->LineNumberType == LineNumber_CaptainKraft)
        {
            if(CursorLineNumber > RoundR32ToS32x(LinesInAScreen - 1.0f))
            {
                MaxLineNumberOnScreen = CursorLineNumber;
            }
            else
            {
                MaxLineNumberOnScreen = RoundR32ToS32x(LinesInAScreen - 1.0f);
            }
        }
        FormatString(sizeof(Buffer), Buffer, "%d", MaxLineNumberOnScreen);
        MaxCharactersForMaxLineNumber = StringLength(Buffer);
        r32 PixelsWideOfMaximumLinesOnScreen = (FileBuffer->Size > 0) ? PixelsWideOfString(Font, Buffer) + ((Config->LineNumberType == LineNumber_CaptainKraft) ? GetAdvanceX(Font, ' ') : 0.0f)
                                                                      : 0.0f;
        GutterWidth = Config->LineNumbers ? PixelsWideOfMaximumLinesOnScreen + Config->GutterPaddingSize*GetAdvanceX(Font, ' ') : Config->GutterPaddingSize*GetAdvanceX(Font, ' ');
    }

    rect ClipRect = RectR32(DrawingLeft + GutterWidth, DrawingTop, DrawingRight, DrawingBottom);

    PushClipRect(RenderGroup, ClipRect);

    s32 CameraLineWrapCount = Config->LineWrapping ? WrapCountOfLine(FileInstance, Font, FileInstance->CameraPositionY) : 0;

    u32 DrawnWrapCountUntilCursorLine = Config->LineWrapping ? DrawnWrapCountUntilBytesIn(FileInstance, Font, FileInstance->Cursor) : 0;
    r32 CursorLineY = (r32)(CursorLineNumber - FileInstance->CameraPositionY - 1 + DrawnWrapCountUntilCursorLine)*Font->LineAdvance;

    { // Note: Get Camera Positioning based on cursor
        if(CursorLineNumber - 1 < FileInstance->CameraPositionY ||
           CursorLineY <= 0)
        {
            FileInstance->CameraPositionY = CursorLineNumber - 1;
            FileInstance->CameraPositionYOffset = 0.0f;
        }
        else if(CursorLineNumber + DrawnWrapCountUntilCursorLine > (r32)(LinesInAScreen) + FileInstance->CameraPositionY)
        {
            if(Config->LineWrapping)
            {
                b32 OutOfRangeOfWrappedLine = (LinesInAScreen + CameraLineWrapCount + 1) < ((CursorLineNumber - FileInstance->CameraPositionY) + DrawnWrapCountUntilCursorLine) ? true : false;
                if(CameraLineWrapCount && !OutOfRangeOfWrappedLine)
                {
                    r32 WrappingOffset = ((r32)CameraLineWrapCount - ((LinesInAScreen + (r32)CameraLineWrapCount) - (((r32)CursorLineNumber + (r32)DrawnWrapCountUntilCursorLine) - (r32)FileInstance->CameraPositionY)));
                    FileInstance->CameraPositionYOffset = (WrappingOffset)*LineAdvance;
                }
                else
                {
                    FileInstance->CameraPositionY = ((CursorLineNumber + (DrawnWrapCountUntilCursorLine - CameraLineWrapCount)) - 1 - (s32)LinesInAScreen);

                    FileInstance->CameraPositionYOffset = ((r32)((u32x)(LinesInAScreen)) + 1 - LinesInAScreen)*LineAdvance;
                }
            }
            else
            {
                FileInstance->CameraPositionY = ((CursorLineNumber) - 1 - (s32)LinesInAScreen);

                FileInstance->CameraPositionYOffset = ((r32)((s32)(LinesInAScreen)) + 1 - LinesInAScreen)*LineAdvance;
            }
        }
        
        Character = ((umm)FileInstance->Cursor < FileBuffer->Size && *(FileBuffer->Memory + FileInstance->Cursor) != '\r' && *(FileBuffer->Memory + FileInstance->Cursor) != '\n')
                    ? *(FileBuffer->Memory + FileInstance->Cursor)
                    : ' ';

        r32 AdvanceX = GetAdvanceX(Font, (u32)Character);

        if(Character == '\t') AdvanceX *= 4;
        
        CursorWidth = AdvanceX + 1;
        
        if(CursorPositionX < FileInstance->CameraPositionX)
        {
            FileInstance->CameraPositionX = CursorPositionX;
        }
        else if(!Config->LineWrapping && (CursorPositionX + CursorWidth > (DrawingWidth - 2.0f*BorderSize + FileInstance->CameraPositionX - GutterWidth)))
        {
            FileInstance->CameraPositionX = CursorPositionX + CursorWidth - (DrawingWidth - 2.0f*BorderSize - GutterWidth);
        }
    }

    PushOffset(RenderGroup, V2(DrawingLeft + GutterWidth - FileInstance->CameraPositionX + BorderSize,
               DrawingTop - FileInstance->CameraPositionYOffset - AtY + BorderSize));

    if(FileInstance->Active)
    {
        CursorLineY = (r32)(CursorLineNumber - FileInstance->CameraPositionY - 1 + DrawnWrapCountUntilCursorLine)*Font->LineAdvance;

        if(!FileInstance->CommandFocus || FileInstance->CommandType == CommandType_Open)
        { // Note: Cursor line's background rectangles

            PushOffset(RenderGroup, V2(DrawingLeft + GutterWidth + BorderSize,
                DrawingTop - FileInstance->CameraPositionYOffset + CursorLineY + BorderSize));

            PushRect(RenderGroup, V2(0.0f, 0.0f),
                V2(DrawingWidth - (GutterWidth + BorderSize), Font->Height), Config->CursorLineFillColor);

            if(FileInstance->CommandFocus == CommandType_Open)
            {
                PushRectOutline(RenderGroup, V2(0.0f, 0.0f),
                    V2(DrawingWidth - (GutterWidth + BorderSize), Font->Height), 1,
                    V4(255.0f, 150.0f, 0.0f, 255.0f));
            }
            else
            {
                PushRectOutline(RenderGroup, V2(0.0f, 0.0f),
                    V2(DrawingWidth - (GutterWidth + BorderSize), Font->Height), 1,
                    Config->CursorLineOutlineColor);
            }
        }

        PushOffset(RenderGroup, V2(DrawingLeft + GutterWidth - FileInstance->CameraPositionX + BorderSize,
            DrawingTop - FileInstance->CameraPositionYOffset + CursorLineY + BorderSize));

        if(FileInstance->Insert && !FileInstance->CommandFocus)
        {
            PushRectOutline(RenderGroup, V2(CursorPositionX, 0.0f),
                V2(CursorWidth, Font->Height), 1, Config->CursorColor);
        }
        else if(!FileInstance->Insert && FileInstance->CommandType != CommandType_Open)
        {
            if(Config->CursorType == CursorType_Block)
            {
                PushRect(RenderGroup, V2(CursorPositionX, 0.0f),
                    V2(CursorWidth, Font->Height), Config->CursorColor);
            }
            else if(Config->CursorType == CursorType_Line)
            {
                PushRect(RenderGroup, V2(CursorPositionX, 0.0f),
                    V2(1, Font->Height), Config->CursorColor);
            }
            else if(Config->CursorType == CursorType_Underscore)
            {
                PushRect(RenderGroup, V2(CursorPositionX, Font->Ascent),
                    V2(CursorWidth, 2.0f), Config->CursorColor);
            }
        }
    }
    else
    {
        CursorLineY = (r32)(CursorLineNumber - FileInstance->CameraPositionY - 1 + DrawnWrapCountUntilCursorLine)*Font->LineAdvance;

        PushOffset(RenderGroup, V2(DrawingLeft + GutterWidth - FileInstance->CameraPositionX + BorderSize,
            DrawingTop - FileInstance->CameraPositionYOffset + CursorLineY + BorderSize));

        PushRectOutline(RenderGroup, V2(CursorPositionX, 0.0f),
            V2(CursorWidth, Font->Height), 1, Config->CursorColor);
    }

    if(FileInstance->MarkerIsDown)
    {
        r32 MarkerLineY = (r32)(MarkerLineNumber - FileInstance->CameraPositionY - 1)*Font->LineAdvance;

        PushOffset(RenderGroup, V2(DrawingLeft + GutterWidth - FileInstance->CameraPositionX + BorderSize,
            DrawingTop - FileInstance->CameraPositionYOffset + MarkerLineY + BorderSize));

        PushRectOutline(RenderGroup, V2(MarkerPositionX, 0.0f),
            V2(CursorWidth, Font->Height), 1, Config->MarkerColor);
    }


    u32x Index = 0;
    if(FileBuffer->LineAssemblage.LineCache)
    {
        Index = (FileBuffer->LineAssemblage.LineCache + FileInstance->CameraPositionY)->BytesIn;
        LineNumber = FileInstance->CameraPositionY + 1;
    }

    token *Token = 0;

    if(FileBuffer->Memory)
    {
        ClipRect = RectR32(DrawingLeft, DrawingTop, DrawingRight, DrawingBottom - InfoBarHeight);
        PushClipRect(RenderGroup, ClipRect);

        for(;
            Index <= FileBuffer->Size;
            ++Index)
        {
            if(Index == FileBuffer->Size || *(FileBuffer->Memory + Index) == '\n')
            { // Note: Once we reach the end of the line

                PushOffset(RenderGroup,
                           V2(DrawingLeft + GutterWidth - FileInstance->CameraPositionX + BorderSize,
                              DrawingTop - FileInstance->CameraPositionYOffset + BorderSize));

                if(LineLength > 0)
                { // Note: Render the line
                    r32 AtX = RenderGroup->Offset.x;
                    u32 PrevCodepoint = 0;

                    u8 *StartOfLine = (u8 *)FileBuffer->Memory + Index - LineLength;
                    for(u8 *At = StartOfLine;
                        At <= StartOfLine + LineLength && At < FileBuffer->Memory + FileBuffer->Size;
                        ++At)
                    {
                        u32 Codepoint = *At;

                        // r32 AdvanceX = GetAdvanceX(Font, Codepoint);

                        // Todo: Proper tab width?
                        // if(*At == '\t') AdvanceX *= 4;

                        // r32 RenderPoint = AtX;

                        // AtX += AdvanceX;

                        u8 RenderedCharacter = 0;
                        switch(*At)
                        {
                            case '\r':
                            {
                                RenderedCharacter = 186;
                            } break;
                            case '\n':
                            {
                                RenderedCharacter = 182;
                            } break;
                            default:
                            {
                                RenderedCharacter = *At;
                            }
                        }

                        r32 OffsetedY = AtY + RenderGroup->Offset.y + Font->Ascent;
                        stbtt_aligned_quad Quad;
                        stbtt_GetPackedQuad(Font->FontAtlas.CharData, Font->FontAtlas.Width, Font->FontAtlas.Height,
                                            RenderedCharacter,
                                            &AtX, &OffsetedY,
                                            &Quad,
                                            false);

                        v2 RenderPoint =  {Quad.x0, Quad.y0};
                        v2 RenderPoint2 = {Quad.x1, Quad.y1};

                        v2 UVMin = {Quad.s0, Quad.t0};
                        v2 UVMax = {Quad.s1, Quad.t1};

                        if(Config->LineWrapping)
                        {
                            if(AtX - FileInstance->CameraPositionX > DrawingRectangle.P2.x)
                            {
                                AtX = RenderGroup->Offset.x;
                                AtY += LineAdvance;

                                OffsetedY = AtY + RenderGroup->Offset.y + Font->Ascent;
                                stbtt_GetPackedQuad(Font->FontAtlas.CharData, Font->FontAtlas.Width, Font->FontAtlas.Height,
                                                    RenderedCharacter,
                                                    &AtX, &OffsetedY,
                                                    &Quad,
                                                    false);

                                RenderPoint =  {Quad.x0, Quad.y0};
                                RenderPoint2 = {Quad.x1, Quad.y1};

                                UVMin = {Quad.s0, Quad.t0};
                                UVMax = {Quad.s1, Quad.t1};
                            }
                        }
                        else
                        {
                            if(AtX - FileInstance->CameraPositionX > DrawingRectangle.P2.x)
                            {
                                break;
                            }
                        }

                        if((umm)(At - FileBuffer->Memory) == (umm)FileInstance->Cursor)
                        { // Note: If at cursor point
                          // Note: In case we want to do something with the glyph that the cursor is on
                            CursorCharacter = true;
                        }

                        if(FileBuffer->Parsed)
                        {
                            if(!Token)
                            {
                                Token = GetTokenAtPoint(FileBuffer, (umm)(At - FileBuffer->Memory), 0);
                            }
                            else if((umm)(At - FileBuffer->Memory) > Token->BytesIn + Token->BytesLong - 1)
                            {
                                ++Token;
                            }
                        }

                        if(!IsWhitespace((char)Codepoint))
                        {
                            if(At > FileBuffer->Memory && *(At-1))
                            { // Note: I guess stbtt is handling kerning? *shrug*
#if 0
                                r32 KerningAmount = GetKerning(Font, *(At - 1), Codepoint);
                                RenderPoint += KerningAmount;
#endif
                            }
                            if(Codepoint >= ' ' && Codepoint <= '~')
                            {
                                if(!CursorCharacter || FileInstance->Insert ||
                                   !FileInstance->Active || Config->CursorType != CursorType_Block ||
                                   FileInstance->CommandType == CommandType_Open)
                                {
                                    v4 FontColor = Config->FontColor;
                                    if(FileBuffer->Parsed)
                                    {
                                        switch(Token->Type)
                                        {
                                            case Token_Identifier: { FontColor = Config->FontIdentifierColor; } break;
                                            case Token_Literal: { FontColor = Config->FontLiteralColor; } break;
                                            case Token_Number: { FontColor = Config->FontNumberColor; } break;
                                            case Token_Comment: 
                                            {
                                                if(Token->SubType == Token_CommentKeyword)
                                                {
                                                    u8 KeywordBuffer[1024] = "";
                                                    StringCopyLength(KeywordBuffer, FileBuffer->Memory + Token->BytesIn, Token->BytesLong);

                                                    GetCommentKeywordInfoFromHash(CommentKeywordHash, KeywordBuffer, &FontColor);
                                                }
                                                else
                                                {
                                                    FontColor = Config->FontCommentColor;
                                                }
                                            } break;
                                            case Token_Keyword: { FontColor = Config->FontKeywordColor; } break;
                                            case Token_Operator: { FontColor = Config->FontOperatorColor; } break;
                                            case Token_Preprocessor: { FontColor = Config->FontPreprocessorColor; } break;
                                            case Token_Unknown: { FontColor = Config->FontUnknownColor; } break;
                                        }

                                        paste_location_data PastedSectionData;
                                        if(IsPastedAtBytesIn(FileInstance, (umm)(At - FileBuffer->Memory), &PastedSectionData))
                                        {
                                            FontColor = Lerp(FontColor, Config->PasteColorChangeColor, PastedSectionData.Time/Config->PasteColorChangeDuration);
                                        }
                                    }

                                    PushGlyph(RenderGroup, Font, Codepoint,
                                              RenderPoint, RenderPoint2,
                                              UVMin, UVMax,
                                              FontColor);
                                }
                                else
                                {
                                    // Note: Character under cursor
                                    PushGlyph(RenderGroup, Font, Codepoint,
                                              RenderPoint, RenderPoint2,
                                              UVMin, UVMax,
                                              Config->BackgroundColor);
                                }
                            }
                        }
                        else if(Config->RenderSpecialCharacters && (Codepoint == '\r' || Codepoint == '\n'))
                        {
                            PushGlyph(RenderGroup, Font, RenderedCharacter,
                                      RenderPoint, RenderPoint2,
                                      UVMin, UVMax,
                                      V4(73, 101, 190, 255));
                        }

                        if(CursorCharacter)
                        {
                            CursorCharacter = !CursorCharacter;
                        }

                        PrevCodepoint = Codepoint;
                    }

                    Token = 0;
                }

                ++LineNumber;

                AtY += LineAdvance;

                if(AtY + FileInstance->CameraPositionYOffset - Font->Height > DrawingRectangle.P2.y - InfoBarHeight)
                {
                    break;
                }

                LineLength = 0;
            }
            else
            {
                ++LineLength;
            }
        }
    }

    if(1)
    { // Note: Gutter Drawing
        PushOffset(RenderGroup, V2(DrawingLeft + BorderSize,
                   DrawingTop + BorderSize));

        PushRect(RenderGroup, V2(0.0f, 0.0f), V2(GutterWidth + BorderSize, DrawingHeight - InfoBarHeight),
                 Config->GutterColor);
    }
    if(Config->LineNumbers && FileBuffer->Size)
    {
        ClipRect = RectR32(DrawingLeft, DrawingTop, DrawingRight, DrawingBottom - InfoBarHeight);
        PushClipRect(RenderGroup, ClipRect);

        // Note: The reason we're drawing here is so hide text that would be drawn on top of these
        PushOffset(RenderGroup,
                   V2(DrawingLeft + BorderSize,
                       DrawingTop - FileInstance->CameraPositionYOffset + BorderSize));

        AtY = 0.0f;

        u8 LineNumberBuffer[256] = "";
        for(s32x Line = FileInstance->CameraPositionY + 1;
            Line <= (s32x)FileBuffer->LineAssemblage.Count;
            ++Line)
        {
            if(Config->LineNumberType == LineNumber_Normal)
            {
                FormatString(sizeof(LineNumberBuffer), LineNumberBuffer, "%*d", MaxCharactersForMaxLineNumber, Line);
            }
            else if(Config->LineNumberType == LineNumber_Relative)
            {
                s32x Temp = Abs(Line - CursorLineNumber);
                FormatString(sizeof(LineNumberBuffer), LineNumberBuffer, "%*d", MaxCharactersForMaxLineNumber, Temp);
            }
            else if(Config->LineNumberType == LineNumber_CaptainKraft)
            {
                s32x Temp = 0;
                if(Line == CursorLineNumber)
                {
                    Temp = CursorLineNumber;
                    FormatString(sizeof(LineNumberBuffer), LineNumberBuffer, "%*d ", MaxCharactersForMaxLineNumber, Temp);
                }
                else
                {
                    Temp = Abs(Line - CursorLineNumber);
                    FormatString(sizeof(LineNumberBuffer), LineNumberBuffer, "%*d", MaxCharactersForMaxLineNumber+1, Temp);
                }
            }
            PushTextLine(RenderGroup, Font, LineNumberBuffer, V2(Config->GutterPaddingSize*GetAdvanceX(Font, ' ')/2.0f, AtY), Config->FontGutterColor);

            AtY += LineAdvance;

            u32 LineWrapCount = WrapCountOfLine(FileInstance, Font, Line - 1);
            if(LineWrapCount)
            {
                for(u32 WrapIndex = 0;
                    WrapIndex < LineWrapCount;
                    ++WrapIndex)
                {
                    AtY += LineAdvance;
                }
            }

            // Note: I think the LineAdvance*2 is a hack in this case... or perhaps it always was an off by one. Might come by and bite me
            if(AtY + FileInstance->CameraPositionYOffset - DrawnWrapCountUntilCursorLine*LineAdvance - LineAdvance*2 > DrawingRectangle.P2.y - InfoBarHeight ||
               Line >= (s32x)FileBuffer->LineAssemblage.Count)
            {
                break;
            }
        }
    }

    ClipRect = RectR32(DrawingLeft, DrawingTop, DrawingRight, DrawingBottom);
    PushClipRect(RenderGroup, ClipRect);

    PushOffset(RenderGroup, V2(DrawingLeft + BorderSize, DrawingTop - BorderSize));

    { // Note: FileBuffer info bar
        r32 InfoBarY = DrawingHeight - InfoBarHeight;
        PushRect(RenderGroup, V2(0.0f, InfoBarY),
            V2(DrawingWidth - BorderSize, InfoBarHeight + 1), Config->InfoBarColor);
        PushRectOutline(RenderGroup, V2(0.0f, InfoBarY),
            V2(DrawingWidth - BorderSize, InfoBarHeight), 1, Config->InfoBarBorderColor,
            RectOutlineSide_Top);

        char LineEndingBuffer[5];
        if(FileBuffer->LineEnding == LineEnding_Dos)
        {
            StringCopy(LineEndingBuffer, "Dos");
        }
        else
        {
            StringCopy(LineEndingBuffer, "Unix");
        }

        // Note: Color Tween for saving
        r32 TweenPercentage = (FileBuffer->HasChanges) ? 1.0f : 0.0f;
        if(FileInstance->SaveIndicatorTime > 0.0f)
        {
            FileInstance->SaveIndicatorTime -= DeltaTime;

            if(FileInstance->SaveIndicatorTime <= 0.0f)
            {
                FileInstance->SaveIndicatorTime = 0.0f;
            }

            TweenPercentage = FileInstance->SaveIndicatorTime/FileInstance->SaveIndicatorDuration;
        }
        PushRect(RenderGroup, V2(0.0f, InfoBarY),
                 V2(PixelsWideOfString(Font, FileBuffer->Filename) + 2.0f, InfoBarHeight + 1.0f), Config->InfoFontColor*TweenPercentage);

        r32 TextPushover = 0.0f;
        if(FileInstance->SaveIndicatorTime > 0.0f)
        {
            TextPushover = PushTextLine(RenderGroup, Font, (u8 *)FileBuffer->Filename,
                                        V2(1.0f, InfoBarY + InfoBarHeight/2.0f - Font->Height/2.0f),
                                        Lerp(Config->InfoFontColor, Config->InfoBarColor, TweenPercentage));
        }
        else
        {
            TextPushover = PushTextLine(RenderGroup, Font, (u8 *)FileBuffer->Filename,
                                        V2(1.0f, InfoBarY + InfoBarHeight/2.0f - Font->Height/2.0f),
                                        (FileBuffer->HasChanges) ? Config->InfoBarColor : Config->InfoFontColor);
        }

        FormatString(sizeof(FileInstance->InfoBuffer), FileInstance->InfoBuffer, " - S:%lld L:%lld C:%llu %s",
                     FileBuffer->Size, CursorLineNumber, GetCursorBias(FileInstance), LineEndingBuffer);

        PushTextLine(RenderGroup, Font, FileInstance->InfoBuffer,
                     V2(TextPushover + 1.0f, InfoBarY + InfoBarHeight/2.0f - Font->Height/2.0f),
                     Config->InfoFontColor);
        
    }
    if(FileInstance->CommandFocus)
    { // Note: Command Bar
        PushRect(RenderGroup, V2(0.0f, DrawingHeight - CommandBarHeight),
                 V2(DrawingWidth, CommandBarHeight), Config->CommandBarColor);
        
        r32 CommandOffset = PushTextLine(RenderGroup, Font, FileInstance->CommandPredicate,
                                         V2(5.0f, DrawingHeight - Font->Height),
                                         Config->CommandPredicateFontColor);

        // Note: -1 Is to leave room for the cusor at the end
        s32 GlyphSpace = FloorR32ToS32((DrawingWidth - (CommandOffset + 5.0f))/GetAdvanceX(Font, ' ')) - 1;

        u8 *OffsetCommandBuffer = Max(FileInstance->CommandBuffer + StringLength(FileInstance->CommandBuffer) - GlyphSpace, FileInstance->CommandBuffer);

        CommandOffset += PushTextLine(RenderGroup, Font, OffsetCommandBuffer,
                                      V2(CommandOffset + 5.0f, DrawingHeight - Font->Height),
                                      Config->CommandFontColor);

        PushRect(RenderGroup, V2(CommandOffset + 5.0f, DrawingHeight - Font->Height), V2(GetAdvanceX(Font, ' '), Font->Height), Config->CursorColor);
    }
    
    PushOffset(RenderGroup, V2(0.0f, 0.0f));
    
    // Note: Instance Window Border
    b32 LeftBorder = (DrawingLeft > GutterWidth + BorderSize) ? true : false;
    b32 TopBorder = (DrawingTop > BorderSize) ? true : false;
    
    if(LeftBorder || TopBorder)
    {
        u32 Sides = 0;
        
        if(LeftBorder) BitfieldSet(Sides, RectOutlineSide_Left);
        if(TopBorder) BitfieldSet(Sides, RectOutlineSide_Top);
        
        PushRectOutline(RenderGroup,
                        V2(DrawingLeft, DrawingTop),
                        V2(DrawingRight - DrawingLeft, DrawingBottom - DrawingTop),
                        BorderSize, Config->BorderColor, 
                        Sides);
    }
}
