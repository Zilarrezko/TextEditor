//
// Todo:
//
// Editing Features:
//    - System for saving *empty* buffer as something, somewhere
//    - Option for CursorLine's rectangle to be a whole line underscore
//    - Better handling of FileInstances and windowing
//        - Alt-[Right|Left|Up|Down] Switch throughout open instance windows?
//    - Implement the standard style of selecting text rather than a marker
//    - Highlighting words with a filled/outlined rectangle
//    - Listing hashed files
//        - select which one to open in other window
//    - Spruce up the open file/explorer functionality
//    - Recognizing, viewing and editing of binary files
//        - Hex line numbers for binary files
//    - Be able to paste binary (bitmaps, binary files, etc) in different forms as text like...
//        - Comma seperated hexadecimal list
//
// Architecture: I never like doing these, they require a lot of rework
//    - Unicode
//    - Create more and parse commandline arguments
//    - A better method of showing that the file needs to be saved, and that you have saved
//        - Maybe if you save the file the info bar blinks once?
//        - Maybe when there's been a change to the file, only highlight/backlight the file name?
//    - Give programability from an outside DLL
//        - No idea how to do this
//        - Should I do this
//    - Do a pass on memory cleaup and reduction
//    - Optimize for large files
//
// Lexical:
//    - Lexate Odin
//    - Lexate Jai
//
// OS:
//    - Drag and drop files onto the editor to open them
//        - Look into com interfaces for extra functionality?
//    - Execute a command and get it's input output handles
//    - Selecting characters using mouse cursor
//

//
// Bugs:
//    - Lexical: There's an error somewhere in the lexer when trying to parse out 1.4GB test file
//        - At first glance it looks like we're getting a null pointer from allocating too much maybe?
//        - Fixed? Yeah I think it might be fixed...
//    - LineWrapping: There's a few problems with line wrapping right now...
//        - When CameraY is on a line wrapped line and you move the cursor up, it moves the CameraYOffset
//        - There's others :x

//
// Low Priority Todo:
//    - Option for Font Ligatures: https://github.com/tonsky/FiraCode
//    - Option for cursor changes to the glyph color underneath it
//    - Autosave functionality?
//    - Save session?
//        - Probably should be one of the last things we do because we will have to serialize
//          and format will be subject to change a lot as the editor becomes more complete
//    - Get rid of the windows titlebar and make our own close minimize maximize/restore
//

// Changelog for self-note
// - 

#define BITMAP_BYTESPP 4
#define MAX_PATH 260

typedef struct bitmap // 20 bytes
{
    void *Memory; // 8
    v2 Alignment; // 4 4 -> 8
    u16 Width;    // 2
    u16 Height;   // 2
} bitmap;

typedef enum cursor_type
{
    CursorType_Block,
    CursorType_Line,
    CursorType_Underscore
} cursor_type;
typedef enum line_number_type
{
    LineNumber_Normal,
    LineNumber_Relative,
    LineNumber_CaptainKraft
} line_number_type;
typedef struct config
{   // Default values
    u8 Filename[MAX_PATH];
    FILETIME LastWriteTime;

    b32 ConfigReloaded;

    v4 BackgroundColor = {21, 21, 21, 255};

    v4 CursorColor = {192,  25, 25, 255};
    v4 MarkerColor = { 10, 195, 35, 111};

    v4 CursorLineFillColor =    {16, 16, 16, 255};
    v4 CursorLineOutlineColor = {36, 36, 36, 255};

    v4 InfoBarBorderColor = {255, 76,  0, 255};
    v4 InfoBarColor =       { 14, 14, 14, 255};
    v4 InfoFontColor =      {255, 76,  0, 255};

    v4 GutterColor =     { 32,  32,  32, 255};
    v4 FontGutterColor = {200, 200, 200, 255};

    v4 CommandBarColor =           { 14,  14,  14, 255};
    v4 CommandFontColor =          {255,  76,   0, 255};
    v4 CommandPredicateFontColor = {  0, 180, 180, 255};

    v4 BorderColor = {205, 76, 0, 255};

    v4 FontColor =             {192, 192, 192, 255};
    v4 FontIdentifierColor =   {192, 192, 192, 255};
    v4 FontLiteralColor =      {192, 192, 192, 255};
    v4 FontNumberColor =       {192, 192, 192, 255};
    v4 FontCommentColor =      {192, 192, 192, 255};
    v4 FontKeywordColor =      {192, 192, 192, 255};
    v4 FontOperatorColor =     {192, 192, 192, 255};
    v4 FontPreprocessorColor = {192, 192, 192, 255};
    v4 FontUnknownColor =      {192, 192, 192, 255};

    r32 GutterPaddingSize = 0.0f;
    r32 BorderSize = 1.0f;

    cursor_type CursorType = CursorType_Block;
    line_number_type LineNumberType = LineNumber_Normal;

    u8 FontFilename[256];
    u16 FontSize = 15;

    b32 LineWrapping = false;
    b32 MarkerSelection = true;
    b32 AutoSave = false;
    b32 LineNumbers = true;

    umm UndoMaxCount = 1024;

    r32 PasteColorChangeDuration = 0.5f;
    v4 PasteColorChangeColor = {205, 50, 0, 255};

    b32 RenderSpecialCharacters = false;
} config;

#include "font.h"

typedef struct editor_assets
{
    font CodeFont;
} editor_assets;

enum dictionary
{
    Dictionary_cpp,
    Dictionary_cpp_keyword,
    Dictionary_jai,
    Dictionary_jai_keyword,
    Dictionary_odin,
    Dictionary_odin_keyword,
    Dictionary_normal,
    Dictionary_count
};
#include "file.h"

#include "render_group.h"

#include "opengl.h"

typedef struct editor_backbuffer
{
    void *Memory;
    u16 Width;
    u16 Height;
} editor_backbuffer;

typedef struct editor_memory
{
    b32 IsInitialized;

    void *MainMemoryBlock;
    umm MainMemoryBlockSize;

    void *TransientMemory;
    umm TransientMemorySize;
    void *PermanentMemory;
    umm PermanentMemorySize;
} editor_memory;

typedef struct
{
    b32 Running;
    b32 Pause;
    b32 IsMaximized;

    mandala TransMandala;
    mandala Mandala;

    umm InstanceCount;
    file_instance *Instances;
    file_hash FileHash;

    umm ActiveInstanceIndex;
    umm InstanceWindows;

    s32 MouseX;
    s32 MouseY;

    render_group *RenderGroup;
    editor_assets Assets;

    char LocalTime[24]; // Note: Because why not, amirite?

    char *CommandLine;

    config Config;

    comment_keyword_hash CommentKeywordHash;

    trie_node Dictionary[Dictionary_count];
    word_list WordList;
    b32 WordListIsValid;

    opengl_info OpenGLInfo;
} editor_state;

typedef enum mouse_state
{
    MouseState_left_pressed,
    MouseState_left_released,
    MouseState_right_pressed,
    MouseState_right_released,
} mouse_state;
