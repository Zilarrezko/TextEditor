#ifndef JEC_GL_H

typedef int GLsizei;
typedef int GLint;
typedef int GLboolean;
typedef unsigned char GLubyte;
typedef unsigned int GLenum;
typedef unsigned int GLuint;
typedef unsigned int GLbitfield;

typedef void GLvoid;

typedef float GLfloat;
typedef float GLclampf;

typedef size_t GLsizeiptr;
typedef long long GLintptr;

#define GL_FLOAT 0x1406
#define GL_TEXTURE_2D 0x0DE1
#define GL_UNSIGNED_BYTE 0x1401
#define GL_TEXTURE_MAG_FILTER 0x2800
#define GL_TEXTURE_MIN_FILTER 0x2801
#define GL_TEXTURE_WRAP_S 0x2802
#define GL_TEXTURE_WRAP_T 0x2803
#define GL_RED 0x1903
#define GL_RGBA8 0x8058
#define GL_NEAREST 0x2600
#define GL_BLEND 0x0BE2
#define GL_SCISSOR_TEST 0x0C11
#define GL_ZERO 0
#define GL_ONE 1
#define GL_ONE_MINUS_SRC_ALPHA 0x0303
#define GL_POINTS 0x0000
#define GL_LINES 0x0001
#define GL_LINE_LOOP 0x0002
#define GL_LINE_STRIP 0x0003
#define GL_TRIANGLES 0x0004
#define GL_TRIANGLE_STRIP 0x0005
#define GL_TRIANGLE_FAN 0x0006
#define GL_QUADS 0x0007
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_MODULATE 0x2100
#define GL_TEXTURE_ENV 0x2300
#define GL_TEXTURE_ENV_MODE 0x2200
#define GL_CLAMP 0x2900
#define GL_VERSION 0x1F02

#define GL_ARRAY_BUFFER         0x8892
#define GL_STATIC_DRAW          0x88E4
#define GL_STATIC_READ          0x88E5
#define GL_STATIC_COPY          0x88E6
#define GL_DYNAMIC_DRAW         0x88E8
#define GL_DYNAMIC_READ         0x88E9
#define GL_DYNAMIC_COPY         0x88EA
#define GL_FRAGMENT_SHADER      0x8B30
#define GL_VERTEX_SHADER        0x8B31
#define GL_COMPILE_STATUS       0x8B81
#define GL_INFO_LOG_LENGTH      0x8B84
#define GL_TEXTURE_SWIZZLE_RGBA 0x8E46

#define GLAPI extern

extern "C"
{
    GLAPI void APIENTRY glBindTexture(GLenum Target, GLuint Texture);
    GLAPI void APIENTRY glGenTextures(GLsizei Index, GLuint *Textures);
    GLAPI void APIENTRY glTexImage2D(GLenum Target, GLint Level, GLint InternalFormat, GLsizei Width, GLsizei Height, GLint Border, GLenum Format, GLenum Type, const GLvoid *Pixels);
    GLAPI void APIENTRY glTexSubImage2D(GLenum Target, GLint Level, GLint XOffset, GLint YOffset, GLsizei Width, GLsizei Height, GLenum Format, GLenum Type, const GLvoid *Pixels);
    GLAPI void APIENTRY glViewport(GLint X, GLint Y, GLsizei Width, GLsizei Height);
    GLAPI void APIENTRY glEnable(GLenum Feature);
    GLAPI void APIENTRY glBlendFunc(GLenum SourceFactor, GLenum DestFactor);
    GLAPI void APIENTRY glScissor(GLint X, GLint Y, GLsizei Width, GLsizei Height);
    GLAPI void APIENTRY glClear(GLbitfield Mask);
    GLAPI void APIENTRY glClearColor(GLclampf Red, GLclampf Green, GLclampf Blue, GLclampf Alpha);
    GLAPI void APIENTRY glDrawArrays(GLenum Mode, GLint First, GLsizei Count);
    GLAPI const GLubyte *APIENTRY glGetString (GLenum Name);

    GLAPI void APIENTRY glTexParameteri(GLenum Target, GLenum ParameterName, GLint Parameter);
    GLAPI void APIENTRY glTexParameteriv(GLenum Target, GLenum ParameterName, const GLint *Parameters);
    GLAPI void APIENTRY glTexEnvi(GLenum Target, GLenum ParameterName, GLint Parameter);
}

#define JEC_GL_H
#endif